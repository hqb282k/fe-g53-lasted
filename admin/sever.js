const express = require('express');
const path = require('path');
const app = express();
app.use(express.static(__dirname + '/dist/dmd-crm-frontend'));
app.get('/*', function(req,res) {
res.sendFile(path.join(__dirname+
'/dist/dmd-crm-frontend/index.html'));});
app.listen(process.env.PORT || 4200);