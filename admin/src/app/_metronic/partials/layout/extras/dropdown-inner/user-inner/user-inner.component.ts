import { Router } from '@angular/router';
import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';

@Component({
    selector: 'app-user-inner',
    templateUrl: './user-inner.component.html',
})
export class UserInnerComponent implements OnInit, OnDestroy {
    @HostBinding('class')
    class = `menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px`;
    @HostBinding('attr.data-kt-menu') dataKtMenu = 'true';

    language: LanguageFlag;
    user$: Observable<any>;
    langs = languages;
    private unsubscribe: Subscription[] = [];
    userName: any;
    email: any;
    avatar: any;
    token:any = '';


    constructor(
        private router: Router,
        public commonService: CommonService
    ) { }

    ngOnInit(): void {
        this.getInformation();
    }

    getInformation(): void {
        this.token = localStorage.getItem('access_token') ? localStorage.getItem('access_token') : '';
        this.userName = localStorage.getItem('_user_name') ? localStorage.getItem('_user_name') : '';
        this.email = localStorage.getItem('_email') ? localStorage.getItem('_email') : '';
        this.avatar = localStorage.getItem('_avatar') ? localStorage.getItem('_avatar') : '';
    }

    logout() {
        localStorage.removeItem('access_token');
        document.location.reload();
    }

    selectLanguage(lang: string) {
        this.setLanguage(lang);
        // document.location.reload();
    }

    setLanguage(lang: string) {
        this.langs.forEach((language: LanguageFlag) => {
            if (language.lang === lang) {
                language.active = true;
                this.language = language;
            } else {
                language.active = false;
            }
        });
    }

    login() {
        
        this.router.navigate(['/auth/login']);
    }

    ngOnDestroy() {
        this.unsubscribe.forEach((sb) => sb.unsubscribe());
    }
}

interface LanguageFlag {
    lang: string;
    name: string;
    flag: string;
    active?: boolean;
}

const languages = [
    {
        lang: 'en',
        name: 'English',
        flag: './assets/media/flags/united-states.svg',
    },
    {
        lang: 'zh',
        name: 'Mandarin',
        flag: './assets/media/flags/china.svg',
    },
    {
        lang: 'es',
        name: 'Spanish',
        flag: './assets/media/flags/spain.svg',
    },
    {
        lang: 'ja',
        name: 'Japanese',
        flag: './assets/media/flags/japan.svg',
    },
    {
        lang: 'de',
        name: 'German',
        flag: './assets/media/flags/germany.svg',
    },
    {
        lang: 'fr',
        name: 'French',
        flag: './assets/media/flags/france.svg',
    },
];
