import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    AfterViewInit,
} from '@angular/core';
import { LayoutService } from './core/layout.service';
import { LayoutInitService } from './core/layout-init.service';
import { Subscription } from 'rxjs';
import { ProfileService } from 'src/app/services/profile.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, AfterViewInit {
    // Public variables
    selfLayout = 'default';
    asideSelfDisplay: true;
    asideMenuStatic: true;
    contentClasses = '';
    contentContainerClasses = '';
    toolbarDisplay = true;
    contentExtended: false;
    asideCSSClasses: string;
    asideHTMLAttributes: any = {};
    headerMobileClasses = '';
    headerMobileAttributes = {};
    footerDisplay: boolean;
    footerCSSClasses: string;
    headerCSSClasses: string;
    headerHTMLAttributes: any = {};
    // offcanvases
    extrasSearchOffcanvasDisplay = false;
    extrasNotificationsOffcanvasDisplay = false;
    extrasQuickActionsOffcanvasDisplay = false;
    extrasCartOffcanvasDisplay = false;
    extrasUserOffcanvasDisplay = false;
    extrasQuickPanelDisplay = false;
    extrasScrollTopDisplay = false;
    asideDisplay: boolean;
    @ViewChild('ktAside', { static: true }) ktAside: ElementRef;
    @ViewChild('ktHeaderMobile', { static: true }) ktHeaderMobile: ElementRef;
    @ViewChild('ktHeader', { static: true }) ktHeader: ElementRef;

    constructor(
        private initService: LayoutInitService,
        private layout: LayoutService,
        private profileService: ProfileService,
        private router: Router,
        private alertService: AlertService
    ) {
        this.initService.init();
        this.getProfile();

    }
    dataUser: any;
    subscription: Subscription;

    ngOnInit(): void {
        // build view by layout config settings
        this.asideDisplay = this.layout.getProp('aside.display') as boolean;
        this.toolbarDisplay = this.layout.getProp('toolbar.display') as boolean;
        this.contentContainerClasses = this.layout.getStringCSSClasses('contentContainer');
        this.asideCSSClasses = this.layout.getStringCSSClasses('aside');
        this.headerCSSClasses = this.layout.getStringCSSClasses('header');
        this.headerHTMLAttributes = this.layout.getHTMLAttributes('headerMenu');
        this.subscription = this.profileService.currentData.subscribe(data => {
            this.dataUser = data;
        })
    }
    

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    newData() {
        this.profileService.changeData(this.dataUser);
    }

    ngAfterViewInit(): void {
        if (this.ktHeader) {
            for (const key in this.headerHTMLAttributes) {
                if (this.headerHTMLAttributes.hasOwnProperty(key)) {
                    this.ktHeader.nativeElement.attributes[key] =
                        this.headerHTMLAttributes[key];
                }
            }
        }
    }

    getProfile() {
        let id = Number(localStorage.getItem('user_id'));
        this.profileService.getInfo(id).subscribe((res: any) => {
            if(res.code == 200) {
                // if(!res.objects.staff) {
                //     this.alertService.fireSmall('error', "Không có nhân viên nào có tài khoản này");
                //     window.location.href="/auth/login";
                // }
                this.dataUser = res.objects ? res.objects : null;
                let name = this.dataUser?.staff?.staff?.fullName;
                let id = this.dataUser?.staff?.staff?.id;
                if(name) {
                    localStorage.setItem('name', name);
                    localStorage.setItem('id', id);
                } else {
                    localStorage.setItem('name', "ADMIN");
                }
                this.newData();
            }
            //  else if(res.code == 403) {
            //     this.router.navigate(['error/403'])
            // } else {
            //     localStorage.clear();
            //     window.location.href="/auth/login";
            // }
            
        });
    }
    
}
