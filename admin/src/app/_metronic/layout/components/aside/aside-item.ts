import permissionName from "src/app/shared/permission-file/permission-name";

interface RouteItem {
    path: string;
    title: string;
    icon: string;
    class: string;
    subMenu: RouteItem[];
    permissionName?: string;
}

export const ROUTES: RouteItem[] = [
    {
        path: 'dashboard',
        title: 'DashBoard',
        icon: '',
        class: 'bx bxs-dashboard',
        subMenu: [],
        permissionName: `ADMIN`
    },
    {
        path: '',
        title: 'Quản lý nhân viên',
        icon: './../../../../../assets/media/icons/duotune/communication/com006.svg',
        class: 'bx bxs-user-account',
        subMenu: [
            {
                path: '/acl/user',
                title: 'Nhân viên',
                icon: '',
                class: '',
                subMenu: [],
                permissionName: `ADMIN`
            },
            {
                path: '/acl/account',
                title: 'Tài khoản',
                icon: '',
                class: '',
                subMenu: [],
                permissionName: `ADMIN`
            }
        ],
        permissionName: `ADMIN`
    },
    {
        path: '',
        title: 'Quản lý phòng',
        icon: '',
        class: 'bx bx-building-house',
        subMenu: [
            {
                path: '/room/list',
                title: 'Các phòng',
                icon: '',
                class: '',
                subMenu: [],
                permissionName: `ADMIN|STAFF|RECEPTIONLIST`
            },
            {
                path: '/room/types',
                title: 'Loại phòng',
                icon: '',
                class: '',
                subMenu: [],
                permissionName: `ADMIN|STAFF|RECEPTIONLIST`
            },
        ],
        permissionName: `ADMIN|STAFF|RECEPTIONLIST`
    },
    {
        path: 'booking',
        title: 'Quản lý đặt phòng',
        icon: '',
        class: 'bx bx-bar-chart',
        subMenu: [],
        permissionName: `ADMIN|STAFF|RECEPTIONLIST`
    },
    {
        path: '',
        title: 'Quản lý dịch vụ',
        icon: '',
        class: 'bx bx-purchase-tag',
        subMenu: [
            {
                path: '/service/list',
                title: 'Dịch vụ',
                icon: '',
                class: '',
                subMenu: [],
                permissionName: `ADMIN|STAFF`
            },
            {
                path: '/service/type',
                title: 'Loại dịch vụ',
                icon: '',
                class: '',
                subMenu: [],
                permissionName: `ADMIN|STAFF`
            },
        ],
        permissionName: `ADMIN|STAFF`
    },
    {
        path: 'news',
        title: 'Quản lý bài viết',
        icon: '',
        class: 'bx bx-news',
        subMenu: [],
        permissionName: `ADMIN|STAFF`
    },
];
