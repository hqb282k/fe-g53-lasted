import { CommonService } from 'src/app/services/service';
import { Router } from '@angular/router';
import { ROUTES } from '../aside-item';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-aside-menu',
    templateUrl: './aside-menu.component.html',
    styleUrls: ['./aside-menu.component.scss'],
})
export class AsideMenuComponent implements OnInit {
    appAngularVersion: string = environment.appVersion;

    showMenu = '';
    showSubMenu = '';
    public sidebarnavItems: any[];

    constructor(
        private router: Router,
        public commonService: CommonService
    ) {}

    ngOnInit(): void {
        this.getListAside();
    }

    getListAside() {
        this.sidebarnavItems = ROUTES.filter(sidebarnavItem => sidebarnavItem);
        let url = (this.router.url.indexOf('?') >= 0) ? this.router.url.slice(0, this.router.url.indexOf('?')) : this.router.url;
        this.sidebarnavItems.forEach((sbItem, index) => {
            if(sbItem.path) {
                if(sbItem.path == url) this.showMenu = sbItem.title;
            }
            else {
                if(sbItem.submenu)
                {
                    sbItem.submenu.forEach((item:any, index:any) => {
                        if(item.path == url) {
                            this.showSubMenu = item.title;
                            this.showMenu = sbItem.title;
                        }
                    });
                }
            }
        });
    }

}
