import { CommonService } from 'src/app/services/service';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {
    // KeenThemes mock, change it to:
    hasError: boolean;
    returnUrl: string;
    isLoading$: Observable<boolean>;

    viewPass: boolean = false;

    dataError: string;

    loginForm = new FormGroup({
        email: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
        password:new FormControl(null, [Validators.required, Validators.maxLength(50)])
    });

    @ViewChild('emailInput') emailInput: ElementRef;

    validation_messages = {
        'email': [
            { type: 'required', message: 'Không được để trống ô này.' },
            { type: 'email', message: 'Sai định dạng email (VD: aa@ss.dd).' },
            { type: 'maxlength', message: 'Độ dài tối đa là 50 ký tự' }
        ],
        'password': [
            { type: 'required', message: 'Không được để trống ô này.' },
            { type: 'minlength', message: 'Mật khẩu có độ dài tối thiểu là 6 ký tự.' },
            { type: 'maxlength', message: 'Mật khẩu có độ dài tối đa là 50 ký tự.' },
        ]
    };

    // private fields
    private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

    constructor(
        private authService: AuthService,
        private route: ActivatedRoute,
        private router: Router,
        public commonService: CommonService
    ) {
        this.isLoading$ = this.authService.isLoading$;
        // redirect to home if already logged in
        if (this.authService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }
    ngAfterViewInit(): void {
        this.emailInput.nativeElement.focus();
    }

    ngOnInit(): void {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'.toString()] || '/';
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls;
    }
    get email() {
        return this.loginForm.get('email') as FormControl;
    }
    get password() {
        return this.loginForm.get('password') as FormControl;
    }

    onTrimEvent(event: any, formControl: string) {
        this.loginForm.controls[`${formControl}`].setValue(event.target.value.trim());
    }

    submit() {
        // window.location.href = '/';
        this.hasError = false;
        this.authService.isLoadingSubject.next(true);
        const loginSubscr = this.authService.login(this.f.email.value, this.f.password.value)
            .subscribe((response: any | undefined) => {
                this.dataError = '';
                if (response.code == 200 && response.objects.isStaff) {
                    localStorage.setItem('access_token', response.objects.access_token);
                    localStorage.setItem('token', '7cd11a53-1eab-4f42-a1ec-7bc609cf5e53');
                    if(response.objects.data) {
                        localStorage.setItem('user_id', response.objects.data.id);
                        window.location.href = '/';
                    } else {
                        this.dataError = 'Không tìm thấy nhân viên tương ứng';
                        localStorage.clear();
                        this.hasError = true;     
                    }
                } else {
                    this.dataError = 'Sai thông tin đăng nhập';
                    localStorage.clear();
                    this.hasError = true;
                }
                this.authService.isLoadingSubject.next(false);
            }, (error:any) => {
                this.authService.isLoadingSubject.next(false);
                this.dataError = 'Sai thông tin đăng nhập';
                localStorage.clear();
                this.hasError = true;
            });
        this.unsubscribe.push(loginSubscr);
    }

    ngOnDestroy() {
        this.unsubscribe.forEach((sb) => sb.unsubscribe());
    }
}
