import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomTableComponent } from './room-table/room-table.component';
import { PipeModule } from 'src/app/shared/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    RoomTableComponent
  ],
  exports: [
    RoomTableComponent
  ],
  imports: [
    CommonModule,
    PipeModule,
    NgbModule
  ]
})
export class RoomChildModule { }
