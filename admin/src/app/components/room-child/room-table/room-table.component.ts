import { Component, Input, OnInit } from '@angular/core';
import { BookingService } from 'src/app/services/booking.service';

@Component({
    selector: 'app-room-table',
    templateUrl: './room-table.component.html',
    styleUrls: ['./room-table.component.scss']
})
export class RoomTableComponent implements OnInit {

    @Input() listBooking: any;
    // @Input() configOrder: any = [];
    // @Input() isLoading: any = false;
    // @Input() optionCancel: any;
    // @Input() orderSelect: any;

    // @Output() selectOrder = new EventEmitter();
    // @Output() openModal = new EventEmitter();

    constructor(
        private bookingService: BookingService
    ) { }

    ngOnInit(): void {
    }

    getNameStatusBooking(status: number) {
        switch (status) {
            case 1: return 'Đã đặt';
            case 2: return 'Chờ duyệt';
            default: return 'Còn trống';
        }
    }

    getClassNameStatus(status: any) {
        switch (status) {
            case 1: return 'badge-light-success';
            case 2: return 'badge-light-warning';
            default: return 'badge-light-primary';
        }
    }

    getNameStatusRoom(id: number) {
        switch (id) {
            case 1: return 'Phòng đơn';
            case 2: return 'Phòng đôi';
            case 3: return 'Phòng vip';
            case 4: return 'Phòng khác';
            default: return ''
        }
    }

    openModal(typeModal: any, userId?: any) {
        let data: any = {
            type: typeModal
        };
        if (userId) {
            data['id'] = userId;
        }
    }

    ngOnDestroy(): void {
    }

}
