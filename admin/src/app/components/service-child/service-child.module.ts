import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableServiceComponent } from './table-service/table-service.component';
import { PipeModule } from 'src/app/shared/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    TableServiceComponent
  ],
  exports: [
    TableServiceComponent
  ],
  imports: [
    CommonModule,
    PipeModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class ServiceChildModule { }
