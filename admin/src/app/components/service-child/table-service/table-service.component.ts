import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BookingService, UserService } from 'src/app/services/service';

@Component({
  selector: 'app-table-service',
  templateUrl: './table-service.component.html',
  styleUrls: ['./table-service.component.scss']
})
export class TableServiceComponent implements OnInit {

    @Input() listBooking: any;
    // @Input() configOrder: any = [];
    // @Input() isLoading: any = false;
    // @Input() optionCancel: any;
    // @Input() orderSelect: any;

    // @Output() selectOrder = new EventEmitter();
    // @Output() openModal = new EventEmitter();

    constructor(
        private bookingService: BookingService,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void {
    }

    getNameStatusBooking(status: number) {
        switch (status) {
            case 1: return 'Đã đặt';
            case 2: return 'Chờ duyệt';
        }
    }

    getClassNameStatus(status: any) {
        switch (status) {
            case 1: return 'badge-light-success';
            case 2: return 'badge-light-warning';
        }
    }

    getNameStatusRoom(id: number) {
        switch (id) {
            case 1: return 'Phòng đơn';
            case 2: return 'Phòng đôi';
            case 3: return 'Phòng vip';
            case 4: return 'Phòng khác';
        }
    }

    openModal(content: any, userId?: any) {
        this.modalService.open(content, {
            windowClass: 'modal-account',
            size: "lg",
            centered: true
        })
    }

    formAccount = new FormGroup({
        name: new FormControl(null, Validators.required),
        user_name: new FormControl(null, Validators.required),
        email: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.maxLength(50)),
        confirm: new FormControl(null, Validators.maxLength(50)),
        roles: new FormControl(null),
        phone: new FormControl(null),
        address: new FormControl(null),
        status: new FormControl(null),
        avatar: new FormControl(null),
        user_type: new FormControl(null),
    });

    get name() { return this.formAccount.get('name') as FormControl };
    get user_name() { return this.formAccount.get('user_name') as FormControl };
    get email() { return this.formAccount.get('email') as FormControl };
    get roles() { return this.formAccount.get('roles') as FormControl };
    get phone() { return this.formAccount.get('phone') as FormControl };
    get address() { return this.formAccount.get('address') as FormControl };
    get status() { return this.formAccount.get('status') as FormControl };
    get avatar() { return this.formAccount.get('avatar') as FormControl };
    get user_type() { return this.formAccount.get('user_type') as FormControl };
    get password() { return this.formAccount.get('password') as FormControl };
    get confirm() { return this.formAccount.get('confirm') as FormControl };

    validator_value = {
        'name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'user_name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'email': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'password': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'Mật khẩu tối đa 50 ký tự.'
            },
        ],
        'confirm': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'maxlength',
                message: 'Mật khẩu tối đa 50 ký tự.'
            },
        ],
    }

}
