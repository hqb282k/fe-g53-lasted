import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService, AlertService, RoleService, CommonService } from '../../../services/service';
import { Component, ViewChild, EventEmitter, Output, Input, OnChanges, ChangeDetectorRef } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
    providers: [DatePipe]
})
export class FormComponent implements OnChanges {

    @ViewChild('content') content: any;
    listRoles: any;
    detailAccount: any;
    statusAccount: any = [
        {
            id: 1,
            name: 'Nam'
        },
        {
            id: 0,
            name: 'Nữ'
        }
    ]

    @Input() isLoading = false;
    @Input() error: string;
    @Input() typeModal: number;
    @Input() idStaff: number;

    @Output() createOrUpdate = new EventEmitter();

    formAccount = new FormGroup({
        fullName: new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z ÀÁÂÃÈÉÊẾỄỂỆÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêễệểềếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$')]),
        email: new FormControl(null, [Validators.required, Validators.email]),
        phone: new FormControl(null, [Validators.required, Validators.pattern('^0[1-9]{1}[0-9]{8}$')]),
        gender: new FormControl(1, Validators.required),
        salary: new FormControl(null, [Validators.required, Validators.min(0)]),
        birthDay: new FormControl(null),
        identityCardNumber: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{12}$')]),
    });

    get fullName() { return this.formAccount.get('fullName') as FormControl };

    get email() { return this.formAccount.get('email') as FormControl };
    get phone() { return this.formAccount.get('phone') as FormControl };
    get salary() { return this.formAccount.get('salary') as FormControl };
    get birthDay() { return this.formAccount.get('birthDay') as FormControl };
    get gender() { return this.formAccount.get('gender') as FormControl };
    get identityCardNumber() { return this.formAccount.get('identityCardNumber') as FormControl };

    validator_value = {
        'fullName': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'Trường này chỉ nhập chữ.'
            }
        ],
        'identityCardNumber': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'CCCD có độ dài 12 ký tự và là số.'
            }
        ],
        'phone': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'Số điện thoại có độ dài 10 ký tự và là số.'
            }
        ],
        'salary': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'min',
                message: 'Tiền lương tối thiểu là 0.'
            }
        ],
        'email': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'email',
                message: 'Email không đúng định dạng(Example"abc@gmail.com)'
            }
        ],
        'birthDay': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ]
    }

    constructor(
        private userService: UserService,
        private modalService: NgbModal,
        private roleService: RoleService,
        private alerService: AlertService,
        private router: Router,
        public cdk: ChangeDetectorRef,
        private datePipe: DatePipe,
        public commonService: CommonService
    ) {
    }

    ngOnChanges(): void {
    }

    ngOnInit(): void {
    }

    getDetailAccount(id: any) {
        this.userService.showUser(id).subscribe(res => {
            if (res.code == 200 && res.objects) {
                this.detailAccount = res.objects;
                this.setValueForm(res.objects);
                this.openModal();
            }else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403']);
            } else {
                this.alerService.fireSmall('error', 'Có lỗi xảy ra.');
            }
        });

    }
    getListRole() {
        this.roleService.getRoles(1, 2000).subscribe(res => {
            if (res.message == 'success' && res.objects?.length > 0) {
                this.listRoles = res.objects.filter((item: any) => item.roleName != 'CUSTOMER');
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
                this.userService.modalSubject.next({});
            } else if (res.code == 403) {
                this.router.navigate(['error/403']);
                this.userService.modalSubject.next({});
            }
        })
    }

    closeModal() {
        this.modalService.dismissAll();
    }

    onSubmit() {
        let data: any = this.formAccount.value;
        data.salary = parseInt(data.salary);
        console.log(data)
        if (data.birthDay) data.birthDay = moment(data.birthDay).format('YYYY/MM/DD');
        data.gender = this.gender.value == 0 ? false : true;
        delete data.roles;
        this.createOrUpdate.emit({
            data: data,
            id: this.detailAccount ? this.detailAccount.id : null
        })
    }

    openModal() {
        this.modalService.open(this.content, {
            windowClass: 'modal-account',
            size: "lg",
            centered: true
        }).result.then(value => {
            if(value == "close") this.createOrUpdate.emit(null);
        }, err => {
            this.createOrUpdate.emit(null);
        });
    }

    setValueForm(data: any) {
        this.formAccount.patchValue({
            fullName: data.fullName,
            email: data.email,
            phone: data.phone,
            gender: data.gender ? 1 : 0,
            salary: data.salary,
            identityCardNumber: data.identityCardNumber
        });
        console.log(this.datePipe.transform(data.birthDay,"yyyy-MM-dd"))
        this.birthDay.setValue(data.birthDay ?  this.datePipe.transform(data.birthDay,"yyyy-MM-dd") : null)
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

    checkGender() {
        if(this.gender.value == 1) return true;
        return false;
    }

}

