import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form/form.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DirectiveModule, ComponentModule } from 'src/app/shared/shared';

@NgModule({
  declarations: [
    FormComponent
  ],
  exports:[
    FormComponent
  ],
  imports: [
    CommonModule,
    ComponentModule,
    FormsModule,
    DirectiveModule,
    NgbModule,
    ReactiveFormsModule,
    NgSelectModule,
  ]
})
export class UserChildModule { }
