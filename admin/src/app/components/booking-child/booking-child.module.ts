import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { ComponentModule, PipeModule } from 'src/app/shared/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormUpdateComponent } from './form-update/form-update.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';



@NgModule({
  declarations: [
    TableComponent,
    FormUpdateComponent
  ],
  exports: [
    TableComponent,
    FormUpdateComponent
  ],
  imports: [
    CommonModule,
    ComponentModule,
    PipeModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ]
})
export class BookingChildModule { }
