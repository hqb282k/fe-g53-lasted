import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService, AlertService, RoleService, BookingService } from '../../../services/service';
import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-form-update',
  templateUrl: './form-update.component.html',
  styleUrls: ['./form-update.component.scss']
})
export class FormUpdateComponent implements OnInit {

    @ViewChild('content') content: any;
    typeModal: any;
    listRoles: any;
    detailAccount: any;
    statusAccount: any = [
        {
            id: 3,
            name: 'phòng Đơn'
        },
        {
            id: 0,
            name: 'phòng Đôi'
        },
        {
            id: 1,
            name: 'phòng Vip'
        },
        {
            id: 2,
            name: 'phòng khác'
        },
    ]

    @Input() isLoading = false;
    @Input() error:string;

    @Output() createOrUpdate = new EventEmitter();

    formAccount = new FormGroup({
        name: new FormControl(null, Validators.required),
        user_name: new FormControl(null, Validators.required),
        email: new FormControl(null, Validators.required),
        password: new FormControl(null, Validators.maxLength(50)),
        confirm: new FormControl(null, Validators.maxLength(50)),
        roles: new FormControl(null),
        phone: new FormControl(null),
        address: new FormControl(null),
        status: new FormControl(null),
        avatar: new FormControl(null),
        user_type: new FormControl(null),
    });

    get name() { return this.formAccount.get('name') as FormControl };
    get user_name() { return this.formAccount.get('user_name') as FormControl };
    get email() { return this.formAccount.get('email') as FormControl };
    get roles() { return this.formAccount.get('roles') as FormControl };
    get phone() { return this.formAccount.get('phone') as FormControl };
    get address() { return this.formAccount.get('address') as FormControl };
    get status() { return this.formAccount.get('status') as FormControl };
    get avatar() { return this.formAccount.get('avatar') as FormControl };
    get user_type() { return this.formAccount.get('user_type') as FormControl };
    get password() { return this.formAccount.get('password') as FormControl };
    get confirm() { return this.formAccount.get('confirm') as FormControl };

    validator_value = {
        'name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'user_name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'email': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'password': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'Mật khẩu tối đa 50 ký tự.'
            },
        ],
        'confirm': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'maxlength',
                message: 'Mật khẩu tối đa 50 ký tự.'
            },
        ],
    }

    constructor(
        private userService: UserService,
        private modalService: NgbModal,
        private roleService: RoleService,
        private alerService: AlertService,
        private bookingService: BookingService
    ) {
        
    }

    ngOnInit(): void {
    }

    onSubmit() {
        let data: any;
        if (this.typeModal == 3) {
            data = {
                name: this.name.value,
                email: this.email.value,
                user_name:this.user_name.value,
                password: this.password.value,
                roles: this.roles.value,
                status: this.status.value,
            };
        } else {
            data = {
                name: this.name.value,
                phone: this.phone.value,
                roles: this.roles.value,
                address: this.address.value,
                status: this.status.value,
            }
        }
        this.createOrUpdate.emit({
            data: data,
            id: this.detailAccount ? this.detailAccount.id : null
        })


    }

    openModal() {
        this.modalService.open(this.content, {
            windowClass: 'modal-account',
            size: "lg",
            centered: true
        }).result.then(value => {
            this.userService.modalSubject.next({});
        }, err => {
            this.userService.modalSubject.next({});
        });
    }

    setValueForm(data: any) {
        this.name.setValue(data.name);
        if (data.roles && data.roles.length > 0) {
            let role: any = data.roles.reduce((arrRole: any, element: any) => {
                arrRole.push(element.id);
                return arrRole;
            }, [])
            this.roles.setValue(role);
        }
        this.status.setValue(data.status);
        this.user_name.setValue(data.username);

        this.email.setValue(data.email);
        this.phone.setValue(data.phone);
        this.address.setValue(data.address);
        this.status.setValue(data.status);
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

}
