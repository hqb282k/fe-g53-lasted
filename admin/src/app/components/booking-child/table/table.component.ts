import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BookingService, UserService } from 'src/app/services/service';

@Component({
    selector: 'app-booking-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Input() listBooking: any;
    // @Input() configOrder: any = [];
    // @Input() isLoading: any = false;
    // @Input() optionCancel: any;
    // @Input() orderSelect: any;

    // @Output() selectOrder = new EventEmitter();
    // @Output() openModal = new EventEmitter();

    constructor(
        private bookingService: BookingService
    ) { }

    ngOnInit(): void {
    }

    getNameStatusBooking(status: number) {
        switch (status) {
            case 1: return 'Đã đặt';
            case 2: return 'Chờ duyệt';
        }
    }

    getClassNameStatus(status: any) {
        switch (status) {
            case 1: return 'badge-light-success';
            case 2: return 'badge-light-warning';
        }
    }

    getNameStatusRoom(id: number) {
        switch (id) {
            case 1: return 'Phòng đơn';
            case 2: return 'Phòng đôi';
            case 3: return 'Phòng vip';
            case 4: return 'Phòng khác';
        }
    }

}
