import { Routes } from '@angular/router';

const Routing: Routes = [
    {
        path: 'dashboard',
        loadChildren: () =>
          import('../modules/dashboard/dashboard.module').then((m) => m.DashboardModule),
      },
    {
        path: 'acl',
        loadChildren: () =>
            import('../modules/user-management/user-management.module').then((m) => m.UserManagementModule),
    },
    {
        path: 'profile',
        loadChildren: () =>
            import('../modules/profile/profile.module').then((m) => m.ProfileModule),
    },
    {
        path: 'room',
        loadChildren: () =>
            import('../modules/room-management/room-management.module').then((m) => m.RoomManagementModule),
    },
    {
        path: 'booking',
        loadChildren: () =>
            import('../modules/booking-management/booking-management.module').then((m) => m.BookingManagementModule),
    },
    {
        path: 'service',
        loadChildren: () =>
            import('../modules/service-facility/service-facility.module').then((m) => m.ServiceTypeModule),
    },
    {
        path: 'news',
        loadChildren: () =>
            import('../modules/news/news.module').then((m) => m.NewsModule),
    },
    {
        path: 'error',
        loadChildren: () =>
            import('../modules/errors/errors.module').then((m) => m.ErrorsModule),
    },
    {
        path: '',
        redirectTo:'room',
        pathMatch:'full'
    },
];

export { Routing };
