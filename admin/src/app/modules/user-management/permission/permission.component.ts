import { Component, OnInit } from '@angular/core';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { tap, delay } from 'rxjs/operators';
import { FormGroup, FormControl } from '@angular/forms';
import permissionName from 'src/app/shared/permission-file/permission-name';
import { CommonService, PermissionService } from 'src/app/services/service';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;

    permissions: any = [];
    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };
    groupType: any = [];

    permission = permissionName.permission; // get permission name to check action

    formSearch = new FormGroup({
        name: new FormControl(null),
        description: new FormControl(null),
        group_type: new FormControl(null),
    });

    constructor(
        private permissionService: PermissionService,
        public commonService: CommonService
    ) {
    }

    ngOnInit(): void {
        this.getGroupType();
        this.getListPermissions(1, 20, this.formSearch.value);
    }

    getListPermissions(page: any, pageSize: any, filter: any) {
        this.isLoading = true;
        let queryParams = {
            page: this.paging.page ? this.paging.page : null,
            pageSize: this.paging.pageSize ? this.paging.pageSize : null,
            name: filter.name ? filter.name : null,
            group_by: filter.group_by ? filter.group_by : null
        }
        this.commonService.paramURL(queryParams, 'acl/permission');
        this.permissionService.getPermissions(page, pageSize, filter,true)
            .pipe(delay(1000), tap(() => this.isLoading = false))
            .subscribe(result => {
                if (result.status == 200) {
                    this.permissions = result.data;
                    this.paging = result.paging;
                }
            });
    }

    getGroupType() {
        this.permissionService.getGroupType().subscribe((res: any) => {
            if (res.status == 200) {
                this.groupType = res.data;
            }
        })
    }

    onSubmit() {
        this.paging.page = 1;
        this.getListPermissions(1, 20, this.formSearch.value);
    }

    cancel() {
        this.formSearch.reset();
        this.getListPermissions(1, 20, this.formSearch.value);
    }

    changePage($event: any) {
        this.paging = $event;
        this.getListPermissions(this.paging.page, this.paging.pageSize, this.formSearch.value);
    }

    checkNameGroup(id: any) {
        if (this.groupType?.length > 0) {
            let value = this.groupType.filter((item: any) => item?.id == id);
            if (value.length > 0) {
                return value[0]?.name;
            }
            return '';
        }
        return '';
    }

}
