import { tap } from 'rxjs/operators';
import { AlertService, CommonService, UserService } from 'src/app/services/service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import permissionName from 'src/app/shared/permission-file/permission-name';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

    isLoading = false;
    listsUser: any = [];
    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };
    filter: any = {};
    groupType: any = 0;
    email: string = '';

    userIds: any = [];
    role: any;
    permission = permissionName.user; // get permission name to check action


    staffNotAccount: any = [];

    userId: number = Number(localStorage.getItem('user_id'));



    constructor(
        private userService: UserService,
        public commonService: CommonService,
        private alertService: AlertService,
        private modalService: NgbModal,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
        this.getStaffNoAccount();
        this.userService.modalSubject.next({});
    }

    getListsUser(paging: any, filter: any, role: any) {
        this.isLoading = true;
        let queryParam = {
            page: this.paging.page ? this.paging.page : null,
            pageSize: this.paging.pageSize ? this.paging.pageSize : null,
            // email: this.email ? this.email : null,
            // group_type: (this.groupType && this.groupType != 0) ? this.groupType : null
        };


        this.commonService.paramURL(queryParam, '/acl/account');
        this.userService.getListsUser(paging, filter, role)
            .subscribe(result => {
                if (result.code == 200) {
                    this.listsUser = result.objects;
                    this.paging.page = result?.paging.page + 1;
                    this.paging.total = result?.paging.total;
                } else if (result.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if (result.code == 403) {
                    this.router.navigate(['error/403'])
                }
                this.isLoading = false;

            });
    }

    getStaffNoAccount() {
        let page = {
            page: 1,
            pageSize: 1000
        }
        this.userService.getListsUser(page, null, 'admin/getStaffNoAccount')
            .subscribe(result => {
                if (result.code == 200) {
                    this.staffNotAccount = result.objects;
                } else if (result.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if (result.code == 403) {
                    this.router.navigate(['error/403'])
                }
            });
    }

    onSubmit() {
        this.filter['group_type'] = this.groupType;
        this.filter['email'] = this.email;
        this.paging.page = 1;
        this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
    }

    changePage($event: any) {
        this.paging = $event;
        this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
    }

    handleSelectOrderItem($event: any, statusAll: boolean, id?: any) {
        if (statusAll) {
            if ($event.target.checked) {
                this.userIds = this.listsUser.reduce((ids: any, element: any) => {
                    ids.push(element.id);
                    return ids;
                }, []);
            } else {
                this.userIds = [];
            };
        } else {
            if (this.userIds?.length > 0) {
                let index = this.userIds.findIndex((item: any) => item == id);
                index < 0 ? this.userIds.push(id) : this.userIds.splice(index, 1);
            } else {
                $event.target.checked ? this.userIds.push(id) : [];
            }
        }
    }

    handleInactiveAccount(statusAll: any, id?: any) {
        if (statusAll && this.userIds?.length <= 0) {
            this.alertService.fireSmall('error', 'Vui lòng chọn những account bạn muốn Inactive.');
            return;
        }
        let userId: any = statusAll ? this.userIds : id;
        this.userService.inActiveAccount(statusAll, userId).subscribe(res => {
            if (res.status == 200) {
                this.alertService.fireSmall('success', 'Inactive tài khoản thành công.');
                this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
            } else {
                this.alertService.fireSmall('error', res.message);
            }
        });
    }

    handleActiveAccount(statusAll: any, id: any) {
        this.isLoading = true;
        if (statusAll && this.userIds?.length <= 0) {
            this.alertService.fireSmall('error', 'Vui lòng chọn những account bạn muốn Active.');
            this.isLoading = false;
            return;
        }
        let userId: any = statusAll ? this.userIds : id;
        this.userService.activeAccount(statusAll, userId)
            .pipe(tap(() => this.isLoading = false))
            .subscribe(res => {
                if (res.status == 200) {
                    this.alertService.fireSmall('success', 'Active tài khoản thành công.');
                    this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
                } else {
                    this.alertService.fireSmall('error', res.message);
                }
                this.isLoading = false;
            });
    }

    error: any;
    handleCreateOrUpdate($event: any) {
        if ($event) {
            this.isLoading = true;
            this.userService.createOrUpdateAccount($event.data, $event?.id)
                .pipe(tap(() => this.isLoading = false))
                .subscribe(res => {
                    if (res.code == 200) {
                        this.paging.page = 1;
                        this.error = null;
                        this.alertService.fireSmall('success', 'Tạo tài khoản thành công.');
                        this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
                        this.userService.modalSubject.next({});
                    } else if (res.code == 401) {
                        localStorage.clear();
                        this.router.navigate(['/auth/login']);
                    } else if (res.code == 403) {
                        this.router.navigate(['error/403'])
                    } else {
                        this.error = res.message;
                        this.alertService.fireSmall('error', res.message);
                    }
                })
        }
    }

    typeModal: any;
    listRoles: any = [
        {
            id: 2,
            name: "Nhân viên"
        },
        {
            id: 3,
            name: "Lễ tân"
        },
    ];
    detailAccount: any;
    idDetail: any;
    statusAccount: any = [
        {
            id: 0,
            name: 'Nam'
        },
        {
            id: 1,
            name: 'Nữ'
        }
    ]

    formAccount = new FormGroup({
        username: new FormControl(null, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$'), Validators.minLength(6), Validators.maxLength(20)]),
        password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
        repass: new FormControl(null, [Validators.required, Validators.minLength(6)],),
        status: new FormControl(2),
        id: new FormControl(null, Validators.required),
        roles: new FormControl(null, Validators.required)
    });

    get username() { return this.formAccount.get('username') as FormControl };
    get status() { return this.formAccount.get('status') as FormControl };
    get password() { return this.formAccount.get('password') as FormControl };
    get repass() { return this.formAccount.get('repass') as FormControl };
    get id() { return this.formAccount.get('id') as FormControl };
    get roles() { return this.formAccount.get('roles') as FormControl };

    validator_value = {
        'username': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'Tên đăng nhập chỉ chứa chữ và số'
            },
            {
                type: 'minlength',
                message: 'Tên đăng nhập tối thiểu 6 ký tự.'
            },
            {
                type: 'maxlength',
                message: 'Tên đăng nhập tối đa 20 ký tự.'
            },
        ],
        'password': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'mật khẩu tối thiểu 6 ký tự.'
            },
        ],
        'id': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
        ],
        'roles': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'email': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'repass': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'Mật khẩu thiểu 6 ký tự.'
            },
        ]
    }

    listStatus = [
        {
            id: 1,
            name: "Hoạt động",
        },
        {
            id: 2,
            name: "Dừng hoạt động",
        },
    ]

    loadingForm = false;

    onSubmitForm() {
        this.loadingForm = true;
        let data: any = this.formAccount.value;

        if (this.repass.value != this.password.value) {
            this.error = "Mật khẩu và mật khẩu xác thực không giống nhau. Vui lòng nhập lại!";
            this.loadingForm = false;
            return;
        }

        if (data.status == 1) {
            data.isActive = true;
        } else {
            data.isActive = false;
        }
        delete data.status;
        delete data.repass;

        if (data.id) {
            data.staff = {
                id: data.id
            };
            delete data.id;
        }

        if (data.roles) {
            data.role = {
                id: data.roles
            };
            delete data.roles;
        }
        this.userService.createAccount(data, this.idDetail).subscribe(res => {
            if (res.code == 200) {
                if (this.idDetail) {
                    this.alertService.fireSmall('success', "Chỉnh sửa tài khoản thành công");
                } else {
                    this.alertService.fireSmall('success', "Tạo tài khoản thành công");
                }
                this.modalService.dismissAll();
                this.getListsUser(this.paging, this.filter, 'admin/getAllAccount');
                this.getStaffNoAccount();

            } else if (res.code == 106) {
                this.alertService.fireSmall('error', "Tên đăng nhập đã tồn tại");
            } else if (res.code = 103) {
                this.alertService.fireSmall('error', "Mật khẩu không đúng định dạng");
            } else if (res.code = 104) {
                this.alertService.fireSmall('error', "Email không đúng định dạng");
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }
            this.loadingForm = false;
        })
    }

    openModal(typeModal: any, userId?: any) {
        this.formAccount.reset();
        this.detailAccount = null;
        this.idDetail = null;
        if (userId) {
            this.userService.getAccountId(userId).subscribe(res => {
                if (res.code == 200 && res.objects) {
                    this.detailAccount = res.objects;
                    this.idDetail = this.detailAccount?.id;
                    this.username.setValue(this.detailAccount?.username);
                    this.roles.setValue(this.detailAccount?.role?.id);
                    this.status.setValue(this.detailAccount?.isActive ? 1 : 2);
                    this.id.setValue(this.detailAccount?.staff?.id);
                    this.password.clearValidators();
                    this.repass.clearValidators();
                    this.modalService.open(typeModal, {
                        windowClass: 'modal-account',
                        size: "lg",
                        centered: true
                    });
                } else {
                    this.alertService.fireSmall('error', 'Có lỗi xảy ra.');
                }
            })
        } else {
            this.password.setValidators(Validators.required);
            this.repass.setValidators(Validators.required);
            this.modalService.open(typeModal, {
                windowClass: 'modal-account',
                size: "lg",
                centered: true
            });
        }

    }

    statusPass = false;
    checkPass(event: any) {
        if (event.target.value) {
            let a = event.target.value.trim();
            if (a != this.password.value) this.statusPass = true;
            else this.statusPass = false;
        }
        return this.statusPass;
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

    getRoleName(id:any) {
        if(id == 1) return "ADMIN";
        else if(id == 2) return "Nhân viên";
        else if(id == 3) return "Lễ tân"
        else return "";
    }

}
