import { tap } from 'rxjs/operators';
import { AlertService, CommonService, UserService} from 'src/app/services/service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import permissionName from 'src/app/shared/permission-file/permission-name';
import { Router } from '@angular/router';
import { FormComponent } from 'src/app/components/user-child/form/form.component';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    isLoading = false;
    listsUser: any = [];
    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };
    filter: any = {};
    groupType: any = 0;
    emailSearch: any;
    nameSearch: any;

    id: number = Number(localStorage.getItem('id'));

    userIds: any = [];
    role:any;
    permission = permissionName.user; // get permission name to check action


    constructor(
        private userService: UserService,
        public commonService: CommonService,
        private alertService: AlertService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getListsUser(this.paging, this.filter, 'system/getAllStaff');
    }

    getListsUser(paging: any, filter: any, role:any) {
        this.isLoading = true;
        let queryParam = {
            page: this.paging.page ? this.paging.page : null,
            pageSize: this.paging.pageSize ? this.paging.pageSize : null,
            email: this.emailSearch ? this.emailSearch : null,
            name: this.nameSearch ? this.nameSearch : null
        };

        this.commonService.paramURL(queryParam, '/acl/user');
        this.userService.getListsUser(paging, filter,role)
            .pipe(tap(() => this.isLoading = false))
            .subscribe(result => {
                if (result.code == 200) {
                    this.listsUser = result.objects;
                    this.paging.page = result.paging.page + 1;
                    this.paging.pageSize = result.paging.pageSize;
                    this.paging.total = result.paging.total;
                    this.typeModal = 0;
                    this.idStaff = 0;
                } else if(result.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if(result.code == 403) {
                    this.router.navigate(['error/403'])
                }
                this.isLoading = false;
            });
    }

    onSubmit() {
        this.filter['nameSearch'] = this.nameSearch;
        this.filter['emailSearch'] = this.emailSearch;
        this.paging.page = 1;
        this.getListsUser(this.paging, this.filter, 'system/getAllStaff');
    }

    changePage($event: any) {
        this.paging = $event;
        this.getListsUser(this.paging, this.filter, 'system/getAllStaff');
    }

    handleSelectOrderItem($event: any, statusAll: boolean, id?: any) {
        if (statusAll) {
            if ($event.target.checked) {
                this.userIds = this.listsUser.reduce((ids: any, element: any) => {
                    ids.push(element.id);
                    return ids;
                }, []);
            } else {
                this.userIds = [];
            };
        } else {
            if (this.userIds?.length > 0) {
                let index = this.userIds.findIndex((item: any) => item == id);
                index < 0 ? this.userIds.push(id) : this.userIds.splice(index, 1);
            } else {
                $event.target.checked ? this.userIds.push(id) : [];
            }
        }
    }

    handleInactiveAccount(statusAll: any, id?: any) {
        if (statusAll && this.userIds?.length <= 0) {
            this.alertService.fireSmall('error', 'Vui lòng chọn những account bạn muốn Inactive.');
            return;
        }
        let userId: any = statusAll ? this.userIds : id;
        this.userService.inActiveAccount(statusAll, userId).subscribe(res => {
            if (res.status == 200) {
                this.alertService.fireSmall('success', 'Inactive tài khoản thành công.');
                this.getListsUser(this.paging, this.filter, 'getAllStaff');
            } else {
                this.alertService.fireSmall('error', res.message);
            }
        });
    }

    handleActiveAccount(statusAll: any, id: any) {
        this.isLoading = true;
        if (statusAll && this.userIds?.length <= 0) {
            this.alertService.fireSmall('error', 'Vui lòng chọn những account bạn muốn Active.');
            this.isLoading = false;
            return;
        }
        let userId: any = statusAll ? this.userIds : id;
        this.userService.activeAccount(statusAll, userId)
        .pipe(tap(() => this.isLoading = false))
        .subscribe(res => {
            if (res.status == 200) {
                this.alertService.fireSmall('success', 'Active tài khoản thành công.');
                this.getListsUser(this.paging, this.filter, 'getAllStaff');
            } else {
                this.alertService.fireSmall('error', res.message);
            }
            this.isLoading = false;
        });
    }

    idStaff: number = 0;
    typeModal = 0;
    @ViewChild(FormComponent) form: FormComponent;
    openModal(typeModal: any, userId?: any) {
        this.form.formAccount.reset();
        if (typeModal) {
            if (userId) {
                this.form.getDetailAccount(userId);
            } else {
                this.form.openModal();
            }
        } 
    }

    error: any;
    handleCreateOrUpdate($event: any) {
        if ($event) {
            this.isLoading = true;
            this.userService.createOrUpdateAccount($event.data, $event?.id)
            .pipe(tap(() => this.isLoading = false))
            .subscribe(res => {
                if (res.code == 200) {
                    this.paging.page = 1;
                    this.error = null;
                    this.alertService.fireSmall('success', `${$event.id ? 'Chỉnh sửa thành công': 'Tạo mới thành công'}`);
                    this.getListsUser(this.paging, this.filter, 'system/getAllStaff');
                    this.form.closeModal();
                    this.typeModal = 0;
                    this.idStaff = 0;
                } else if(res.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if(res.code == 403) {
                    this.router.navigate(['error/403'])
                } else if(res.code == 107) {
                    this.alertService.fireSmall('error', "Số điện thoại đã tồn tại");
                } else if(res.code == 110) {
                    this.alertService.fireSmall('error', "Căn cước công dân đã tồn tại");
                } else if(res.code == 109) {
                    this.alertService.fireSmall('error', "Email đã tồn tại");
                } else {
                    this.alertService.fireSmall('error', "Có lỗi xảy ra khi nhập dữ liệu");
                }
                this.isLoading = false;
                
            })
        } else {
            this.typeModal = 0;
            this.idStaff = 0;
        }
    }

    ngOnDestroy(): void {
    }
}
