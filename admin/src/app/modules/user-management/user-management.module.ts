import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleComponent } from './role/role.component';
import { RouterModule, Routes } from '@angular/router';
import { PermissionComponent } from './permission/permission.component';
import { UserComponent } from './user/user.component';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserChildModule } from 'src/app/components/user-child/user-child.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PermissionGuard } from 'src/app/shared/guard/permission.guard';
import { PipeModule } from 'src/app/shared/shared';
import { AccountComponent } from './account/account.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'user',
                canActivate:[PermissionGuard],
                component: UserComponent,
                data:{
                    role: "ADMIN"
                }
                
            },
            {
                path: 'account',
                canActivate:[PermissionGuard],
                component: AccountComponent,
                data:{
                    role: "ADMIN"
                }
            },
            // {
            //     path: 'account',
            //     canActivate:[PermissionGuard],
            //     component: AccountComponent
            // }
        ]
    }
]

@NgModule({
  declarations: [
    RoleComponent,
    UserComponent,
    PermissionComponent,
    AccountComponent
  ],
  imports: [
    CommonModule,
    ComponentModule,
    ReactiveFormsModule,
    FormsModule,
    PipeModule,
    NgSelectModule,
    NgSelectModule,
    UserChildModule,
    NgbModule,
    RouterModule.forChild(routes)

  ]
})
export class UserManagementModule { }
