import { tap } from 'rxjs/operators';
import { CommonService, RoleService } from 'src/app/services/service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;

    listRoles: any = [];
    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    permission = permissionName.role; // get permission name to check action

    formSearch = new FormGroup({
        name: new FormControl(null),
        group_type: new FormControl(null),
        permission: new FormControl(null),
    });

    constructor(
        private roleService: RoleService,
        public commonService: CommonService
    ) {}

    ngOnInit(): void {
        this.getRoles(1);

    }

    getRoles(page: any) {
        this.isLoading = true;
        let queryParams = {
            page: this.paging.page ? this.paging.page : null,
            pageSize: this.paging.pageSize ? this.paging.pageSize : null,
            // name: filter.name ? filter.name : null,
            // group_by: filter.group_by ? filter.group_by : null
        }
        this.commonService.paramURL(queryParams, 'acl/role');
        this.roleService.getRoles(page)
            .pipe(tap(() => this.isLoading = false))
            .subscribe(result => {
                if (result.status == 200) {
                    this.listRoles = result.data;
                    this.paging = result.paging;
                }
            });
    }

    onSubmit() {
        // this.getRoles(1, this.formSearch.value);
    }

    cancel() {
        this.formSearch.reset();
        this.getRoles(1);
    }

    changePage($event: any) {
        this.paging = $event;
        this.getRoles(this.paging.page);
    }

}
