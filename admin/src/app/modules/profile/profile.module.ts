import { ComponentModule } from './../../shared/component/component.module';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';

const routes: Routes = [
    {
        path: '',
        component: ProfileComponent,
        children: [
            {
                path: 'overview',
                loadChildren: () =>
                    import('./overview/overview.module').then((m) => m.OverviewModule),
            },
            {
                path: '',
                redirectTo:'overview',
                pathMatch:'full'
            },
        ]
    },
]

@NgModule({
    declarations: [
    ProfileComponent
  ],
    imports: [
        CommonModule,
        ComponentModule,
        RouterModule.forChild(routes)
    ]
})
export class ProfileModule { }
