import { ProfileService } from '../../services/profile.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    isLoading = true;
    profileInfo: any;
    noAvatar = 'assets/images/no-avatar.png';
    id: any;

    constructor(
        private profileService: ProfileService
    ) { }
    
    ngOnInit(): void {
    }


    // ngOnDestroy(): void {

    // }


}
