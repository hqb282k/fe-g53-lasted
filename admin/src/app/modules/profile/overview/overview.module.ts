import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowComponent } from './show/show.component';
import {RouterModule, Routes} from "@angular/router";
import { EditComponent } from './edit/edit.component';
import { ProfileDetailComponent } from './edit/profile-detail/profile-detail.component';
import { ProfilePasswordComponent } from './edit/profile-password/profile-password.component';
import { ComponentModule } from 'src/app/shared/shared';

const routes: Routes = [
    {
        path: '',
        component: ShowComponent
    },
]

@NgModule({
    declarations: [
        ShowComponent,
        EditComponent,
        ProfileDetailComponent,
        ProfilePasswordComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        ComponentModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ]
})
export class OverviewModule { }
