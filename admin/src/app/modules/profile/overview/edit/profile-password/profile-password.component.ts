import { CommonService } from 'src/app/services/common.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import {ProfileService} from "src/app/services/profile.service";
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';

@Component({
    selector: 'app-profile-password',
    templateUrl: './profile-password.component.html',
    styleUrls: ['./profile-password.component.scss']
})
export class ProfilePasswordComponent implements OnChanges {

    @Input() isLoading = false;
    @Input() profileInfo: any;
    showChangePasswordForm: boolean = false;
    checkConfirm = false;

    username: any;

    formPassword = new FormGroup({
        oldPass: new FormControl(null, Validators.required),
        newPass: new FormControl(null, Validators.required),
        password_confirm: new FormControl(null, Validators.required)
    });

    get oldPass() {return this.formPassword.get('oldPass') as FormControl};
    get newPass() {return this.formPassword.get('newPass') as FormControl};
    get password_confirm() {return this.formPassword.get('newPass') as FormControl};

    validator_value = {
        'oldPass': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'mật khẩu tối thiểu 6 ký tự.'
            },
        ],
        
        'newPass': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'mật khẩu tối thiểu 6 ký tự.'
            },
        ],
       
        'password_confirm': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'minlength',
                message: 'Mật khẩu thiểu 6 ký tự.'
            },
        ]
    }

    constructor(
        public commonService: CommonService,
        private profileService: ProfileService,
        private router: Router,
        private alertService: AlertService
    ) { }
    ngOnChanges(changes: SimpleChanges): void {
        if(this.profileInfo) {
            this.username = this.profileInfo?.staff?.username;
        }
    }

    ngOnInit(): void {
    }

    togglePasswordForm(show: boolean) {
        this.showChangePasswordForm = show;
    }

    statusPass = false;
    checkPass(event: any) {
        if(event.target.value) {
           let a = event.target.value.trim();
           if(a != this.newPass.value) this.statusPass = true;
           else this.statusPass = false;
        }
        return this.statusPass;
    }

    savePassword() {
        if(this.password_confirm.value.trim() !== this.newPass.value.trim()) {
            this.checkConfirm = true;
            return;
        }
        let body = {
            "objectRequest":{
                "username": this.username,
                "oldPass":this.oldPass.value,
                "newPass":this.newPass.value
            }
        }
        this.profileService.changePassword(body).subscribe(res => {
            if(res.status == 200) {
                this.alertService.fireLoad('success','Cập nhật mật khẩu thành công').then(rs => {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                });
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else if(res.code == 120) {
                this.alertService.fireSmall('error', "Mật khẩu cũ không tồn tại");
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại sau.");
            }
        })
    }

}
