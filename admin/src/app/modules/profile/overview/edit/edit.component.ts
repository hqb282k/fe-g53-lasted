import { tap } from 'rxjs/operators';
import { ProfileService, AlertService, UserService } from '../../../../services/service';
import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html'
})
export class EditComponent implements OnChanges {

    isLoading = false;

    @Input() profileInfo: any ;
    @Output() changeProfile = new EventEmitter();
    @Output() changePass = new EventEmitter();

    id: any;

    constructor(
        private profileService: ProfileService,
        private alertService: AlertService,
        private router: Router,
        private userService: UserService
    ) { }
    ngOnChanges(changes: SimpleChanges): void {
    }
    
    ngOnInit(): void {
    }

    // getInfo() {
    //     this.profileService.getInfo(this.id).subscribe((res: any) => {
    //         if(res.code == 200) {
    //             this.profileInfo = res.objects;
    //         }
    //     });
    // }

    // getInfo() {
    //     this.profileService.getInfo().subscribe(result => {
    //             if (result.status === 200 && result.data) {
    //                 this.profileService.profileSubject.next(result.data);
    //             }
    //         });
    // }

    handleUpdateProfile($event: any) {
        if ($event) {
            let body = {
                "objectRequest": $event
            }
            this.profileService.updateProfile(body)
            .pipe(tap(() => this.isLoading = false))
            .subscribe(res => {
                if (res.code == 200) {
                    window.location.href = 'profile'
                    
                } else if(res.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if(res.code == 403) {
                    this.router.navigate(['error/403'])
                } else if(res.code == 107) {
                    this.alertService.fireSmall('error', "Số điện thoại đã tồn tại");
                } else if(res.code == 110) {
                    this.alertService.fireSmall('error', "Căn cước công dân đã tồn tại");
                } else if(res.code == 109) {
                    this.alertService.fireSmall('error', "Email đã tồn tại");
                } else {
                    this.alertService.fireSmall('error', "Có lỗi xảy ra khi nhập dữ liệu");
                }
            })
        }
    }

}
function OutPut() {
    throw new Error('Function not implemented.');
}

