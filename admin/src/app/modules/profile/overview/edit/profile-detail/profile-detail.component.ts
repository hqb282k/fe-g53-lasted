import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import {AlertService, CommonService, ProfileService,} from '../../../../../services/service'
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-profile-detail',
    templateUrl: './profile-detail.component.html',
    styleUrls: ['./profile-detail.component.scss'],
    providers: [DatePipe]
})
export class ProfileDetailComponent implements OnInit{

    @Input() isLoading = false;
    @Input() profileInfo: any;

    @Output() save = new EventEmitter();

    formDetail = new FormGroup({
        fullName: new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$')]),
        email: new FormControl(null, [Validators.required, Validators.email]),
        phone: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{10}$')]),
        birthDay: new FormControl(null,Validators.required),
        identityCardNumber: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{10,12}$')]),
    });

    get fullName() {return this.formDetail.get('fullName') as FormControl};
    get email() {return this.formDetail.get('email') as FormControl};
    get birthDay() { return this.formDetail.get('birthDay') as FormControl };
    get identityCardNumber() {return this.formDetail.get('identityCardNumber') as FormControl};
    get phone() {return this.formDetail.get('phone') as FormControl};

    validator_value = {
        'fullName': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'Trường này chỉ nhập chữ.'
            }
        ],
        'identityCardNumber': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'CCCD có độ dài 10 đến 12 ký tự và là số.'
            }
        ],
        'phone': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'Số điện thoại có độ dài 10 ký tự và là số.'
            }
        ],
        'salary': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'min',
                message: 'Tiền lương tối thiểu là 0.'
            }
        ],
        'email': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'email',
                message: 'Email không đúng định dạng(Example"abc@gmail.com)'
            }
        ],
        'birthDay': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ]
    }

    constructor(
        private router: Router,
        private profileService: ProfileService,
        public commonService: CommonService,
        private datePipe: DatePipe,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        
    }
    ngOnChanges() {
        if(this.profileInfo) {
            this.fullName.setValue(this.profileInfo?.staff?.staff?.fullName);
            this.email.setValue(this.profileInfo?.staff?.staff?.email);
            this.phone.setValue(this.profileInfo?.staff?.staff?.phone);
            this.identityCardNumber.setValue(this.profileInfo?.staff?.staff?.identityCardNumber);
            this.birthDay.setValue(this.profileInfo?.staff?.staff?.birthDay ?  this.datePipe.transform(this.profileInfo?.staff?.staff?.birthDay,"yyyy-MM-dd") : null)
        }
       
    }

    saveProfile() {
        if(this.formDetail.invalid) {
            this.alertService.fireSmall('error', "Sai thông tin chỉnh sửa");
            return;
        }
        this.save.emit({
            "isStaff":true,
            staff: this.formDetail.value
        });
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

}
