import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import {ProfileService} from "../../../../services/profile.service";

@Component({
    selector: 'app-show',
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

    profileInfo: any;

    isLoading = false;

    id: any;

    edit = false;

    constructor(
        private profileService: ProfileService,
        public commonService: CommonService
    ) { }
    
    ngOnInit(): void {
        this.id = Number(localStorage.getItem('user_id'));
        this.getInfo();
    }

    getInfo() {
        this.profileService.getInfo(this.id).subscribe((res: any) => {
            if(res.code == 200) {
                this.profileInfo = res.objects;
            }
        });
    }

    editProfile() {
        this.edit = true;
    }

    changeProfile(e: any) {

    }

    changePass(e: any) {

    }
 }
