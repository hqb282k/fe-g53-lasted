import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import moment from 'moment';
import {
    ChartComponent,
    ApexAxisChartSeries,
    ApexChart,
    ApexXAxis,
    ApexDataLabels,
    ApexStroke,
    ApexMarkers,
    ApexYAxis,
    ApexGrid,
    ApexTitleSubtitle,
    ApexLegend,
    ApexNonAxisChartSeries,
    ApexResponsive
} from "ng-apexcharts";
import { delay, forkJoin, pipe, tap } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { BookingService } from 'src/app/services/booking.service';
import { CommonService } from 'src/app/services/common.service';
import { RoomService } from 'src/app/services/room.service';


export type ChartOptions = {
    series: ApexNonAxisChartSeries;
    chart: ApexChart;
    responsive: ApexResponsive[];
    labels: any;
};

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
    @ViewChild("chart") chart: ChartComponent;
    public chartOptions: Partial<any>;
    public chartOptions1: Partial<any>;

    isLoading = false;

    listInCome: any = [];
    listExpense: any = [];
    listPassengers: any = [];

    time: any = [moment().format("DD/MM/YYYY")];

    times = moment().startOf('month');

    valueExpense: any = [];
    valueIncome: any = [];
    valuePassenger: any = [];

    totalIncomes = 0;
    totalExpense = 0;

    listType: any;

    isReset = false;

    constructor(
        private alertService: AlertService,
        private bookingService: BookingService,
        public commonService: CommonService,
        private roomService: RoomService,
        private router: Router
    ) {

    }
    ngOnInit(): void {
        let start: any = moment().subtract(7, "days")
        let end: any = moment()
        let days = end.diff(start, "days");

        for (let i = 0; i <= days; i++) {
            this.time.push(start.format("DD/MM/YYYY"));
            start = start.add(1, "days");
        }
        this.getRoomType();
        this.getPriceSale({ fromTime: moment().subtract(7, "days").format("YYYY-MM-DD"), toTime: moment().format("YYYY-MM-DD") });
        this.getPassengers({ fromTime: moment().subtract(7, "days").format("YYYY-MM-DD"), toTime: moment().format("YYYY-MM-DD") });
    }

    handleDate($event: any) {

        if ($event) {
            this.time = []
            let start: any = moment($event.start)
            let end: any = moment($event.end)
            let days = end.diff(start, "days");

            for (let i = 0; i <= days; i++) {
                this.time.push(start.format("DD/MM/YYYY"));
                start = start.add(1, "days");
            }
            if (this.commonService.checkPermission('ADMIN')) {
                this.getPriceSale({ fromTime: $event.start, toTime: $event.end })
            }

        }
    }

    getRoomType() {
        this.isLoading = true;
        this.roomService.getRoomType({ page: 1, pageSize: 100 }).subscribe(res => {
            if (res.code == 200) {
                this.listType = res.objects;
                this.passengers();
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }

            this.isLoading = false;
        })
    }

    money() {
        this.chartOptions1 = {
            series: [
                {
                    name: "Tiền chi dịch vụ",
                    data: this.valueExpense
                },
                {
                    name: "Tiền lương",
                    data: this.valueIncome
                }
            ],
            chart: {
                type: "line"
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 5,
                curve: "straight",
                dashArray: [0, 8, 5]
            },
            title: {
                text: "Doanh thu",
                align: "left"
            },
            legend: {
                tooltipHoverFormatter: function (val: any, opts: any) {
                    return (
                        val +
                        " - <strong>" +
                        opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] +
                        "</strong>"
                    );
                }
            },
            markers: {
                size: 0,
                hover: {
                    sizeOffset: 6
                }
            },
            xaxis: {
                labels: {
                    trim: false
                },
                categories: this.time
            },
            tooltip: {
                y: [
                    {
                        title: {
                            formatter: function (val: any) {
                                return val + " (mins)";
                            }
                        }
                    },
                    {
                        title: {
                            formatter: function (val: any) {
                                return val + " per session";
                            }
                        }
                    },
                    {
                        title: {
                            formatter: function (val: any) {
                                return val;
                            }
                        }
                    }
                ]
            },
            grid: {
                borderColor: "#f1f1f1"
            }
        }
    }

    passengers() {
        let name = this.listType.map((item: any) => item.roomTypeName);
        this.chartOptions = {
            series: [10, 2, 2],
            chart: {
                type: "pie"
            },
            title: {
                text: "Khách đặt phòng",
                align: "center"
            },
            labels: name,
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            position: "bottom"
                        }
                    }
                }
            ]
        };
    }

    getExpense(filter?: any) {
        this.bookingService.getExpense(filter);
    }

    getPriceSale(filter?: any) {
        // if (this.commonService.checkPermission('ADMIN')) {
            const expense = this.bookingService.getExpense(filter);
            const income = this.bookingService.getIncome(filter);
            forkJoin([expense, income]).subscribe((res: any) => {
                if (res[0]?.code == 200) {
                    this.listExpense = res[0].objects?.data?.map((item: any) => {
                        item.date = moment(item.date).format("DD/MM/YYYY");
                        return item;
                    });
                    console.log(this.listExpense)
                    this.valueExpense = this.time.reduce((arr: any, item: any) => {
                        let element: any = this.listExpense?.find((e: any) => e.date == item);
                        if (element) arr.push(Number(element.finalPrice));
                        else arr.push(0);
                        return arr;
                    }, []);
                    console.log(this.valueExpense)
                }


                if (res[1]?.code == 200) {
                    this.listExpense = res[1].objects?.data?.map((item: any) => {
                        item.date = moment(item.date).format("DD/MM/YYYY");
                        return item;
                    });
                    this.valueIncome = this.time.reduce((arr: any, item: any) => {
                        let element: any = this.listExpense?.find((e: any) => e.date == item);
                        if (element) arr.push(Number(element.totalAmount));
                        else arr.push(0);
                        return arr;
                    }, []);
                }

                this.money();
            })
        // }

    }

    getPassengers(filter?: any) {
        this.bookingService.getPassengers(filter).subscribe((res: any) => {
            if (res.code == 200) {
                this.listPassengers = res.objects;
                this.valuePassenger = this.time.reduce((arr: any, item: any) => {
                    let element: any = this.listPassengers?.find((e: any) => e.date == item);
                    if (element) arr.push(Number(element.totalAmount));
                    else arr.push(0);
                    return arr;
                }, []);
            }
        })
    }


}
