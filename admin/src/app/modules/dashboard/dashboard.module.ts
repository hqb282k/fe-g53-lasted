import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { WidgetsModule } from '../../_metronic/partials';

import { NgApexchartsModule } from "ng-apexcharts";
import { ComponentModule } from 'src/app/shared/shared';
import { PermissionGuard } from 'src/app/shared/guard/permission.guard';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    ComponentModule,
    RouterModule.forChild([
      {
        path: '',
        canActivate:[PermissionGuard],
        component: DashboardComponent,
      },
    ]),

    WidgetsModule,
    NgApexchartsModule,
  ],
  bootstrap: []
})
export class DashboardModule {}
