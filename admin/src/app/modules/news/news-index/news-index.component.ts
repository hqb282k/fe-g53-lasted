import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { CommonService, BookingService, RoomService, AlertService } from '../../../services/service';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
  selector: 'app-news-index',
  templateUrl: './news-index.component.html',
  styleUrls: ['./news-index.component.scss']
})
export class NewsIndexComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;
    permission = permissionName.order

    listBooking: any;

    listPost: any = [];

    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    started_at:any;
    ended_at: any;
    code: string;
    customer: string;
    isReset = false;

    constructor(
        public commonService: CommonService,
        private alertService: AlertService,
        private router: Router,
        private roomService: RoomService
    ) {
     }

    ngOnInit(): void {
        this.getListPost();
       
    }

    getListPost() {
        this.isLoading = true;
        let queryParam = {
            page: this.paging.page,
            pageSize: this.paging.pageSize
        };

        this.commonService.paramURL(queryParam, 'news');
        this.roomService.getPosts(this.paging).subscribe(res => {
            if(res.code == 200) {
                this.listPost = res.objects;
                this.paging = res.paging;
                this.paging.page = this.paging.page + 1;
            } else if(res.code == 403) {
                this.router.navigate(["/error/403"]);
            } else if(res.code == 401) {
                this.router.navigate(['/auth/login']);
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
            this.isLoading = false;
        }) 
    }

    changePage(event: any) {
        this.getListPost();
    }

    deletePost(id: number) {
        this.paging.page = 1;
        this.alertService.fireConfirmYes("Bạn có chắc chắn xóa bài viết không?").then((result: any) => {
            this.isLoading = true;
            if (result.isConfirmed) {
                this.roomService.deletePost(id).subscribe(res => {
                    if(res.code == 200) {
                        this.getListPost();
                    } else if(res.code == 403) {
                        this.router.navigate(["/error/403"]);
                    } else if(res.code == 401) {
                        this.router.navigate(['/auth/login']);
                    } else {
                        this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
                    }
                    this.isLoading = false;
                })
            } else {
                this.isLoading = false;
            }
        });
        
    }

    create() {
        this.router.navigate(['news/create']);
    }

}
