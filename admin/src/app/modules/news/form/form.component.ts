import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { CommonService, RoomService } from '../../../services/service';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

    isLoading = false;
    imgSelect: any;

    detail: any;

    id: any;

    thumbImage = {
        file: null,
        file_name: null,
        base64image: null,
        old: false
    };


    formNew = new FormGroup({
        title: new FormControl(null, Validators.required),
        description: new FormControl(null, Validators.required),
        body: new FormControl(null, Validators.required),
    });

    get title() { return this.formNew.get('title') as FormControl };
    get description() { return this.formNew.get('description') as FormControl };
    get body() { return this.formNew.get('body') as FormControl };

    validator_value = {
        'title': [
            {
                type: 'required',
                message: 'Tiêu đề không được để trống.'
            }
        ],
        'image': [
            {
                type: 'required',
                message: 'Hình ảnh không được để trống.'
            }
        ],
        'short': [
            {
                type: 'required',
                message: 'Mô tả ngắn không được để trống.'
            }
        ],
        'body': [
            {
                type: 'required',
                message: 'Nội dung không được để trống.'
            }
        ]
    }

    constructor(
        private alertService: AlertService,
        private roomService: RoomService,
        private router: Router,
        private route: ActivatedRoute,
        public commonService: CommonService
    ) { }

    ngOnInit(): void {
        this.id = this.route.snapshot.paramMap.get('id');
        if(this.id) {
            this.detailPost(this.id);
        }
    }

    detailPost(id: number) {
        this.isLoading = true;
        this.roomService.getPostDetail(id).subscribe(res => {
            if(res.code == 200) {
                this.detail = res.objects;
                this.appenData(this.detail);
            } else if(res.code == 403) {
                this.router.navigate(["/error/403"]);
            } else if(res.code == 401) {
                this.router.navigate(['/auth/login']);
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
            this.isLoading = false;
        })
    }

    appenData(data: any) {
        if(data) {
            this.formNew.patchValue({
                title: data.title,
                description: data.description,
                body: data.body
            });
            if(data.photo) {
                this.thumbImage = {
                    file: data.photo,
                    file_name: data.photo,
                    base64image: data.photo,
                    old: true
                };
            }
            console.log(this.thumbImage)

        } else {
            this.alertService.fireSmall('error', "Không tìm thấy dữ liệu");
            this.router.navigate(['news']);
        }
        
    } 

    allowExt: string[] = ['png', 'jpg', 'jpeg', 'gif'];

    onFileChanged(event: any, itemFile: any) {
        if (event) {
            if (event.target.files[0].type.indexOf('image') < 0) {
                this.alertService.fireSmall('error', 'File ảnh không đúng định dạng!');
                return;
            }
            if (event.target.files[0].size > 2000000) {
                this.alertService.fireSmall('error', 'File ảnh nhỏ hơn 2MB!');
                return;
            }
            let fileName = event.target.files[0].name;
            let extFile = fileName.split('.').pop();

            if (!this.allowExt.includes(extFile)) {
                this.alertService.fireSmall('error', 'Định dạng này không được hỗ trợ');
                return;
            }
            itemFile.file = event.target.files[0];
            itemFile.old = false;
            itemFile.file_name = event.target.files[0].name;
            this.readFile(event.target, itemFile);
        }
    }

    readFile(inputValue: any, itemFile: any): void {

        const file: File = inputValue.files[0];
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
            itemFile.base64image = myReader.result;
        };
        myReader.readAsDataURL(file);
    }

    deleteImage(event: any, item: any, index: number | null) {
        event.stopPropagation();
        item.file = null;
        item.base64image = null;
        item.file_name = null;
        
    }

    clickInputFile(event: any, id: any) {
        event.stopPropagation();
        let element = document.querySelector("#file_" + id) as HTMLElement;
        element.click();
    }

    quillDescriptionRef: any;
    quillDescription(quill: any) {
        this.quillDescriptionRef = quill;
        const toolbar = quill.getModule('toolbar');
        toolbar.addHandler('image', () => {
            const range = this.quillDescriptionRef.getSelection();
            if (range != null) {
                this.uploadToS3(this.quillDescriptionRef);
            }
        });
    }


    uploadToS3(quillRef: any) {

        let input: any = document.createElement('input');
        const section = quillRef.getSelection();
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.addEventListener('change', () => {
            let file: any = input.files[0];

            let fileName = file.name;
            let extFile = fileName.split('.').pop();

            if (!this.allowExt.includes(extFile.toLocaleLowerCase())) {
                this.alertService.fireSmall('error', 'Định dạng không được hỗ trợ');
                return;
            }
            this.isLoading = true;
            this.roomService.uploadImage(file, 'product/editor').toPromise().then((response) => {
                if (response.status == 200) {
                    let url: string = response.data.file;
                    quillRef.insertEmbed(section.index, 'image', url, 'user');
                }
                this.isLoading = false;

            });
        });
        input.click();
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

    async onSubmit() {
        let params: any = this.formNew.value;
        if(this.title.value == null || this.title.value?.trim() == "") {
            this.alertService.fireSmall('error', "Tiêu đề không được để trống");
            return;
        }
        if(this.description.value == null || this.description.value?.trim() == "") {
            this.alertService.fireSmall('error', "Mô tả bài viết không được để trống");
            return;
        }
        if(!this.thumbImage.file) {
            this.alertService.fireSmall('error', "Vui lòng chọn ảnh");
            return;
        }
        this.isLoading = true;
        await this.uploadThumbImage(params);
        let data: any = params;
        if(this.id) {
            data = {...params, id: this.id};
        }
        this.roomService.createUpdatePost(data,this.id).subscribe(res => {
                if(res.code == 200) {
                    this.alertService.fireSmall("success", `${this.id ? "Chỉnh sửa": "Tạo"} bài viết thành công`)
                    this.router.navigate(['/news']);
                } else if(res.code == 403) {
                    this.router.navigate(["/error/403"]);
                } else if(res.code == 401) {
                    this.router.navigate(['/auth/login']);
                } else {
                    this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
                }
                this.isLoading = false;
        })
    }

    // async saveImages(params) {
    //     await this.uploadThumbImage(params);
    //     await this.uploadImageSlide(params);
    //     return params;
    // }

    async uploadThumbImage(params: any) {
        if (!this.thumbImage.old) {
            let responseUpload = await this.roomService.uploadImage(this.thumbImage.file, 'product/image').toPromise();
            if (responseUpload.status == 200) {
                params.photo = responseUpload.data.file;
            }
            else params.photo = null;
        } else {
            params.photo = this.thumbImage.file_name;
        }
    }

    // async uploadImageSlide(params) {
    //     let imagesUpload: object[] = [];
    //     let listImages: string[] = [];

    //     this.imageFiles.forEach((item: any, index: number) => {
    //         if (item.file && item.old == false) imagesUpload.push({
    //             file: item.file,
    //             fieldName: 'image_' + index
    //         });
    //         else if (item.file && item.old == true) listImages.push(item.file);
    //     });
    //     if (imagesUpload.length > 0) {
    //         let responseUpload = await this.uploadService.uploadMultiImages(imagesUpload, 'product/image').toPromise();
    //         if (responseUpload.status == 200) {
    //             responseUpload.data.files.forEach((item: any) => {
    //                 listImages.push(item.url);
    //             });
    //         }
    //         params.list_images = listImages;
    //     }
    //     else {
    //         params.list_images = listImages;
    //     }
    // }


}
