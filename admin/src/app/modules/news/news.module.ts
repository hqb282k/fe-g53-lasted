import { NgModule } from '@angular/core';
import { NewsIndexComponent } from './news-index/news-index.component';
import { ComponentModule } from 'src/app/shared/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { BookingChildModule } from 'src/app/components/booking-child/booking-child.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { PermissionGuard } from 'src/app/shared/guard/permission.guard';
import { FormComponent } from './form/form.component';
import { CommonModule } from '@angular/common';
// import { CKEditorModule } from 'ng2-ckeditor/ckeditor.module';
import {QuillModule} from 'ngx-quill'

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                canActivate:[PermissionGuard],
                component: NewsIndexComponent,
                data:{
                    role: "ADMIN|STAFF"
                }
            },
            {
                path: 'create',
                canActivate:[PermissionGuard],
                component: FormComponent,
                data:{
                    role: "STAFF"
                }
            },
            {
                path: 'edit/:id',
                canActivate:[PermissionGuard],
                component: FormComponent,
                data:{
                    role: "STAFF"
                }
            },
        ]
    },
]

@NgModule({
  declarations: [
    NewsIndexComponent,
    FormComponent
  ],
  imports: [
    BookingChildModule,
    ComponentModule,
    FormsModule,
    NgbModule,
    QuillModule.forRoot(),
    // CKEditorModule,
    ReactiveFormsModule,
    CommonModule,   
    NgSelectModule,
    RouterModule.forChild(routes)
  ]
})
export class NewsModule { }
