import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { AlertService, CommonService, RoomService } from 'src/app/services/service';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
    selector: 'app-create-room-type',
    templateUrl: './create-room-type.component.html',
    styleUrls: ['./create-room-type.component.scss']
})
export class CreateRoomTypeComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;

    detail: any;


    listType: any;

    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    base64: any;
    file: any;

    imageFiles: any = [];

    ENUM_ROOM_OPTION = [
        {
            "value": 1,
            "name": "Đặc điểm"
        },
        {
            "value": 2,
            "name": "Phòng tắm và vật dụng phòng tắm",
        },
        {
            "value": 3,
            "name": "Giải trí"
        },
        {
            "value": 4,
            "name": "Tiện nghi"
        },
        {
            "value": 5,
            "name": "Bố trí và nội thất"
        },
        {
            "value": 6,
            "name": "Quần áo và Giặt ủi"
        },
        {
            "value": 7,
            "name": "Vật dụng an toàn và an ninh"
        }
    ];

    sale = {
        week: 10,
        month: 15
    }


    formRoomType = new FormGroup({
        roomTypeName: new FormControl(null, Validators.required),
        numberCustomer: new FormControl(null, Validators.required),
        priceHour: new FormControl(null, Validators.required),
        priceDay: new FormControl(null, Validators.required),
        // priceNight: new FormControl(null),
        // priceWeek: new FormControl(null),
        // priceMonth: new FormControl(null),
        priceOverHour: new FormControl(null, Validators.required),
        option: new FormControl(null, Validators.required)
    });

    get roomTypeName() { return this.formRoomType.get('roomTypeName') as FormControl };

    get numberCustomer() { return this.formRoomType.get('numberCustomer') as FormControl };
    get priceHour() { return this.formRoomType.get('priceHour') as FormControl };
    get priceDay() { return this.formRoomType.get('priceDay') as FormControl };
    // get priceNight() { return this.formRoomType.get('priceNight') as FormControl };
    // get priceWeek() { return this.formRoomType.get('priceWeek') as FormControl };
    // get priceMonth() { return this.formRoomType.get('priceMonth') as FormControl };
    get priceOverHour() { return this.formRoomType.get('priceOverHour') as FormControl };
    get option() { return this.formRoomType.get('option') as FormControl };

    validator_value = {
        'roomTypeName': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'numberBed': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'numberCustomer': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceHour': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceDay': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceNight': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceWeek': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceMonth': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceOverHour': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'option': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
    };

    configs: number = 0;

    listOption: any = [];

    id: any

    constructor(
        public commonService: CommonService,
        private roomService: RoomService,
        private modalService: NgbModal,
        private alertService: AlertService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        this.initListImages();
        let detailId = Number(this.route.snapshot.paramMap.get('id'));
        if (detailId) {
            this.getDetailType(detailId);
        }

    }

    ngOnInit(): void {
        this.getRoomOption({ page: 1, pageSize: 1000 });
        
    }

    initListImages(startIndex: number = 1) {
        for (let i = startIndex; i <= 9; i++) {
            this.addImageItem();
        }
    }

    addImageItem() {
        this.imageFiles.push({
            file: null,
            file_name: null,
            base64image: null,
            old: false
        });
    }

    getRoomOption(paging: any, filter?: any) {
        this.roomService.getRoomOption(paging, filter).subscribe(res => {
            if (res.code == 200) {
                this.listType = res.objects;
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
        })
    }

    getDetailType(id: number) {
        this.isLoading = true;
        this.roomService.getRoomTypeId(id).subscribe(res => {
            if (res.code == 200) {
                this.detail = res.objects;
                this.formRoomType.patchValue(res.objects);
                if(this.detail.photos?.length > 0) {
                    this.detail.photos.forEach((image: any, i: number) => {
                        this.imageFiles[i] = {
                            file: image.url,
                            file_name: image.url,
                            base64image: image.url,
                            old: true
                        }
                    });
                }
                let opt = this.detail.roomOption?.map((item: any) => item.id);
                this.option.setValue(opt)
            } else if (res.status == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.status == 403) {
                this.router.navigate(['error/403'])
            }
            this.isLoading = false;
        });
    }

    navigate() {
        this.router.navigate(['/room/types/create']);
    }

    async onSubmit() {
        let data: any = this.formRoomType.value;

        let options = this.listType.filter((item: any) => {
            if (this.option.value.includes(item.id)) {
                return item;
            }
        });
        if(this.priceDay.value == null || this.priceDay.value == "") {
            this.alertService.fireSmall('error', "Giá theo ngày không được để trống");
            return;
        }

        data.priceMonth = Number(this.priceDay.value) * 30 * 0.85;
        data.priceNight = Number(this.priceDay.value);
        data.priceWeek = Number(this.priceDay.value) * 7 * 0.95;

        data.roomOption = options;
        delete data.option;
        this.isLoading = true;
        await this.uploadImageSlide(data);
        if (data.photos?.length <= 0 || !data.photos) {
            this.alertService.fireSmall('error', "Hãy chọn ít nhất 1 ảnh");
            return;
        }
        let body = {
            "objectRequest": data
        };
        let id: number;
        if (this.detail) {
            id = this.detail.id;
            body.objectRequest = { ...body.objectRequest, id: this.detail.id };
        }
        this.roomService.createRoomType(body, this.detail?.id).subscribe(res => {
            if (res.code == 200 || res.code == 201) {
                if (id) {
                    this.alertService.fireSmall('success', "Chỉnh sửa thành công");
                } else {
                    this.alertService.fireSmall('success', "Thêm mới thành công");
                }
                this.router.navigate(['room/types']);
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else if (res.code == 113) {
                this.alertService.fireSmall('error', "Tên loại phòng đã tồn tại");
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }

            this.isLoading = false;
        })
    }

    allowExt: string[] = ['png', 'jpg', 'jpeg', 'gif'];

    onFileChanged(event: any, itemFile: any) {
        if (event) {
            if (event.target.files[0].type.indexOf('image') < 0) {
                this.alertService.fireSmall('error', 'File ảnh không đúng định dạng!');
                return;
            }
            if (event.target.files[0].size > 2000000) {
                this.alertService.fireSmall('error', 'File ảnh nhỏ hơn 2MB!');
                return;
            }
            let fileName = event.target.files[0].name;
            let extFile = fileName.split('.').pop();

            if (!this.allowExt.includes(extFile)) {
                this.alertService.fireSmall('error', 'Định dạng này không được hỗ trợ');
                return;
            }
            itemFile.file = event.target.files[0];
            itemFile.old = false;
            itemFile.file_name = event.target.files[0].name;
            this.readFile(event.target, itemFile);
        }
    }

    readFile(inputValue: any, itemFile: any): void {

        const file: File = inputValue.files[0];
        const myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
            itemFile.base64image = myReader.result;
        };
        myReader.readAsDataURL(file);
    }

    deleteImage(event: any, item: any, index: number | null) {
        event.stopPropagation();
        item.file = null;
        item.base64image = null;
        item.file_name = null;

    }

    clickInputFile(event: any, id: any) {
        event.stopPropagation();
        let element = document.querySelector("#file_" + id) as HTMLElement;
        element.click();
    }

    async uploadImageSlide(data: any) {
        let imagesUpload: object[] = [];
        let listImages: any[] = [];

        this.imageFiles.forEach((item: any, index: number) => {
            if (item.file && item.old == false) imagesUpload.push({
                file: item.file,
                fieldName: 'image_' + index
            });
            else if (item.file && item.old == true) listImages.push({
                url: item.url,
                status: 0
            });
        });
        if (imagesUpload.length > 0) {
            let responseUpload = await this.roomService.uploadMultiImages(imagesUpload, 'product/image').toPromise();
            if (responseUpload.status == 200) {
                responseUpload.data.files.forEach((item: any) => {
                    listImages.push({
                        url: item.url,
                        status: 0
                    });
                });
            }
            data.photos = listImages;
        }
        else {
            data.photos = listImages;
        }
    }

    countPrice($event: any) {
        console.log(this.priceDay.value)
    }

    checkImg() {
        let check = this.imageFiles.every((item: any) => item.file == null);
        return check;
    }

}
