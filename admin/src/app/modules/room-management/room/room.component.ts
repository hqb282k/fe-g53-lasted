import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { AlertService, CommonService, RoomService } from 'src/app/services/service';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
    selector: 'app-room',
    templateUrl: './room.component.html',
    styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;
    permission = permissionName.order

    listRoom: any;
    listType: any;

    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    listFloor = [
        {
            id: 1,
            name: "Tầng 1"
        },
        {
            id: 2,
            name: "Tầng 2"
        },
        {
            id: 3,
            name: "Tầng 3"
        },
        {
            id: 4,
            name: "Tầng 4"
        },
    ];

    listStatus = [
        {
            label: 'Bảo trì',
            value: 'REPAIR'
        },
        {
            label: 'Còn trống',
            value: 'AVAILABLE'
        },
        {
            label: 'Đang có người',
            value: 'BUSY'
        },
    ]

    started_at: any;
    ended_at: any;
    code: string;
    typeId: any;
    isReset = false;


    formRoom = new FormGroup({
        roomNo: new FormControl(null, Validators.required),
        roomDescription: new FormControl(null),
        floor: new FormControl(null, Validators.required),
        type: new FormControl(null, Validators.required),
        status: new FormControl(null, Validators.required),
    });

    get roomNo() { return this.formRoom.get('roomNo') as FormControl };
    get roomDescription() { return this.formRoom.get('roomDescription') as FormControl };
    get floor() { return this.formRoom.get('floor') as FormControl };
    get type() { return this.formRoom.get('type') as FormControl };
    get status() { return this.formRoom.get('status') as FormControl };

    validator_value = {
        'roomNo': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'roomDescription': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'floor': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'type': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'status': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ]
    }

    configs: number = 0;

    constructor(
        public commonService: CommonService,
        private roomService: RoomService,
        private modalService: NgbModal,
        private alertService: AlertService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.getRoomType();
        this.getRooms(this.paging);
    }

    getRooms(paging: any, filter?: any) {
        this.isLoading = true;
        let queryParam = {
            page: this.paging.page,
            type: this.typeId ? this.typeId : null,
            roomNo: this.code != null ? this.code : null
        };

        this.commonService.paramURL(queryParam, 'room/list');

        this.roomService.getRooms(paging, filter).subscribe((res: any) => {
            if (res.code == 200) {
                this.listRoom = res.objects;
                this.paging.page = res.paging?.page + 1;
                this.paging.total = res.paging?.total;
                this.paging.pageSize = res.paging?.pageSize;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }

            this.isLoading = false;
        }, err => {
            console.log(err)
        })
    }

    getRoomType() {
        this.roomService.getRoomType().subscribe(res => {
            if (res.code == 200) {
                this.listType = res.objects;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
        })
    }

    selectDate($event: any) {
        if ($event) {
            this.started_at = $event.start;
            this.ended_at = $event.end;
        }
    }

    resetForm() {
        this.isReset = true;
        this.typeId = null;
        this.code = '';
        this.started_at = null;
        this.ended_at = null;
        this.getRooms({page: 1, pageSize: 20});
    }

    chooseStatus(event: number) {

    }

    filter() {
        this.paging.page = 1;
        this.getRooms({page: 1, pageSize: 20}, {typeId: this.typeId, code: this.code});
    }

    changePage(event: any) {
        this.paging = event;
        this.getRooms(this.paging);
    }

    detail: any;

    openModal(content: any, id?: number) {
        this.formRoom.reset();
        this.detail = undefined;
        if (id) {
            this.roomService.getRoomId(id).subscribe(res => {
                if (res.code == 200) {
                    this.detail = res.objects;
                    this.formRoom.patchValue(res.objects);
                    let check = this.listStatus.some(item => item.value == this.detail?.status);
                    if(!check) {
                        this.status.setValue(null);
                    }
                    this.type.setValue(res?.objects?.roomTypeMap?.id);
                    this.modalService.open(content, {
                        windowClass: 'modal-account',
                        size: "lg",
                        centered: true
                    });
                } else if(res.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if(res.code == 403) {
                    this.router.navigate(['error/403'])
                }
            })
        } else {
            this.modalService.open(content, {
                windowClass: 'modal-account',
                scrollable: true,
                size: "lg",
                centered: true
            })
        }

    }

    onSubmit() {
        this.isLoading = true;
        let data = {
            ...this.formRoom.value,
            roomTypeMap: {
                id: this.type.value
            }
        };
        delete data.type;
        let body = {
            "objectRequest": data
        };
        let id: any;
        if (this.detail) {
            id = this.detail.id;
            body.objectRequest = { ...body.objectRequest, id: this.detail.id };
        }
        this.roomService.createRoom(body, id).subscribe(res => {
            if (res.code == 200) {
                if (id) {
                    this.alertService.fireSmall('success', "Chỉnh sửa thành công");
                } else {
                    this.alertService.fireSmall('success', "Thêm mới thành công");
                }
                this.modalService.dismissAll();
                this.getRooms(this.paging);
            } else if(res.code == 122) {
                this.alertService.fireSmall('error', "Phòng đang có người, không thể chỉnh sửa thông tin phòng");
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else if( res.code == 114) {
                this.alertService.fireSmall('error', "Tên phòng đã tồn tại");
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }
            this.isLoading = false;
        })
    }

    deleteRoomType(id: number) {
        console.log(id);
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

    getRoomStatus(status: any) {
        if(status) {
            let name = this.listStatus.find((item: any) => item.value == status);
            return name ? name.label : 'Chưa cập nhật';
        }
        return 'Chưa cập nhật';
    }

    getClassStatus(status: string) {
        if(status == 'BUSY') return 'badge badge-light-danger';
        else if(status == 'REPAIR') return 'badge badge-light-warning';
        else if(status == 'AVAILABLE') return 'badge badge-light-success';
        else return 'badge badge-light-primary' 
    }

}
