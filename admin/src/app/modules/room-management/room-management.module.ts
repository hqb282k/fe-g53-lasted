import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { RoomChildModule } from 'src/app/components/room-child/room-child.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BookingChildModule } from 'src/app/components/booking-child/booking-child.module';
import { CreateComponent } from './create/create.component';
import { DirectiveModule, PipeModule } from 'src/app/shared/shared';
import { PermissionGuard } from 'src/app/shared/guard/permission.guard';
import { RoomTypeComponent } from './room-type/room-type.component';
import { RoomComponent } from './room/room.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CreateRoomTypeComponent } from './create-room-type/create-room-type.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                redirectTo:'list',
                pathMatch:'full'
            },
            {
                path: 'list',
                canActivate:[PermissionGuard],
                component: RoomComponent,
                data:{
                    role: "ADMIN|STAFF|RECEPTIONLIST"
                } 
            },
           
            {
                path: 'types',
                canActivate:[PermissionGuard],
                component: RoomTypeComponent,
                data:{
                    role: "ADMIN|STAFF|RECEPTIONLIST"
                } 
            },
            {
                path: 'types/create',
                canActivate:[PermissionGuard],
                component: CreateRoomTypeComponent,
                data:{
                    role: "STAFF"
                } 
            },
            {
                path: 'types/edit/:id',
                canActivate:[PermissionGuard],
                component: CreateRoomTypeComponent,
                data:{
                    role: "STAFF"
                }
            },
        ]
    },
]

@NgModule({
  declarations: [CreateComponent, RoomTypeComponent, RoomComponent, CreateRoomTypeComponent],
  imports: [
    CommonModule,
    ComponentModule,
    RoomChildModule,
    BookingChildModule,
    DirectiveModule,
    FormsModule,
    NgbModule,
    PipeModule,
    NgSelectModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class RoomManagementModule { }
