import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { AlertService, CommonService, RoomService } from 'src/app/services/service';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
    selector: 'app-room-type',
    templateUrl: './room-type.component.html',
    styleUrls: ['./room-type.component.scss']
})
export class RoomTypeComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;
    permission = permissionName.order

    listType: any;

    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    started_at: any;
    ended_at: any;
    code: string;
    customer: string;
    isReset = false;

    base64: any;
    file: any;

    ENUM_ROOM_OPTION = [
        {
            "value" : 1,
            "name" : "Đặc điểm"
        },
        {
            "value" : 2,
            "name" : "Phòng tắm và vật dụng phòng tắm",
        },
        {
            "value" : 3,
            "name" : "Giải trí"
        },
        {
            "value" : 4,
            "name" : "Tiện nghi"
        },
        {
            "value" : 5,
            "name" : "Bố trí và nội thất"
        },
        {
            "value" : 6,
            "name" : "Quần áo và Giặt ủi"
        },
        {
            "value" : 7,
            "name" : "Vật dụng an toàn và an ninh"
        }
    ]


    formRoomType = new FormGroup({
        roomTypeName: new FormControl(null, Validators.required),
        numberBed: new FormControl(null, Validators.required),
        numberCustomer: new FormControl(null, Validators.required),
        priceHour: new FormControl(null, Validators.required),
        priceDay: new FormControl(null, Validators.required),
        priceNight: new FormControl(null, Validators.required),
        priceWeek: new FormControl(null, Validators.required),
        priceMonth: new FormControl(null, Validators.required),
        priceOverHour: new FormControl(null, Validators.required),
        services: new FormControl(null, Validators.required)
    });

    get roomTypeName() { return this.formRoomType.get('roomTypeName') as FormControl };
    get numberBed() { return this.formRoomType.get('numberBed') as FormControl };

    get numberCustomer() { return this.formRoomType.get('numberCustomer') as FormControl };
    get priceHour() { return this.formRoomType.get('priceHour') as FormControl };
    get priceDay() { return this.formRoomType.get('priceDay') as FormControl };
    get priceNight() { return this.formRoomType.get('priceNight') as FormControl };
    get priceWeek() { return this.formRoomType.get('priceWeek') as FormControl };
    get priceMonth() { return this.formRoomType.get('priceMonth') as FormControl };
    get priceOverHour() { return this.formRoomType.get('priceOverHour') as FormControl };
    get services() { return this.formRoomType.get('services') as FormControl };

    validator_value = {
        'roomTypeName': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'numberBed': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'numberCustomer': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceHour': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceDay': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceWeek': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceMonth': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'priceOverHour': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'services': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
    };

    formOption = new FormGroup({
        name: new FormControl(null, Validators.required),
        typeOption: new FormControl(null, Validators.required)

    });

    get name() { return this.formOption.get('name') as FormControl };
    get typeOption() { return this.formOption.get('typeOption') as FormControl };

    validator_option_value = {
        'name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'typeOption': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ]
    }

    configs: number = 0;

    listOption: any = [];

    constructor(
        public commonService: CommonService,
        private roomService: RoomService,
        private modalService: NgbModal,
        private alertService: AlertService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.getRoomType(this.paging);
    }

    getRoomType(paging: any, filter?: any) {
        this.isLoading = true;
        this.roomService.getRoomType(paging,filter).subscribe(res => {
            if (res.code == 200) {
                this.listType = res.objects;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
            
            this.isLoading = false;
        })
    }

    getRoomOption(paging: any, filter?: any) {
        this.roomService.getRoomType(paging,filter).subscribe(res => {
            if (res.code == 200) {
                this.listType = res.objects;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            } 
        })
    }

    selectDate($event: any) {
        if ($event) {
            this.started_at = $event.start;
            this.ended_at = $event.end;
        }
    }

    resetForm() {
        this.isReset = true;
        this.customer = '';
        this.code = '';
        this.started_at = null;
        this.ended_at = null;
        // this.listBooking = this.listBooking.getList();
    }

    chooseStatus(event: number) {

    }

    filter() {
        let queryParam = {
            booking_code: this.code ? this.code : null,
            customer: this.customer ? this.customer : null,
            time_checkin: this.started_at ? this.started_at : null,
            ended_at: this.ended_at ? this.ended_at : null,
        };

        this.commonService.paramURL(queryParam, 'booking');
    }

    changePage(event: any) {

    }

    navigate(id?: any) {
        if(id) {
        this.router.navigate([`/room/types/edit/${id}` ]);

        } else {
        this.router.navigate(['/room/types/create']);

        }
    }

    detail: any;

    openModal(content: any, id?: number) {
        this.formOption.reset();
        this.modalService.open(content, {
            windowClass: 'modal-account',
            size: "lg",
            centered: true
        });

    }
    onSubmit() {
        this.isLoading = true;

        let body = {
            "objectRequest": this.formRoomType.value
        };
        let id: number;
        if (this.detail) {
            id = this.detail.id;
            body.objectRequest = { ...body.objectRequest, id: this.detail.id };
        }
        this.roomService.createRoomType(body, this.detail?.id).subscribe(res => {
            if (res.code == 200) {
                if (id) {
                    this.alertService.fireSmall('success', "Chỉnh sửa thành công");
                } else {
                    this.alertService.fireSmall('success', "Thêm mới thành công");
                }
                this.modalService.dismissAll();
                this.getRoomType(this.paging);
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else if (res.code == 113) {
                this.alertService.fireSmall('error', "Tên loại phòng đã tồn tại");
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }
            
            this.isLoading = false;
        })
    }

    deleteRoomType(id: number) {
        console.log(id);
    }

    onSubmitOption() {
        this.isLoading = true;

        let data: any = this.formOption.value;
        data.type = data.typeOption;
        delete data.typeOption;

        let body = {
            "objectRequest": data
        };
        let id: number;
        if (this.detail) {
            id = this.detail.id;
            body.objectRequest = { ...body.objectRequest, id: this.detail.id };
        }
        this.roomService.createRoomOption(body, this.detail?.id).subscribe(res => {
            if (res.code == 200) {
                if (id) {
                    this.alertService.fireSmall('success', "Chỉnh sửa thành công");
                } else {
                    this.alertService.fireSmall('success', "Thêm mới thành công");
                }
                this.modalService.dismissAll();
                this.getRoomType(this.paging);
            } else if (res.code == 117) {
                this.alertService.fireSmall('error', "Tên tùy chọn đã tồn tại");
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }
            this.isLoading = false;
        })
    }

}
