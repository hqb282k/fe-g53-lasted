import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { delay, tap } from 'rxjs';
import { CommonService, RoomService, AlertService } from 'src/app/services/service';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.scss']
})
export class TypeComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;
    paging = {
        page: 1,
        total: 0,
        pageSize: 20
    };
    code: any;
    serviceType: any = [];

    form = new FormGroup({
        name: new FormControl(null, Validators.required),
        typeDescribe: new FormControl(null)

    });

    get name() { return this.form.get('name') as FormControl };
    get typeDescribe() { return this.form.get('typeDescribe') as FormControl };

    validator_value = {
        'name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'typeDescribe': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ]
    }

    constructor(
        public commonService: CommonService,
        private modalService: NgbModal,
        private roomService: RoomService,
        private router: Router,
        private alertService: AlertService
    ) {
     }

    ngOnInit(): void {
        this.getServiceType(this.paging);
    }
    
    getServiceType(paging: any, filter?: any) {
        this.isLoading = true;
        let queryParam = {
            page: this.paging.page ? this.paging.page : null,
            pageSize: this.paging.pageSize ? this.paging.pageSize : null,
            name: this.code ? this.code : null
        };

        this.commonService.paramURL(queryParam, '/service/type');
        this.roomService.getServiceType(paging, filter)
        .pipe(tap(() => this.isLoading = false))
        .subscribe(res => {
            if(res.code == 200) {
                this.serviceType = res.objects;
                this.paging.page = res.paging.page + 1;
                this.paging.pageSize = res.paging.pageSize;
                this.paging.total = res.paging.total;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            }
        })
    }

    // selectDate($event: any) {
    //     if ($event) {
    //         this.started_at = $event.start;
    //         this.ended_at = $event.end;
    //     }
    // }

    resetForm() {
        this.code = null;
        this.getServiceType({page: 1, pageSize: 20}, {name: this.code});
    }

    chooseStatus(event:number) {

    }

    filter() {
        this.getServiceType({page: 1, pageSize: 20}, {name: this.code});
    }

    changePage(event: any) {
        console.log(event)
        this.getServiceType(this.paging, {name: this.code});
    }

    openModal(content:any, id?: number) {
        this.form.reset();
        this.detail = undefined;
        if(id) {
            this.roomService.detailServiceType(id).subscribe(res => {
                if(res.code == 200) {
                    this.detail = res.objects;
                    this.form.patchValue(this.detail);
                    this.modalService.open(content, {
                        windowClass: 'modal-account',
                        size: "lg",
                        centered: true
                    });
                } else if(res.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if(res.code == 403) {
                    this.router.navigate(['error/403'])
                } else {
                    this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
                }
            })
        } else {
            this.modalService.open(content, {
                windowClass: 'modal-account',
                size: "lg",
                centered: true
            });
        }
        
    }

    detail: any;

    onSubmit() {
        this.isLoading = true;
        let data = this.form.value;
        let body = {
            "objectRequest": data
        };
        let id: any;
        if (this.detail) {
            id = this.detail.id;
            body.objectRequest = { ...body.objectRequest, id: this.detail.id };
        }
        this.roomService.handleServiceType(body, id)
        .pipe(delay(500)).subscribe(res => {
            if (res.code == 200) {
                if (id) {
                    this.alertService.fireSmall('success', "Chỉnh sửa thành công");
                } else {
                    this.alertService.fireSmall('success', "Thêm mới thành công");
                }
                this.getServiceType({page: 1, pageSize: 20});
                this.modalService.dismissAll();
            } else if(res.code == 115) {
                this.alertService.fireSmall('error', "Tên loại dịch vụ đã tồn tại");
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }
            this.isLoading = false;
        })
    }

    delete(id: number) {
        this.isLoading = true;
        this.alertService.fireConfirmYes("Bạn có chắc chắn xóa loại dịch vụ này không?").then((result: any) => {
            this.isLoading = true;
            if (result.isConfirmed) {
                this.roomService.deleteServiceType(id).subscribe(res => {
                    if(res.code == 200) {
                        this.getServiceType({page: 1, pageSize: 20});
                    } else if(res.code == 403) {
                        this.router.navigate(["/error/403"]);
                    } else if(res.code == 401) {
                        this.router.navigate(['/auth/login']);
                    } else {
                        this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
                    }
                    this.isLoading = false;
                })
            } else {
                this.isLoading = false;
            }
        });
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

}
