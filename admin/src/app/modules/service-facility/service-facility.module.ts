import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceManageComponent } from './service-manage/service-manage.component';
import { TypeComponent } from './type/type.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ServiceChildModule } from 'src/app/components/service-child/service-child.module';
import { ComponentModule, DirectiveModule, PipeModule } from 'src/app/shared/shared';
import { PermissionGuard } from 'src/app/shared/guard/permission.guard';
import { NgSelectModule } from '@ng-select/ng-select';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'list',
                canActivate:[PermissionGuard],
                component: ServiceManageComponent,
                data:{
                    role: "ADMIN|STAFF"
                } 
            },
            {
                path: 'type',
                canActivate:[PermissionGuard],
                component: TypeComponent,
                data:{
                    role: "ADMIN|STAFF"
                } 
            }
        ]
    },
    {
        path:'',
        redirectTo:'list',
        pathMatch:'full'
    }
]


@NgModule({
  declarations: [
    ServiceManageComponent,
    TypeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    PipeModule,
    ServiceChildModule,
    ReactiveFormsModule,
    DirectiveModule,
    NgSelectModule,
    ComponentModule,
    RouterModule.forChild(routes)
  ]
})
export class ServiceTypeModule { }
