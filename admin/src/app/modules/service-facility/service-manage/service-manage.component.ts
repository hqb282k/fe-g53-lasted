import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { delay, tap } from 'rxjs';
import { CommonService, BookingService, roomType,statusRoom, RoomService, AlertService } from 'src/app/services/service';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Component({
  selector: 'app-service-manage',
  templateUrl: './service-manage.component.html',
  styleUrls: ['./service-manage.component.scss']
})
export class ServiceManageComponent implements OnInit {

    public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
    isLoading = false;
    permission = permissionName.order

    listService: any;

    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    started_at:any;
    ended_at: any;
    code: string;
    customer: string;
    isReset = false;

    configs: number = 0;

    serviceType: any = [];

    form = new FormGroup({
        name: new FormControl(null, Validators.required),
        price: new FormControl(0, Validators.required),
        inPrice: new FormControl(0, Validators.required),
        quantity: new FormControl(0, Validators.required),
        note: new FormControl(null),
        enable: new FormControl(0),
        service: new FormControl(null, Validators.required)

    });

    get name() { return this.form.get('name') as FormControl };
    get price() { return this.form.get('price') as FormControl };
    get inPrice() { return this.form.get('inPrice') as FormControl };
    get quantity() { return this.form.get('quantity') as FormControl };
    get note() { return this.form.get('note') as FormControl };
    get enable() { return this.form.get('enable') as FormControl };
    get service() { return this.form.get('service') as FormControl };

    validator_value = {
        'name': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'price': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'quantity': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'note': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ],
        'service': [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            }
        ]
    }

    constructor(
        public commonService: CommonService,
        private modalService: NgbModal,
        private roomService: RoomService,
        private router: Router,
        private alertService: AlertService
    ) {
     }

    ngOnInit(): void {
        this.getService(this.paging);
        this.getServiceType({page: 1, pageSize: 1000});
    }

    getService(paging: any, filter?: any) {
        this.isLoading = true;
        let queryParam = {
            page: this.paging.page ? this.paging.page : null,
            name: this.code ? this.code : null
        };

        this.commonService.paramURL(queryParam, '/service/list');
        this.roomService.getServices(paging, filter)
        .pipe(tap(() => this.isLoading = false))
        .subscribe(res => {
            if(res.code == 200) {
                this.listService = res.objects;
                this.paging = res.paging;
                this.paging.page = res.paging.page + 1;
                this.paging.total = res.paging.total;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
        })
    }
    
    getServiceType(paging: any) {
        this.roomService.getServiceType(paging).subscribe(res => {
            if(res.code == 200) {
                this.serviceType = res.objects;
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            }
        })
    }

    selectDate($event: any) {
        if ($event) {
            this.started_at = $event.start;
            this.ended_at = $event.end;
        }
    }

    resetForm() {
        this.isReset = true;
        this.customer = '';
        this.code = '';
        this.started_at = null;
        this.ended_at = null;
        this.paging.page = 1;
        this.getService(this.paging);
    }

    chooseStatus(event:number) {

    }

    filter() {
        this.getService({page: 1, pageSize: 20}, {name: this.code})
    }

    changePage(event: any) {
        this.paging = event;
        this.getService(event, {name: this.code});
    }

    openModal(content:any, id?: number) {
        this.form.reset();
        if(id) {
            this.roomService.getDetailService(id).subscribe(res => {
                if(res.code == 200) {
                    this.detail = res.objects;
                    console.log(this.detail)
                    this.form.patchValue({
                        name: this.detail.name ? this.detail.name : null,
                        service: this.detail?.serviceType?.id ? this.detail?.serviceType?.id  : 0,
                        price: this.detail.price ? this.detail.price : 0,
                        note: this.detail.note,
                        enable: this.detail.enable ? 1 : 0,
                        inPrice: this.detail.inPrice ? this.detail.inPrice : 0,
                        quantity: this.detail.quantity ? this.detail.quantity : 0
                    });
                } 
                if(res.status == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if(res.status == 403) {
                    this.router.navigate(['error/403'])
                }
            })
        }
        this.modalService.open(content, {
            windowClass: 'modal-account',
            size: "lg",
            centered: true
        })
    }

    checkPrice() {
        if(this.inPrice.value && this.price.value) {
            return Number(this.inPrice.value) > Number(this.price.value);
        }
    }

    delete(id: number) {
        this.alertService.fireConfirmYes("Bạn có chắc chắn xóa dịch vụ này không?").then((result: any) => {
            this.isLoading = true;
            if (result.isConfirmed) {
                this.roomService.deletePost(id).subscribe(res => {
                    if(res.code == 200) {
                        this.getService({page:1, pageSize: 20});
                    } else if(res.code == 403) {
                        this.router.navigate(["/error/403"]);
                    } else if(res.code == 401) {
                        this.router.navigate(['/auth/login']);
                    } else {
                        this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
                    }
                    this.isLoading = false;
                })
            } else {
                this.isLoading = false;
            }
        });
    } 

    detail: any;

    onSubmit() {
        this.isLoading = true;
        let data = {
            ...this.form.value,
            serviceType: {
                id: this.service.value
            }
        };
        data.enable = data.enable ? true: false;
        data.price = data.price + ',';
        console.log(data.price.includes(','))
        if(data.price.includes(',')) data.price = data.price.replaceAll(',','')
        let body = {
            "objectRequest": data
        };
        let id: any;
        if (this.detail) {
            id = this.detail.id;
            body.objectRequest = { ...body.objectRequest, id: this.detail.id };
        }
        this.roomService.createUpdateService(body, id)
        .subscribe(res => {
            if (res.code == 200) {
                if (id) {
                    this.alertService.fireSmall('success', "Chỉnh sửa thành công");
                } else {
                    this.alertService.fireSmall('success', "Thêm mới thành công");
                }
                this.modalService.dismissAll();
                this.getService({page: 1, pageSize: 20});
            } else if(res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if(res.code == 403) {
                this.router.navigate(['error/403'])
            } else if(res.code == 116) {
                this.alertService.fireSmall('error', "Tên dịch vụ đã tồn tại");
            } else  {
                this.alertService.fireSmall('error', "Có lỗi xảy ra");
            }
            
            this.isLoading = false;
        });
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }

}
