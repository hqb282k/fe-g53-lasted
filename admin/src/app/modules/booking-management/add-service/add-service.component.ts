import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import moment from 'moment';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { CommonService, BookingService, roomType, statusRoom, AlertService, RoomService } from 'src/app/services/service';


@Component({
    selector: 'app-add-service',
    templateUrl: './add-service.component.html',
    styleUrls: ['./add-service.component.scss']
})
export class AddServiceComponent implements OnInit {

    isLoading = false;
    listRoom: any = [];

    formService = new FormGroup({
        roomId: new FormControl(null, [Validators.required]),
        note: new FormControl(null),
        serviceLogs: new FormArray([], Validators.required)
    });

    get roomId() { return this.formService.get('roomId') as FormControl };
    get note() { return this.formService.get('note') as FormControl };
    get serviceLogs() { return this.formService.get('serviceLogs') as FormArray };

    services: any = [];
    bookingServices: any = [
        {
            id: null,
            quantity: null,
        }
    ]
    validator_value = [
        {
            type: 'required',
            message: 'Trường này không được để trống.'
        },
        {
            type: 'pattern',
            message: 'Trường này sai định dạng.'
        },
    ];
    constructor(
        public commonService: CommonService,
        private bookingService: BookingService,
        private router: Router,
        private alertService: AlertService,
        private roomService: RoomService
    ) { }

    ngOnInit(): void {
        this.getRooms({ page: 1, size: 20 })

    }

    getRooms(paging: any, filter?: any) {
        this.roomService.getRooms(paging, filter).subscribe((res: any) => {
            if (res.code == 200) {
                this.listRoom = res.objects;
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
        }, err => {
            console.log(err)
        })
    }


}
