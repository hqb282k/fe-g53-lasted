import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ComponentModule } from 'src/app/shared/component/component.module';
import { BookingChildModule } from 'src/app/components/booking-child/booking-child.module';
import { IndexComponent } from './index/index.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PermissionGuard } from 'src/app/shared/guard/permission.guard';
import { CreateComponent } from './create/create.component';
import { DirectiveModule, PipeModule } from 'src/app/shared/shared';
import { NgSelectModule } from '@ng-select/ng-select';
import { ShowComponent } from './show/show.component';
import { AddServiceComponent } from './add-service/add-service.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                canActivate:[PermissionGuard],
                component: IndexComponent,
                data:{
                    role: "ADMIN|STAFF|RECEPTIONLIST"
                }
            },
            {
                path: 'create',
                canActivate:[PermissionGuard],
                component: CreateComponent,
                data:{
                    role: "STAFF|RECEPTIONLIST"
                }
            },
            {
                path: 'add-service',
                canActivate:[PermissionGuard],
                component: AddServiceComponent,
                data:{
                    role: "STAFF|RECEPTIONLIST"
                }
            },
        ]
    },
]
@NgModule({
  declarations: [IndexComponent, CreateComponent, ShowComponent, AddServiceComponent],
  imports: [
    CommonModule,
    BookingChildModule,
    DirectiveModule,
    FormsModule,
    NgSelectModule,
    PipeModule,
    ReactiveFormsModule,
    ComponentModule,
    FormsModule,
    NgbModule,
    RouterModule.forChild(routes)
  ]
})
export class BookingManagementModule { }
