import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import moment from 'moment';
import { ngxLoadingAnimationTypes } from 'ngx-loading';
import { CommonService, BookingService, roomType, statusRoom, AlertService, RoomService } from 'src/app/services/service';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

    isLoading = false;

    listBooking: any = [];
    listRoom: any = [];

    configs = 0;

    configBooking = [
        {
            id: 0,
            name: "Tất cả",
            value: ""
        },
        {
            id: 1,
            name: "Chờ xác nhận",
            value: "DRAFT"
        },
        {
            id: 2,
            name: "Chờ check in",
            value: "SUCCESS"
        },
        {
            id: 3,
            name: "Check in",
            value: "CHECKIN"
        },
        {
            id: 4,
            name: "Check out",
            value: "CHECKOUT"
        },
        {
            id: 5,
            name: "Hoàn thành",
            value: "DONE"
        }
    ]

    paging = {
        page: 1,
        total_page: 1,
        total: 0,
        pageSize: 20
    };

    listStatus = [
        {
            label: "Đã đặt",

        }
    ]

    formSearch = new FormGroup({
        time: new FormControl(null),
        name: new FormControl(null),
        phone: new FormControl(null),
        status: new FormControl(null),
    });

    get time() { return this.formSearch.get('time') as FormControl };
    get name() { return this.formSearch.get('name') as FormControl };
    get phone() { return this.formSearch.get('phone') as FormControl };
    get status() { return this.formSearch.get('status') as FormControl };

    validator_value = {
        roomId: [
            {
                type: 'required',
                message: 'Trường này không được để trống.'
            },
            {
                type: 'pattern',
                message: 'Trường này sai định dạng.'
            },
        ]
    }

    formService = new FormGroup({
        roomId: new FormControl(null, [Validators.required]),
        note: new FormControl(null),
    });

    get roomId() { return this.formService.get('roomId') as FormControl };
    get note() { return this.formService.get('note') as FormControl };

    services: any = [];
    bookingServices: any = [

    ]

    constructor(
        public commonService: CommonService,
        private bookingService: BookingService,
        private router: Router,
        private alertService: AlertService,
        private roomService: RoomService,
        private modalService: NgbModal
    ) {

    }

    ngOnInit(): void {
        this.getBookings(this.paging);
        this.getRooms();
        this.getService();
        this.initService();
    }

    initService() {
        this.bookingServices.push({
            service: {
                id: null
            },
            quantity: null,
            price: 0
        })
    }

    changeQuantity(e: any, booking: any) {
        if (e) {
            booking.quantity = Number(e.target.value);
        }
        console.log(this.bookingServices)
    }

    choosePrice(e: any, booking: any) {
        if (e) {
            booking.price = e.price;
            booking.service.id = e.id;
        }
    }

    delete(index: any) {
        this.bookingServices.splice(index, 1);
    }

    buy() {
        let data = { ...this.formService.value, serviceLogs: this.bookingServices };
        this.bookingService.bookingService(data).subscribe((res: any) => {
            if (res.code == 200) {
                this.getBookings({ page: 1, pageSize: 20 });
                this.alertService.fireSmall("success", "Mua thêm dịch vụ thành công");
                this.modalService.dismissAll()
            } else if (res.code == 121) {
                this.alertService.fireSmall("error", "Số lượng sản phẩm vượt mức trong kho");
            }
        })
    }

    getBookings(page: any, filter?: any) {
        this.isLoading = true;
        this.bookingService.getBookings(page, filter).subscribe((res: any) => {
            if (res.code == 200) {
                this.listBooking = res.objects.map((item: any) => {
                    item.priceBefore = item.finalPrice;
                    item.service = item.bookingDetail?.finalServiceAmount ? item.bookingDetail?.finalServiceAmount: 0;
                    if(item.bookingDetail?.finalServiceAmount) {
                       item.finalPrice += item.bookingDetail.finalServiceAmount;
                    }
                    item.tax = item.finalPrice * 10/100;
                    item.total = item.finalPrice + item.tax;
                    return item;
                });
                this.paging.total = res.paging?.total;
                this.paging.page = res.paging?.page;
            } else {
                this.alertService.fireSmall("error", "Có lỗi xảy ra, vui lòng thử lại")
            }
            this.isLoading = false
        })
    }

    getRooms() {
        this.roomService.getRooms({ page: 1, pageSize: 100 }).subscribe(res => {
            if (res.code == 200) {
                this.listRoom = res.objects;
            }
        })
    }

    getService() {
        this.roomService.getServices({ page: 1, pageSize: 1000 })
            .subscribe(res => {
                if (res.code == 200) {
                    this.services = res.objects;
                } else if (res.code == 401) {
                    localStorage.clear();
                    this.router.navigate(['/auth/login']);
                } else if (res.code == 403) {
                    this.router.navigate(['error/403'])
                } else {
                    this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
                }
            })
    }


    resetForm() {
        this.configs = 0;
        this.formSearch.reset();
        this.getBookings({ page: 1, size: 20 })
    }

    chooseStatus(event: any, id: any) {
        this.configs = id
        if (event) this.status.setValue(event);
        this.filter();
    }

    filter() {
        let timeBooking: any;
        if (this.time.value) {
            timeBooking = moment(this.time.value).format("yyyy-MM-DD")
        }
        let queryParam = {
            customer: this.name.value ? this.name.value : null,
            time: timeBooking ? timeBooking : null,
            phone: this.phone.value ? this.phone.value : null,
            status: this.status.value ? this.status.value : null
        };
        this.commonService.paramURL(queryParam, 'booking');
        this.paging.page = 1;
        this.getBookings(this.paging, queryParam);
    }

    openModal(content: any) {
        this.formService.reset();
        this.bookingServices = [];
        this.initService();
        this.modalService.open(content, {
            windowClass: 'modal-account',
            size: "lg",
            centered: true
        });
    }

    changePage(event: any) {
        this.paging.page = event.page;
        // this.filter();
        // this.getBookings(this.paging)
    }

    checkInOut(id: string, type: number, time?: any) {
        let status = "";
        switch (type) {
            case 0: status = "SUCCESS"; break;
            case 1: status = "CHECKIN"; break;
            case 2: status = "CHECKOUT"; break;
            case 3: status = "DONE"; break;
        }
        let body = {
            "bookingId": id,
            "status": status
        }

        if (time && !this.checkTime(time)) {
            this.alertService.fireSmall('error', `Chưa đến thời gian ${type == 1 ? 'check in' : 'check out'}`);
            return;
        }
        this.isLoading = true;
        this.bookingService.updateBooking(body).subscribe((res: any) => {
            if (res.code == 200) {
                this.alertService.fireSmall('success', "Cập nhật trạng thái đặt phòng thành công")
                this.getBookings({ page: 1, pageSize: 20 });
            } else if (res.code == 204) {
                this.alertService.fireSmall('error', "Không tìm thấy đơn đặt phòng")

            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại")
            }
            this.isLoading = false;
            // else if(res.code == 200) {
            //     this.getBookings({ page: 1, pageSize: 20 });
            // }  else if(res.code == 200) {
            //     this.getBookings({ page: 1, pageSize: 20 });
            // }  
        })
    }

    getNameStatusBooking(status: string) {
        switch (status) {
            case "DRAFT": return 'Đơn nháp';
            case "SUCCESS": return 'Thành công';
            case "CHECKIN": return 'CHECKIN';
            case "CHECKOUT": return 'CHECKOUT';
            case "DONE": return 'Hoàn thành';
            case "CANCEL": return 'Hủy bỏ';
        }
    }

    checkTime(time: any) {
        if (time) {
            let now = moment().format("DD/MM/YYYY");
            if (time == now) return true;
            return false;
        }
    }

    getClassNameStatus(status: any) {
        switch (status) {
            case "DRAFT": return 'badge-light-warning';
            case "SUCCESS": return 'badge-light-success';
            case "CHECKIN": return 'badge-light-primary';
            case "CHECKOUT": return 'badge-light-success';
            case "DONE": return 'badge-light-success';
            case "CANCEL": return 'badge-light-danger';
        }
    }

    getNameStatusRoom(id: number) {
        switch (id) {
            case 1: return 'Phòng đơn';
            case 2: return 'Phòng đôi';
            case 3: return 'Phòng vip';
            case 4: return 'Phòng khác';
        }
    }

    routers() {
        this.router.navigate(['/booking/create'])
    }

    bookingId: any;
    reason: any;
    cancel(content: any, id: any) {
        if (id) {
            this.bookingId = id;
            this.modalService.open(content, {
                size: 'lg',
                centered: true
            })
        }
    }

    cancelBooking() {
        let body = {
            "bookingId": this.bookingId,
            "note": this.reason
        }
        this.alertService.fireConfirmYes("Bạn có chắc muốn hủy hay không").then((result: any) => {

            if (result.isConfirmed) {
            this.isLoading = true;

                this.bookingService.cancel(body).subscribe((res: any) => {
                    if (res.code == 200) {
                        this.alertService.fireSmall("success", "Hủy đặt phòng thành công")
                        this.getBookings({ page: 1, pageSize: 20 });
                        this.modalService.dismissAll();
                        this.bookingId = null
                        this.reason = null
                    }
                    this.isLoading = false;
                })
            }

        })

    }

}
