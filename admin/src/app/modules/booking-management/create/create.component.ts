import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import moment from 'moment';
import { timeout } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { BookingService } from 'src/app/services/booking.service';
import { CommonService } from 'src/app/services/common.service';
import { RoomService } from 'src/app/services/room.service';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

    isLoading = false;
    started_at: any = null;
    ended_at: any = null;

    constructor(
        private router: Router,
        private bookingService: BookingService,
        public commonService: CommonService,
        private roomService: RoomService,
        private alertService: AlertService
    ) { }

    ngOnInit(): void {
        // this.getRoom({ page: 1, pageSize: 1000 })
        this.getRoomType({ page: 1, pageSize: 10 })
    }

    isNext = false;
    rooms: any = [];

    formSearch = new FormGroup({
        checkIn: new FormControl(null, Validators.required),
        checkOut: new FormControl(null, Validators.required),
        numberPassenger: new FormControl(null, Validators.required),
        roomNo: new FormControl(null, Validators.required),
    });

    get checkIn() { return this.formSearch.get('checkIn') as FormControl };
    get checkOut() { return this.formSearch.get('checkOut') as FormControl };
    get numberPassenger() { return this.formSearch.get('numberPassenger') as FormControl };
    get roomNo() { return this.formSearch.get('roomNo') as FormControl };

    validator_value = [
        {
            type: 'required',
            message: 'Trường này không được để trống.'
        },
        {
            type: 'pattern',
            message: 'Trường này sai định dạng.'
        },
    ];

    formBook = new FormGroup({
        checkIn: new FormControl({ value: null, disabled: true }, Validators.required),
        checkOut: new FormControl({ value: null, disabled: true }, Validators.required),
        numberPassenger: new FormControl({ value: null, disabled: true }, Validators.required),
        roomId: new FormControl(null, Validators.required),
        totalAmount: new FormControl(0, Validators.required),
        address: new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$')]),
        birthDay: new FormControl(null, Validators.required),
        email: new FormControl(null, [Validators.required, Validators.email]),
        firstName: new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$')]),
        gender: new FormControl(null, Validators.required),
        identityCardNumber: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]{10,12}$')]),
        lastName: new FormControl(null, [Validators.required, Validators.pattern('^[A-Za-z ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]+$')]),
        passPortNo: new FormControl(null, [Validators.pattern('^[0-9]{6,15}$')]),
        phone: new FormControl(null, [Validators.required, Validators.pattern('^0[1-9]{1}[0-9]{8}$')]),
    });

    get address() { return this.formBook.get('address') as FormControl }
    get birthDay() { return this.formBook.get('birthDay') as FormControl }
    get email() { return this.formBook.get('email') as FormControl }
    get firstName() { return this.formBook.get('firstName') as FormControl }
    get gender() { return this.formBook.get('gender') as FormControl }
    get identityCardNumber() { return this.formBook.get('identityCardNumber') as FormControl }
    get lastName() { return this.formBook.get('lastName') as FormControl }
    get phone() { return this.formBook.get('phone') as FormControl }
    get passPortNo() { return this.formBook.get('passPortNo') as FormControl }


    result: any;
    roomAvailable: any = [];
    totalAmounts = 0;
    roomTypes: any = [];
    statusAccount: any = [
        {
            id: 1,
            name: 'Nam'
        },
        {
            id: 0,
            name: 'Nữ'
        }
    ];

    isReset = false;

    time: any = moment();



    handleDate($event: any) {
        console.log($event)
        if ($event) {
            this.checkIn.setValue($event.start);
            this.checkOut.setValue($event.end);
        }
    }

    search() {
        if (this.formSearch.invalid) {
            this.alertService.fireSmall('error', "Các trường không được để trống");
            return;
        }
        let timeIn: any = this.checkIn.value ? moment(this.checkIn.value) : null ;
        let timeOut: any = this.checkOut.value ? moment(this.checkOut.value) : null;
        if(!timeIn || !timeOut) {
            this.alertService.fireSmall('error', "Vui lòng chọn ngày bắt đầu hoặc kết thúc");
            return;
        }
        if (timeOut.isBefore(timeIn)) {
            this.alertService.fireSmall('error', "Ngày kết thúc bằng hoặc sau ngày bắt đầu");
            return;
        }
        let data = {
            "checkIn": timeIn.format("YYYY-MM-DD"),
            "checkOut": timeOut.format("YYYY-MM-DD"),
            "numberPassenger": this.numberPassenger.value,
            "roomNo": this.roomNo.value,
            "roomStatus": "REPAIR",
            "bookingStatus": "CANCEL"
        }
        this.isLoading = true;
        this.bookingService.getRoomAvailable(data).subscribe((res: any) => {
            if (res.code == 200) {
                this.roomAvailable = res.objects?.filter((item: any) => item.roomStatus == 'AVAILABLE');
                if (this.roomAvailable.length > 0) {
                    this.formBook.patchValue({
                        checkIn: this.checkIn.value,
                        checkOut: this.checkOut.value,
                        numberPassenger: this.numberPassenger.value
                    });
                    this.isReset = true;
                } else {
                    this.alertService.fireSmall('error', "Không tìm thấy phòng phù hợp trong thời gian này");
                }
            } else {
                this.alertService.fireSmall('error', "Không tìm thấy phòng phù hợp trong thời gian này");
            }
            this.isLoading = false;

        })
    }

    chooseRoom(e: any) {
        if(e) {
            let timeIn: any = this.checkIn.value ? moment(this.checkIn.value) : null ;
            let timeOut: any = this.checkOut.value ? moment(this.checkOut.value) : null;
            if(!timeIn || !timeOut) {
                this.alertService.fireSmall('error', "Vui lòng chọn ngày bắt đầu hoặc kết thúc");
                return;
            }
            if(!e.price) {
                this.alertService.fireSmall('error', "Giá phòng không hợp lệ");
                return;
            }
            let totalDate = timeOut.diff(timeIn, "days") + 1;
            this.totalAmounts = Number(e.price) * totalDate;
        }
    }
    book() {
        if (this.formBook.invalid) {
            this.alertService.fireSmall('error', "Các trường không được để trống");
            return;
        }
        let timeIn = moment(this.checkIn.value);
        let timeOut = moment(this.checkOut.value);
        let dob = moment(this.birthDay.value)
        if (timeOut.isBefore(timeIn)) {
            this.alertService.fireSmall('error', "Thời gian checkout bằng hoặc sau thời gian checkin");
            return;
        }
        let user_id = localStorage.getItem('user_id');
        if(!user_id) {
            this.router.navigate(['/auth/login']);
        }
        if (this.totalAmounts <= 0) {
            this.alertService.fireSmall('error', "Tổng tiền không hợp lệ");
            return;
        }
        if (this.numberPassenger.value && parseInt(this.numberPassenger.value) <= 0) {
            this.alertService.fireSmall('error', "Số lượng khách không hợp lệ");
            return;
        }
        let data = {
            "checkIn": timeIn.format("DD/MM/YYYY"),
            "checkOut": timeOut.format("DD/MM/YYYY"),
            "roomId": this.formBook.value.roomId,
            "staffId": user_id,
            "totalAmount": this.totalAmounts,
            "numberPassenger": this.numberPassenger.value,
            "customerDTO": {
                "address": this.address.value,
                "birthDay": dob.format('DD/MM/YYYY'),
                "email": this.email.value,
                "firstName": this.firstName.value,
                "gender": this.gender.value == 1 ? true: false,
                "identityCardNumber": this.identityCardNumber.value,
                "lastName": this.lastName.value,
                "passPortNo": this.passPortNo.value ? this.passPortNo.value : null,
                "phone": this.phone.value
            }
        }
        this.isLoading = true;
        this.bookingService.createBook(data).subscribe((res: any) => {
            if(res.code == 200 || res.code == 201) {
                this.alertService.fireSmall('success', "Đặt phòng thành công");
                this.router.navigate(['/booking']);
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }
            this.isLoading = false;
        })
    }

    getRoomType(paging: any) {
        this.isLoading = true;
        this.roomService.getRoomType(paging).subscribe((res : any)=> {
            if (res.code == 200) {
                this.roomTypes = res.objects;
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }

            this.isLoading = false;
        })
    }

    getRoom(paging: any, filter?: any) {
        this.isLoading = true;
        this.roomService.getRoomType(paging).subscribe(res => {
            if (res.code == 200) {
                this.rooms = res.objects;
            } else if (res.code == 401) {
                localStorage.clear();
                this.router.navigate(['/auth/login']);
            } else if (res.code == 403) {
                this.router.navigate(['error/403'])
            } else {
                this.alertService.fireSmall('error', "Có lỗi xảy ra, vui lòng thử lại");
            }

            this.isLoading = false;
        })
    }

    handleSelectDate(event: any) {
        console.log(event)
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any) {
        return fieldName.hasError(errorType) && (fieldName.dirty || fieldName.touched);
    }
    checkInputDate() {

    }

}
