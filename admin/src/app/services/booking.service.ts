import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, catchError, map, of, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';
import { ErrorService } from './error.service';

@Injectable({
    providedIn: 'root'
})
export class BookingService extends BaseService {

    apiHost = `${environment.apiHost}/`;
    constructor(
        public http: HttpClient,
        public errorService: ErrorService,
        private router: Router
    ) {
        super(http);
    }

    getBookings(paging: any, filter: any) {
        let params = new HttpParams();
        if (paging?.page != null) params = params.append('page', paging.page-1);
        if (paging?.pageSize != null) params = params.append('size', paging.pageSize);
        if (filter?.customer != null) params = params.append('nameCustomer', filter.customer);
        if (filter?.time != null) params = params.append('dateBooking', filter.time);
        if (filter?.status != null) params = params.append('bookingStatus', filter.status);
        if (filter?.phone != null) params = params.append('phone', filter.phone);

        let url = `${this.apiHost}system/bookings`;
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('booking@info')),
                map(this.errorService.handleMapData()));
    }

    getExpense(data: any) {
        
        let url = `${this.apiHost}admin/expenses`;
        return this.http.post(url,data, { headers: this.requestHeaders() })
            .pipe(catchError((err) => of(null)));
    }

    getIncome(data: any) {
        
        let url = `${this.apiHost}admin/income`;
        return this.http.post(url,data, { headers: this.requestHeaders() })
        .pipe(catchError((err) => of(null)));

    }

    getPassengers(filter: any) {
        let params = new HttpParams();
        if(filter.started && filter.enabled) {
            params = params.append('fromTime', filter.started);
            params = params.append('toTime', filter.ended);
        }
        let url = `${this.apiHost}admin/passengers`;
        return this.http.get(url,{ headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('income@info')),
                map(this.errorService.handleMapData()));
    }

    getRoomAvailable(data: any) {
        let url = `${this.apiHost}system/roomAvailable`;
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(
                catchError(this.errorService.handleError('roomAvailable@info')),
                map(this.errorService.handleMapData()));
    }

    updateBooking(data: any) {
        let url = `${this.apiHost}system/update/book`;
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(
                catchError(this.errorService.handleError('update/book@info')),
                map(this.errorService.handleMapData()));
    }

    createBook(data: any) {
        let url = `${this.apiHost}system/book`;
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('book@book')), 
            map(this.errorService.handleMapData()));
    }

    bookingService(data: any) {
        let url = `${this.apiHost}system/book/service`;
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('book@bookService')), 
            map(this.errorService.handleMapData()));
    }

    cancel(data: any) {
        let url = `${this.apiHost}system/cancel`;
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('book@Cancel')), 
            map(this.errorService.handleMapData()));
    }
}
