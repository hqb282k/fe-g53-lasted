import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpParams } from "@angular/common/http";
import { ErrorService } from "./error.service";
import { catchError, delay } from "rxjs/operators";
import permissionName from 'src/app/shared/permission-file/permission-name';

@Injectable({
    providedIn: 'root'
})
export class RoleService extends BaseService {

    api = `${environment.apiHost}/admin`;

    permission = permissionName.role;
    
    constructor(
        public http: HttpClient,
        public errorService: ErrorService,
    ) {
        super(http);
    }

    getRoles(page: any, pageSize?: any) {
        let url = this.api + '/getRoleList';
        let params = new HttpParams()
            .set('page', page);

        if (pageSize) {
            params = params.append('pageSize', pageSize);
        }
        return this.http.get(url, { headers: this.requestHeaders(this.permission.index), params: params })
            .pipe(
                catchError(this.errorService.handleError('get roles', {
                    'data': {}
                })),
                delay(1000));
    }

    getDetailRole(id: any) {
        let url = this.api + '/role/show/' + id;
        return this.http.get(url, { headers: this.requestHeaders(this.permission.update) })
            .pipe(catchError(this.errorService.handleError('get detailRole', {
                'data': {}
            })));
    }

    createOrUpdateRole(data: any, id: any) {
        let url = `${this.api}/role/`;
        if (id) {
            url += `update/${id}`;
            return this.http.put(url, data, { headers: this.requestHeaders(this.permission.update) })
                .pipe(
                    catchError(this.errorService.handleError('update role', {
                        'data': {}
                    })),
                    delay(1000));
        }
        url += 'create';
        return this.http.post(url, data, { headers: this.requestHeaders(this.permission.create) })
            .pipe(
                catchError(this.errorService.handleError('create role', {
                    'data': {}
                })),
                delay(1000));
    }

}
