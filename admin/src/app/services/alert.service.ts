import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor() { }

    fireSmall(type: any, title: any) {
        Swal.fire({
            icon: type,
            title: title,
            showConfirmButton: false,
            toast: true,
            position: 'top-end',
            timer: 3000
        });
    }

    fireConfirmYes(title: string) {
        return Swal.fire({
            title: title,
            showDenyButton: true,
            confirmButtonText: 'Đồng ý',
            denyButtonText: `Hủy bỏ`,
          });
    }

    fireLoad(type: any, title: any) {
       return Swal.fire({
            icon: type,
            title: title,
            showConfirmButton: false,
            toast: true,
            position: 'top-end',
            timer: 3000
        });
    }
}
