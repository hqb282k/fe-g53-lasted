import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpParams } from "@angular/common/http";
import { ErrorService } from "./error.service";
import { catchError } from "rxjs/operators";
import permissionName from 'src/app/shared/permission-file/permission-name';

@Injectable({
    providedIn: 'root'
})
export class PermissionService extends BaseService {

    api = `${environment.apiHost}/acl`;
    
    permission = permissionName.permission;

    constructor(
        public http: HttpClient,
        public errorService: ErrorService
    ) {
        super(http);
    }

    getPermissions(page: any, pageSize: any, filters: any, status: boolean): Observable<any> {
        let params = new HttpParams()
            .set('page', page)
            .set('pageSize', pageSize);
        if (filters.group_type) params = params.append('group_type', filters.group_type);
        if (filters.name) params = params.append('name', filters.name);
        if (filters.id) params = params.append('id', filters.id);
        if (filters.pageSize) params = params.append('id', filters.id);

        let url = this.api + '/permission/lists';
        if (status) {
            return this.http.get(url, { headers: this.requestHeaders(this.permission.index), params: params })
                .pipe(catchError(this.errorService.handleError('get permissions', {
                    'data': {}
                })));
        }
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('get permissions', {
                'data': {}
            })));
    }

    getGroupType() {
        let url = this.api + '/permission/config-type';
        return this.http.get(url, { headers: this.requestHeaders()})
            .pipe(catchError(this.errorService.handleError('get config_type', {
                'data': {}
            })));
    }

    getDetailPermission(id: any): Observable<any> {
        let url = this.api + '/permission/show/' + id;
        return this.http.get(url, { headers: this.requestHeaders(this.permission.update) })
            .pipe(catchError(this.errorService.handleError('get detailPermission', {
                'data': {}
            })));
    }

    createOrUpdatePermission(data: any, id: any): Observable<any> {
        let url = `${this.api}/permission/`;
        data.group_type = Number(data.group_type);
        if (id) {
            url += `update/${id}`;
            return this.http.put(url, data, { headers: this.requestHeaders(this.permission.update) })
                .pipe(catchError(this.errorService.handleError('update permission', {
                    'data': {}
                })));
        }
        url += 'create';
        return this.http.post(url, data, { headers: this.requestHeaders(this.permission.create) })
            .pipe(catchError(this.errorService.handleError('create permission', {
                'data': {}
            })));
    }
}
