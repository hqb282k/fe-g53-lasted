import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class BaseService {

    constructor(public http: HttpClient) { }

    public requestHeaders(permission?: string) {
        const token = localStorage.getItem('access_token');
        if (permission) {
            let permissionHeader = `${permission}|full`;
            return new HttpHeaders({
                'Authorization': 'Bearer ' + token,
                'x-permission': permissionHeader
            });
        } else {
            return new HttpHeaders({
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
                'x-permission': 'full'
            });
        } 
    }

    public rqSSOToken() {
        const token = localStorage.getItem('token');
        console.log(token)
        return new HttpHeaders({
            'Authorization': 'Bearer ' + token,
        });
    }
}
