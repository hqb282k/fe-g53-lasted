export { CommonService } from "./common.service"
export { AlertService } from "./alert.service";
export { ProfileService } from "./profile.service";
export { PermissionService } from "./permission.service";
export { RoleService } from "./role.service";
export { UserService } from "./user.service";
export { CustomerService } from "./customer.service";
export { BookingService } from "./booking.service";
export { RoomService } from "./room.service";
export const roomType = [
    {
        id: 1,
        name: 'Phòng Đơn'
    },
    {
        id: 2,
        name: 'Phòng Đôi'
    },
    {
        id: 3,
        name: 'Phòng Vip'
    },
    {
        id: 4,
        name: 'Phòng khác'
    },
];

export const statusRoom = [
    {
        id: 1,
        name: 'Đang hoạt động'
    },
    {
        id: 2,
        name: 'Chờ duyệt'
    },
    {
        id: 3,
        name: 'Còn phòng'
    },
];

