import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient, HttpParams } from "@angular/common/http";
import { ErrorService } from "./error.service";
import { catchError, delay } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import permissionName from 'src/app/shared/permission-file/permission-name';

@Injectable({
    providedIn: 'root'
})
export class UserService extends BaseService {

    apiCreateStaff = `${environment.apiHost}/admin/createStaff`;
    apiAdmin = `${environment.apiHost}/`;

    modalSubject = new BehaviorSubject({});

    permission = permissionName.user;

    constructor(
        public http: HttpClient,
        public errorService: ErrorService,
    ) {
        super(http);
    }

    getListsUser(paging: any, filters: any, type?: string) {
        let params = new HttpParams()
            .set('page', paging.page - 1)
            .set('size', paging.pageSize)

        if (filters && filters.emailSearch != null) params = params.append('emailSearch', filters.emailSearch);
        if (filters && filters.nameSearch != null) params = params.append('nameSearch', filters.nameSearch);
        let url = `${this.apiAdmin}${type}`;
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(
                catchError(this.errorService.handleError('get accounts')),
                delay(1000));
    }

    getAccount(paging: any, filters: any, type?: string) {
        let params = new HttpParams()
            .set('page', paging.page - 1)
            .set('pageSize', paging.pageSize)

        // if(filters.email != null) params = params.append('email', filters.email);
        // if(filters.name != null) params = params.append('fullName', filters.name);
        let url = `${this.apiAdmin}${type}`;
        return this.http.get(url, { headers: this.requestHeaders(), params: params})
            .pipe(
                catchError(this.errorService.handleError('get accounts')),
                delay(1000));
    }

    showUser(id: any) {
        let url = this.apiAdmin + 'system/getStaff/' + id;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('show account')));
    }

    getAccountId(id: any) {
        let url = this.apiAdmin + 'admin/getAccount/' + id;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('show account')));
    }

    activeAccount(statusAll: any, id: any) {
        let url = this.apiAdmin + '/user/active';
        let data: any;
        if (statusAll) {
            data = {
                'user_ids': id
            };
        } else {
            url += `/${id}`;
        }
        return this.http.put(url, data, { headers: this.requestHeaders(this.permission.active) })
            .pipe(catchError(this.errorService.handleError('active account')));
    }

    inActiveAccount(statusAll: any, id: any) {
        let url = this.apiAdmin + '/user/cancel';
        let data: any;
        if (statusAll) {
            data = {
                'user_ids': id
            };
        } else {
            url += `/${id}`;
        }
        return this.http.put(url, data, { headers: this.requestHeaders(this.permission.active) })
            .pipe(catchError(this.errorService.handleError('inactive account')));
    }

    createOrUpdateAccount(data: any, id?: any) {
        let url = '';
        if (id && id > 0) {
            data.id = id;
            let body = {
                objectRequest: data
            }
            url = `${this.apiAdmin}admin/updateStaff`;
            return this.http.post(url, body, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('update account')),
                    delay(1000));
        } else {
            url = `${this.apiCreateStaff}`;
            let body = {
                objectRequest: data
            }
            return this.http.post(url, body, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('create account')),
                    delay(1000));
        }

    }

    createAccount(data: any, id?: any) {
        if (id != null) {
            data.id = id;
            delete data.password;
            let url = `${this.apiAdmin}admin/updateAccount`;
            let body = {
                objectRequest: data
            }
            return this.http.post(url, body, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('create account')),
                    delay(1000));
        }
        let url = `${this.apiAdmin}admin/createAccount`;
        let body = {
            objectRequest: data
        }
        return this.http.post(url, body, { headers: this.requestHeaders() })
            .pipe(
                catchError(this.errorService.handleError('create account')),
                delay(1000));
    }
}
