import { ProfileService } from './profile.service';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CommonService{

    dataUser: any;
    role: any;

    constructor(
        private router: Router,
        private profileService: ProfileService,
        private http: HttpClient
    ) {
        const accessToken = localStorage.getItem('access_token');
        let headers = new HttpHeaders({
        'Authorization': 'Bearer ' + accessToken,
        });
        this.profileService.currentData.subscribe((data: any) => {
            this.dataUser = data?.staff;
            if(this.dataUser?.role)
            {
                this.role = this.dataUser.role.roleName;
            }
        }, err => {
            console.log(err);
            this.role = null;
        });
    }

    checkNameGroup(groupType: any, id: any) {
        if (groupType?.length > 0) {
            let value = groupType.filter((item: any) => item?.id == id);
            if (value.length > 0) {
                return value[0]?.name;
            }
            return '';
        }
        return '';
    }

    formatTime(date: any, str: string) {
        if (date && str) return moment(date).format(str);
        return '';
    }

    convertTime(time: any) {
        let arrTime = time.split(" ");
        let timeChange: any = new Date(`${arrTime[0]} ${arrTime[1]} ${arrTime[2]} ${arrTime[3]}`).toLocaleDateString('es-US');
        return timeChange.split("/").reverse().join("-");
    }

    paramURL(queryParams: any, routeName: any) {
        this.router.navigate([routeName], { queryParams: queryParams });
    }

    getScreenWidth() {
        return window.innerWidth;
    }

    checkExistInSelectOrder(arrId: any, listResults: any, statusAll: boolean, id?: any) {
        if (statusAll) {
            return arrId.length == listResults.length;
        }
        return arrId.some((item: any) => item == id);
    }

    dirtyOrTouchShowError(fieldName: any, errorType: any, formGroup: FormGroup) {
        return formGroup.controls[`${fieldName}`].hasError(errorType) && (formGroup.controls[`${fieldName}`].dirty || formGroup.controls[`${fieldName}`].touched)
    }

    dirtyOrTouchFieldCheck(fieldName: any, formGroup: FormGroup) {
        if (!formGroup.controls[`${fieldName}`].dirty && !formGroup.controls[`${fieldName}`].touched) {
            return false;
        }
        return (formGroup.controls[`${fieldName}`].dirty || formGroup.controls[`${fieldName}`].touched) && formGroup.controls[`${fieldName}`].status == 'INVALID' ? true : false
    }

    onTrimOutForm(event: any,formName: any, control: any) {
        if(event) {
            formName.controls[`${control}`].setValue(event.target.value.trim());
        } else {
            formName.controls[`${control}`].setValue(null);
        }
    }

    onTrimOut(value: any, control: any) {
        if(value) {
            control = value.trim();
        } else {
            control = null;
        }
    }

    checkPermission(roles: any) {
        if(!roles) return true;
        let roleArray = roles.split('|');

        if(roleArray.includes(this.role)) return true;
        return false;
    }
}
