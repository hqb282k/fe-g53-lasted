import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { catchError, delay } from "rxjs/operators";
import { BehaviorSubject } from 'rxjs';
import { ErrorService } from './error.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileService extends BaseService {

    api = `${environment.apiHost}`;
    apiHost = `${environment.apiHost}/`;
    profileSubject = new BehaviorSubject(null);
    constructor(
        public http: HttpClient,
        public errorService: ErrorService
    ) {
        super(http);
    }

    private dataSource = new BehaviorSubject(null);
    currentData = this.dataSource.asObservable();

    changeData(data: any) {
        this.dataSource.next(data);
    }

    getInfo(id: number) {
        let url = this.api + '/profile';
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')));
    }

    updateProfile(data: any) {
        let url = this.apiHost + 'update/profile';
        console.log(data)
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('ProfileService@updateInfo')));
    }

    changePassword(data: any) {
        let url = this.api + '/change/password';
        return this.http.post(url, data, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('ProfileService@change-password')));
    }
}
