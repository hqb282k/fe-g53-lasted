import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private router: Router
  ) { }

  handleError(operation = 'operation', result?:any) {
    return (error: any): Observable<any> => {
      console.error(`${operation} failed: ${error.message}`);
      return of({code: error.status})
    };
  }

  handleMapData() {
    return (result: any): Observable<any> => {
        if(result.code == 401) {
            localStorage.clear();
            this.router.navigate(['/auth/login']);
        } else if(result.code == 403) {
            this.router.navigate(['error/403'])
        }
        return result;
    };
  }
}
