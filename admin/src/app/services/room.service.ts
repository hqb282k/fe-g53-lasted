import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, delay, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseService } from './base.service';
import { ErrorService } from './error.service';

@Injectable({
    providedIn: 'root'
})
export class RoomService extends BaseService {

    data = [
        {
            id: 1,
            name: 'R1',
            booking: 1,
            room_type: 1,
            price_per_day: 25000,
            bed: 2

        },
        {
            id: 2,
            name: 'R2',
            booking: 2,
            room_type: 2,
            price_per_day: 25000,
            bed: 3

        },
        {
            id: 3,
            name: 'R3',
            booking: 3,
            room_type: 1,
            price_per_day: 25000,
            bed: 1

        },
        {
            id: 4,
            name: 'R4',
            booking: 2,
            room_type: 3,
            price_per_day: 25000,
            bed: 4

        },
        {
            id: 5,
            name: 'R5',
            booking: 1,
            room_type: 1,
            price_per_day: 25000,
            bed: 2

        },
    ];

    api = `${environment.apiHost}/staff`;

    apiService = `${environment.apiHost}/system`;
    apiHost =`${environment.apiHost}`

    constructor(
        public http: HttpClient,
        public errorService: ErrorService
    ) {
        super(http);
    }

    getRoomType(paging?: any, filter?: any) {
        let params = new HttpParams();
        if (paging) {
            params = params.append("page", paging.page - 1);
            params = params.append("size", paging.pageSize);
        }
        let url = this.apiHost + '/room/types';
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')), delay(500));
    }

    getRooms(paging?: any, filter?: any) {
        let params = new HttpParams()
            .set("page", paging.page - 1)
            .set("size", paging.pageSize);

        if (filter?.typeId != null) params = params.append('typeId', filter.typeId);
        if (filter?.code != null) params = params.append('roomNo', filter.code);

        let url = this.apiService + '/rooms';
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
        .pipe(catchError(this.errorService.handleError('room@info')), delay(500));
    }

    getRoomTypeId(id: number) {
        let url = `${this.apiHost}/room/type/${id}`;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')), delay(500));
    }

    getRoomId(id: number) {
        let url = `${this.apiService}/room/${id}`;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')));
    }

    createRoomType(data: any, id?: number) {
        if (id != null) {
            let url = `${this.api}/modify/room/type`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomtype')));
        } else {
            let url = `${this.api}/modify/room/type`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomtype')));
        }

    }

    createRoom(data: any, id?: number) {
        if (id != null) {
            let url = `${this.api}/update/room`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomtype')));
        } else {
            let url = `${this.api}/create/room`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomtype')));
        }

    }

    //Room Option

    createRoomOption(data: any, id?: number) {
        if (id != null) {
            let url = `${this.api}/update/room/option`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomOption')));
        } else {
            let url = `${this.api}/create/room/option`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomOption')));
        }

    }

    getRoomOption(paging: any, filter?: any) {
        let params = new HttpParams()
            .set("page", paging.page - 1)
            .set("size", paging.pageSize);
        if (filter?.name != null) params = params.append('name', filter.name);
        let url = this.api + '/room/options';
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')));
    }

    //Service type
    getServiceType(paging: any, filter?: any) {
        let params = new HttpParams()
            .set("page", paging.page - 1)
            .set("size", paging.pageSize);
            console.log(paging)
        if (filter?.name != null) params = params.append('name', filter.name);
        let url = this.api + '/service/types';
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')));
    }

    detailServiceType(id: number) {
        let url = this.api + '/service/type/' + id;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('detailType@info')));
    }

    handleServiceType(data: any, id?: number) {
        if (id != null) {
            let url = `${this.api}/update/service/type`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('servicetype')),
                    delay(1000));
        } else {
            let url = `${this.api}/create/service/type`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('servicetype')),
                    delay(1000));
        }

    }

    deleteServiceType(id: number) {
        let url = this.api + '/delete/service/type/' + id;
        return this.http.delete(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('deleteServiceType')), delay(1000));
    }

    //Service

    getServices(paging: any, filter?: any) {
        let params = new HttpParams()
            .set("page", paging.page - 1)
            .set("size", paging.pageSize);

        if (filter && filter.name != null) params = params.append('name', filter.name);
        if (filter && filter.status != null) params = params.append('isEnable', filter.status)

        let url = this.apiService + '/services';
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')));
    }

    deleteService(id: number) {
        let url = this.api + '/delete/post/' + id;
        return this.http.post(url, null, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('deletePost@info')), delay(1000));
    }

    getDetailService(id: number) {
        let url = this.apiService + '/service/' + id;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('ProfileService@info')));
    }

    createUpdateService(data: any, id?: number) {
        if (id != null) {
            let url = `${this.api}/update/service`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomtype')),
                    delay(1000));
        } else {
            let url = `${this.api}/create/service`;
            return this.http.post(url, data, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createroomtype')),
                    delay(1000));
        }

    }


    //upload img

    uploadFile = environment.apiImg;
    uploadImage(selectedFile: any, folder: string): Observable<any> {
        const uploadData = new FormData();
        uploadData.append('image', selectedFile, selectedFile.name);
        uploadData.append('folder_name', folder);
        return this.http.post(this.uploadFile + '/upload/image', uploadData, { headers: this.rqSSOToken() })
            .pipe(catchError(this.errorService.handleError('uploadImage')));
    }

    uploadMultiImages(files: any, folder: string): Observable<any> {
        const uploadData = new FormData();
        files.forEach((file: any, i: any) => {
            uploadData.append(file.fieldName, file.file, file.file.name);
        });
        uploadData.append('folder_name', folder);
        return this.http.post(this.uploadFile + '/upload/multi-image', uploadData, { headers: this.rqSSOToken() })
            .pipe(catchError(this.errorService.handleError('uploadMultiImages', {
                'data': {
                    'files': [],
                }
            })));
    }


    // Post
    createUpdatePost(data: any, id?: any) {
        let dataRq = {
            "objectRequest": data
        };
        if (id != null) {
            let url = `${this.api}/update/post`;
            return this.http.post(url, dataRq, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('updatePost')),
                    delay(1000));
        } else {
            let url = `${this.api}/create/post`;
            return this.http.post(url, dataRq, { headers: this.requestHeaders() })
                .pipe(
                    catchError(this.errorService.handleError('createPost')),
                    delay(1000));
        }

    }

    getPosts(paging: any, filter?: any) {
        let params = new HttpParams()
            .set("page", paging.page - 1)
            .set("size", paging.pageSize);

        let url = environment.apiHost + '/posts';
        return this.http.get(url, { headers: this.requestHeaders(), params: params })
            .pipe(catchError(this.errorService.handleError('getPosts@info')), delay(10));
    }

    getPostDetail(id: number) {
        let url = `${environment.apiHost}/post/${id}`;
        return this.http.get(url, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('getPostDetail@info')), delay(500));
    }

    deletePost(id: number) {
        let url = this.apiHost + '/admin/delete/post/' + id;
        return this.http.post(url, null, { headers: this.requestHeaders() })
            .pipe(catchError(this.errorService.handleError('deletePost@info')), delay(1000));
    }
}
