let permissionName = {
    user: {
        index: "user_index",
        create: "user_create",
        update: "user_update",
        active: "user_active",
        show: "user_show"
    },
    role: {
        index: "role_index",
        create: "role_create",
        update: "role_update"
    },
    permission: {
        index: "permission_index",
        create: "permission_create",
        update: "permission_update"
    },
    order: {
        index: "order_index",
        create: "order_create",
        update: "order_update",
        export: "order_export",
        cancel: "order_cancel",
        show: "order_show",
        closing: "order_closing"
    },
    activity: {
        index: "activity_index",
        create: "activity_create",
        update: "activity_update",
        show: "activity_show"
    }
}

export default permissionName;