import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
    selector: '[numbersOnly]'
})
export class NumbersOnlyInputDirective {

    @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

    @Input() onlyNumber: boolean = false;
  @Input() negativeNumber: boolean = false;
  @Input() limitLength: boolean = false;
  @Input() max?: any;
  @Input() min?: any;

  oldVal: any;
  countCharacter: number = 0;

  constructor(
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    setTimeout(() => {
      if (this.elementRef.nativeElement.value) {
        this.oldVal = this.elementRef.nativeElement.value;
        this.elementRef.nativeElement.value = this.insertComma(this.elementRef.nativeElement.value);
      }
    }, 0);
  }

  @HostListener('focusin', ['$event'])
  onFocusin(event:any) {
    let value = event.target.value;
    if (value.length >= 4) {
      this.countCharacter = 0;
    }
    if (value) {
      value = this.deleteComma(value);
      this.oldVal = value;
      this.elementRef.nativeElement.value = value;
      this.elementRef.nativeElement.select()
    }
  }

  @HostListener('focusout', ['$event'])
  onFocusout(event:any) {
    if (event.target.value) {
      event.target.value = this.insertComma(event.target.value)
    }
  }

  @HostListener('input', ['$event'])
  onInput(event: any): any {
    let value = event.target.value;
    if (value == '00') {
      this.elementRef.nativeElement.value = 0;
    }
    if (this.max && parseInt(value) > this.max) {
      this.ngModelChange.emit(this.oldVal);
      this.elementRef.nativeElement.value = this.oldVal;
    } else if (value == '') {
      this.oldVal = '';
      this.countCharacter = 0
    } else if (value == '-' || (value.length >= 1 && !isNaN(Math.sign(value)))) {
      this.oldVal = value;
      this.countCharacter = value.length;
    } else {
      this.ngModelChange.emit(this.oldVal);
      this.elementRef.nativeElement.value = this.oldVal;
    }
  }

  @HostListener('keypress', ['$event'])
  onKeypress(event: any) {
    let value = event.target.value;
    if (this.onlyNumber) {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (this.negativeNumber) {
        if (this.oldVal == '-') {
          this.oldVal = 0;
          this.ngModelChange.emit(this.oldVal);
          this.elementRef.nativeElement.value = this.oldVal;
          return false;
        } 
               
        if (charCode === 45) {
          if (this.oldVal == "-" || (this.oldVal && this.oldVal.toString().includes("-"))) {
            return false;
          }
          return true;
        }

       if (this.limitLength) {
        if (value > 0) {
          if (this.countCharacter >= 4) {
            return false;
          }
        } else {
          if (this.countCharacter >= 8) {
            return false;
          }
        }
       }
      }

      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }

      this.elementRef.nativeElement.value = this.elementRef.nativeElement.value.replace(/^0+/, ''); 
    }
    return true;
  }

  deleteComma(str: string) {
    return str.replace(/[,]/g, '');
  }

  insertComma(str: string) {
    if (str.match(/\./)) {
      return str.split('.')[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',') + '.' + str.split('.')[1];
    }
    if (this.countCharacter > 1) {
      str = str.replace(/^0+/, '');
    }
    return str.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  countStr(str: any, searchStr: any) {
    if (!searchStr || !str) return 0;
    let count = 0
    let position = str.indexOf(searchStr);
    while (position !== -1) {
      count++;
      position = str.indexOf(searchStr, position + count);
    }
    return count;
  }
}
