import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumbersOnlyInputDirective } from './numbers-only-input.directive';



@NgModule({
  declarations: [
    NumbersOnlyInputDirective
  ],
  exports: [
    NumbersOnlyInputDirective
  ],
  imports: [
    CommonModule
  ]
})
export class DirectiveModule { }
