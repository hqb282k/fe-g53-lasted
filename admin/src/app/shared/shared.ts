export { ComponentModule } from "./component/component.module";
export { PipeModule } from "./pipe/pipe.module";
export { DirectiveModule } from "./directive/directive.module";

export const ENUM_ROOM_OPTION = [
    {
        "value" : 1,
        "type" : "Đặc điểm"
    },
    {
        "value" : 2,
        "type" : "Phòng tắm và vật dụng phòng tắm",
    },
    {
        "value" : 3,
        "type" : "Giải trí"
    },
    {
        "value" : 4,
        "type" : "Tiện nghi"
    },
    {
        "value" : 5,
        "type" : "Bố trí và nội thất"
    },
    {
        "value" : 6,
        "type" : "Quần áo và Giặt ủi"
    },
    {
        "value" : 7,
        "type" : "Vật dụng an toàn và an ninh"
    }
]
