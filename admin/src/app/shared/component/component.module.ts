import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './pagination/pagination.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoadingComponent } from './loading/loading.component';
import { NgxLoadingModule } from 'ngx-loading';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { FormsModule } from '@angular/forms';
import { SearchComponent } from './search/search.component';
import { ChartLineComponent } from './chart-line/chart-line.component';
import { ChartCircleComponent } from './chart-circle/chart-circle.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';




@NgModule({
  declarations: [
    PaginationComponent,
    LoadingComponent,
    DatePickerComponent,
    SearchComponent,
    ChartLineComponent,
    ChartCircleComponent

  ],
  exports: [
    PaginationComponent,
    LoadingComponent,
    DatePickerComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    NgxDaterangepickerMd.forRoot({}),
    NgxLoadingModule.forRoot({})
  ],
  bootstrap: [DatePickerComponent]
})
export class ComponentModule { }
