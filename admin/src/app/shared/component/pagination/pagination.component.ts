import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

    @Input() paging: any;

    @Output() change = new EventEmitter();

    pageSize = [5, 10, 20, 50];

    constructor() { }

    ngOnInit(): void {
    }

    changePage(): void {
        this.change.emit(this.paging);
    }

    changePageSize(size: number) {
        this.paging.page = 1;
        this.paging.pageSize = size;
        this.change.emit(this.paging)
    }

    convertPage(total: number, size: number) {
        return Math.ceil(total % size === 0 ? total / size + 1 : total / size);
    }
}
