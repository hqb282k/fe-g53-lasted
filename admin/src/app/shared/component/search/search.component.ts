import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    @Input() filter: any;

    @Output() submit = new EventEmitter();
    @Output() reset = new EventEmitter();

    started_at: any;
    ended_at: any;

    isReset = true;

    code: string;
    status: string;
    // productName: string;
    // customer: string;
    // phone: string;

    constructor() { }

    ngOnInit(): void {
    }

    ngOnChanges(): void {
        if (!this.isReset) {
            this.started_at = "";
            this.ended_at = "";
            this.code = "";
            this.status = "";
            // this.productName = "";
            // this.customer = "";
            // this.phone = "";
        }
    }

    selectDate($event: any) {
        if ($event) {
            this.started_at = $event.start;
            this.ended_at = $event.end;
        }
    }

    filterOrder() {
        // let filter = {
        //     code: this.code,
        //     phone: this.phone,
        //     customer: this.customer,
        //     started: this.started_at,
        //     ended: this.ended_at,
        //     // product_name: this.productName,
        //     // shipping_code: this.shippingCode
        // }
        this.isReset = false;
        this.submit.emit(true);
    }

    resetForm() {
        // this.started_at = "";
        // this.ended_at = "";
        // this.orderCode = "";
        // this.shippingCode = "";
        // this.productName = "";
        // this.customer = "";
        // this.phone = "";
        // this.isReset = true;
        this.reset.emit(true);
    }

}
