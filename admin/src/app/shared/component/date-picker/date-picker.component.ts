import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import * as dayjs from 'dayjs'

@Component({
    selector: 'app-date-picker',
    templateUrl: './date-picker.component.html',
    styleUrls: ['./date-picker.component.scss']
})
export class DatePickerComponent implements OnInit, OnChanges {

    // @Input() isReset: boolean = false;
    @Input() time: any = moment()
    toDate: any
    fromDate: any


    maxDate = moment();
    selected: any;
    ranges: any = {
        'Today': [moment(), moment()],
        // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        // 'This Week': [moment().startOf('week').add(1, 'days'), moment()],
        // 'Last Week': [moment().subtract(1, 'week').startOf('week').add(1, 'days'), moment().subtract(1, 'week').endOf('week').add(1, 'days')],
        // 'This Month': [moment().startOf('month'), moment().endOf('month')],
        // 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        // 'This Year': [moment().startOf('year'), moment()],
        // 'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
    }

    @Output() selectDate = new EventEmitter();

    constructor(
    ) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        // if(time)
        this.selected = this.time
    }

    ngOnInit(): void {
    }

    chooseTime(event: any) {
        console.log(event)
        console.log(this.selected)
        if (event.startDate != null && event.endDate != null) {
            this.fromDate = dayjs(event.startDate).format("YYYY-MM-DD")
            this.toDate = dayjs(event.endDate).format("YYYY-MM-DD")

            console.log(new Date(event.endDate))
            this.selectDate.emit({
                start: this.fromDate,
                end: this.toDate
            });
        }
    }

    convertTime(time: any){
        if(time) {
            let arrTime = time.split(" ")
        let timeChange:any = new Date (`${arrTime[0]} ${arrTime[1]} ${arrTime[2]} ${arrTime[3]}`).toLocaleDateString('es-US')
        console.log(timeChange)
        return timeChange.split("/").reverse().join("-")
        }
        
      }
}
