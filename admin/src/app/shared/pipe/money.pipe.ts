import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'money'
})
export class MoneyPipe implements PipeTransform {

    transform(value: any): string {
        if (value == '0' || !value || value <= 0) return '0đ';
        else {
            value = parseFloat(value);
            let vnd = Intl.NumberFormat("vi-VN", {
                currency: "VND",
                useGrouping: true,
            });
            return vnd.format(value).replace(/\./g, '.') + 'đ';
        }

    }
}
