import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value:any): any {
    if(value) return moment(value).format("DD/MM/YYYY");
    return null
  }

}
