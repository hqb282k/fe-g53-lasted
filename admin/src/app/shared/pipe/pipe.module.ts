import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoneyPipe } from './money.pipe';
import { TimePipe } from './time.pipe';



@NgModule({
  declarations: [
    MoneyPipe,
    TimePipe
  ],
  exports: [
    MoneyPipe,
    TimePipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipeModule { }
