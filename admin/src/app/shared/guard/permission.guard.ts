import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { ProfileService } from 'src/app/services/profile.service';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";



@Injectable({
    providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

    id: number;
    constructor(
        private router: Router,
        private profileService: ProfileService,
        private http: HttpClient,

    ) {
        this.id = Number(localStorage.getItem('user_id'));
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
        return true;
        
        if (route.data && route.data.role) 
        {
            let listRoles: any = route.data.role;
            let url = environment.apiHost + '/admin/getAccount/' + this.id;

            const token = localStorage.getItem('access_token');
            const headers = new HttpHeaders({
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
            });

            return this.http.get(url, { headers: headers })
                .pipe(map((res: any) => {
                    if (res.code == 200) {
                        let roleUser = res.objects.role.roleName;
                        if(listRoles.includes(roleUser)) 
                        return true;
                    }
                    if (res.code == 403) return this.router.navigate(['/error/403']);
                    return false;
                }), catchError((error: any) => {
                    if (error.code == 401) {
                        this.router.navigate(['auth/login']);
                    }
                    return of(false);
                }));
        } else {
            return true;
        }

    }
}