// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    appVersion: 'v8.0.38',
    apiHost: 'http://127.0.0.1:3333/api',
    USERDATA_KEY: 'auth',
    isMockEnabled: true,
    apiUrl: 'api',
    appThemeName: 'Admin Dimuadi',
  };