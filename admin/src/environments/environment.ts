// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    appVersion: 'v8.0.38',
    apiHost: 'http://localhost:8899/api',
    // apiHost: 'https://g53-api.gogitek.online/api',
    apiImg: 'https://apis.dev.dimuadi.vn/d2c-service',
    USERDATA_KEY: 'auth',
    isMockEnabled: true,
    apiUrl: 'api',
    appThemeName: 'G53 Admin',
};
