export default {
  locations: ["Ha Noi", "Hai Phong", "Quang Ninh"],
  type: ["Phong Don", "Phong Doi", "Phong Tinh Yeu"],
  rooms: [
    {
      id: 202,
      roomName: "Lake View",
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe:
        "Với lịch sử nghìn năm văn hiến, bất kỳ ai du lịch Hà Nội sẽ bị thu hút với vẻ đẹp xưa cũ của 36 phố phường, chùa Một Cột, hay hồ Hoàn Kiếm… Tất nhiên Hà Nội cũng vô cùng hiện đại, sôi động với vai trò thành phố thủ đô. Nhịp sống trẻ của thành phố này dễ dàng được cảm nhận qua các điểm vui chơi, giao lưu nghệ thuật mới của giới trẻ. Và nếu đã du lịch Hà Nội thì…",
      type: "Phong tong thong",
      price: 300,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      images: [
        "https://s3.r29static.com/bin/entry/79d/0,47,460,460/1200x1200,80/1387291/image.jpg",
        "https://images.unsplash.com/photo-1615874959474-d609969a20ed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmVkJTIwcm9vbXxlbnwwfHwwfHw%3D&w=1000&q=80",
        "https://media.cntraveler.com/photos/5a69f248794e860b9527ebb8/1:1/w_1522,h_1522,c_limit/1713_POD_TWIN_026.jpg",
      ],
      rate: 5,
    },
    {
      id: 203,
      roomName: "Lake View",
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe:
        "Với lịch sử nghìn năm văn hiến, bất kỳ ai du lịch Hà Nội sẽ bị thu hút với vẻ đẹp xưa cũ của 36 phố phường, chùa Một Cột, hay hồ Hoàn Kiếm… Tất nhiên Hà Nội cũng vô cùng hiện đại, sôi động với vai trò thành phố thủ đô. Nhịp sống trẻ của thành phố này dễ dàng được cảm nhận qua các điểm vui chơi, giao lưu nghệ thuật mới của giới trẻ. Và nếu đã du lịch Hà Nội thì…",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      images: [
        "https://s3.r29static.com/bin/entry/79d/0,47,460,460/1200x1200,80/1387291/image.jpg",
        "https://images.unsplash.com/photo-1615874959474-d609969a20ed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmVkJTIwcm9vbXxlbnwwfHwwfHw%3D&w=1000&q=80",
        "https://media.cntraveler.com/photos/5a69f248794e860b9527ebb8/1:1/w_1522,h_1522,c_limit/1713_POD_TWIN_026.jpg",
      ],
      rate: 5,
    },
    {
      id: 204,
      roomName: "Lake View",
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe:
        "Với lịch sử nghìn năm văn hiến, bất kỳ ai du lịch Hà Nội sẽ bị thu hút với vẻ đẹp xưa cũ của 36 phố phường, chùa Một Cột, hay hồ Hoàn Kiếm… Tất nhiên Hà Nội cũng vô cùng hiện đại, sôi động với vai trò thành phố thủ đô. Nhịp sống trẻ của thành phố này dễ dàng được cảm nhận qua các điểm vui chơi, giao lưu nghệ thuật mới của giới trẻ. Và nếu đã du lịch Hà Nội thì…",
      type: "Phong tong thong",
      price: 350,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      images: [
        "https://s3.r29static.com/bin/entry/79d/0,47,460,460/1200x1200,80/1387291/image.jpg",
        "https://images.unsplash.com/photo-1615874959474-d609969a20ed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmVkJTIwcm9vbXxlbnwwfHwwfHw%3D&w=1000&q=80",
        "https://media.cntraveler.com/photos/5a69f248794e860b9527ebb8/1:1/w_1522,h_1522,c_limit/1713_POD_TWIN_026.jpg",
      ],
      rate: 5,
    },
    {
      id: 205,
      roomName: "Lake View",
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe:
        "Với lịch sử nghìn năm văn hiến, bất kỳ ai du lịch Hà Nội sẽ bị thu hút với vẻ đẹp xưa cũ của 36 phố phường, chùa Một Cột, hay hồ Hoàn Kiếm… Tất nhiên Hà Nội cũng vô cùng hiện đại, sôi động với vai trò thành phố thủ đô. Nhịp sống trẻ của thành phố này dễ dàng được cảm nhận qua các điểm vui chơi, giao lưu nghệ thuật mới của giới trẻ. Và nếu đã du lịch Hà Nội thì…",
      type: "Phong tong thong",
      price: 100,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      images: [
        "https://s3.r29static.com/bin/entry/79d/0,47,460,460/1200x1200,80/1387291/image.jpg",
        "https://images.unsplash.com/photo-1615874959474-d609969a20ed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmVkJTIwcm9vbXxlbnwwfHwwfHw%3D&w=1000&q=80",
        "https://media.cntraveler.com/photos/5a69f248794e860b9527ebb8/1:1/w_1522,h_1522,c_limit/1713_POD_TWIN_026.jpg",
      ],
      rate: 5,
    },
    {
      id: 206,
      roomName: "Lake View",
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe:
        "Với lịch sử nghìn năm văn hiến, bất kỳ ai du lịch Hà Nội sẽ bị thu hút với vẻ đẹp xưa cũ của 36 phố phường, chùa Một Cột, hay hồ Hoàn Kiếm… Tất nhiên Hà Nội cũng vô cùng hiện đại, sôi động với vai trò thành phố thủ đô. Nhịp sống trẻ của thành phố này dễ dàng được cảm nhận qua các điểm vui chơi, giao lưu nghệ thuật mới của giới trẻ. Và nếu đã du lịch Hà Nội thì…",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      images: [
        "https://s3.r29static.com/bin/entry/79d/0,47,460,460/1200x1200,80/1387291/image.jpg",
        "https://images.unsplash.com/photo-1615874959474-d609969a20ed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmVkJTIwcm9vbXxlbnwwfHwwfHw%3D&w=1000&q=80",
        "https://media.cntraveler.com/photos/5a69f248794e860b9527ebb8/1:1/w_1522,h_1522,c_limit/1713_POD_TWIN_026.jpg",
      ],
      rate: 5,
    },
    {
      id: 207,
      roomName: "Lake View",
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe:
        "Với lịch sử nghìn năm văn hiến, bất kỳ ai du lịch Hà Nội sẽ bị thu hút với vẻ đẹp xưa cũ của 36 phố phường, chùa Một Cột, hay hồ Hoàn Kiếm… Tất nhiên Hà Nội cũng vô cùng hiện đại, sôi động với vai trò thành phố thủ đô. Nhịp sống trẻ của thành phố này dễ dàng được cảm nhận qua các điểm vui chơi, giao lưu nghệ thuật mới của giới trẻ. Và nếu đã du lịch Hà Nội thì…",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      images: [
        "https://s3.r29static.com/bin/entry/79d/0,47,460,460/1200x1200,80/1387291/image.jpg",
        "https://images.unsplash.com/photo-1615874959474-d609969a20ed?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmVkJTIwcm9vbXxlbnwwfHwwfHw%3D&w=1000&q=80",
        "https://media.cntraveler.com/photos/5a69f248794e860b9527ebb8/1:1/w_1522,h_1522,c_limit/1713_POD_TWIN_026.jpg",
      ],
      rate: 5,
    },
  ],
  sale: [
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],

        report: false,
      },
      rate: 5,
    },
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],

        report: false,
      },
      rate: 2,
    },
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],

        report: false,
      },
      rate: 5,
    },
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],

        report: false,
      },
      rate: 5,
    },
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      rate: 5,
    },
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],
        report: false,
      },
      rate: 5,
    },
    {
      id: 202,
      avatar:
        "https://pix8.agoda.net/hotelImages/967/967386/967386_15100215430036608874.jpg?ca=5&ce=1&s=1024x768",
      describe: "phong tong thong",
      type: "Phong tong thong",
      price: 200,
      services: ["Thang máy", "View biển", "Bữa sáng"],
      status: {
        isBusy: [
          {
            from: "05/10/2022 04:00 pm",
            to: "06/10/2022 05:00 am",
          },
          {
            from: "06/10/2022 10:00 am",
            to: "06/10/2022 05:00 pm",
          },
        ],

        report: false,
      },
    },
  ],
  offers: [
    {
      valid: "Jan 16, 2022",
      avatar:
        "https://cdn.tgdd.vn/Files/2021/03/09/1333700/cac-loai-banh-ngot-duoc-yeu-thich-nhat-tai-viet-nam-202103090937443232.jpg",
      title: "Tiệc chiều với bánh ngọt",
      filter: {
        type: "romantic",
        service: "",
      },
    },
    {
      valid: "Jan 16, 2022",
      avatar:
        "https://dep365.com/wp-content/uploads/2018/09/ai-cung-phai-dong-y-rang-phong-tam-la-noi-thu-gian-tuyet-nhat-qua-dat-01.jpg",
      title: "Thư giãn trong bồn tắm với tinh dầu",
      filter: {
        type: "romantic",
        service: "",
      },
    },
    {
      valid: "Jan 16, 2022",
      avatar:
        "https://luhanhvietnam.com.vn/du-lich/vnt_upload/File/Image/dia_diem_ngam_hoang_hon_dep_o_ha_noi_ho_tay_dep.jpg",
      title: "Hoàng hôn cùng với view tuyệt đỉnh",
      filter: {
        type: "romantic",
        service: "",
      },
    },
    {
      valid: "Jan 16, 2022",
      avatar:
        "https://i.pinimg.com/736x/ff/3f/c6/ff3fc653ef88da07d8a8621c36b7c69f--tasting-room-wine-tasting.jpg",
      title:
        "Thưởng thức những loại rượu vang ngon nhất được nhập khẩu ngay tại G53",
      filter: {
        type: "romantic",
        service: "",
      },
    },
    {
      valid: "Jan 16, 2022",
      avatar:
        "https://media.istockphoto.com/id/917725816/photo/real-love.jpg?s=612x612&w=0&k=20&c=Aw5ktMfSUxXvn9TkFTxsq90e6j6a4IQOBR7AjnYJkNA=",
      title: "Bùng nổ tình yêu của bạn với người đó cùng với G53",
      filter: {
        type: "romantic",
        service: "",
      },
    },
  ],
  sponsors: [
    {
      name: "Travel loka",
      avatar: "https://themehut.co/html/geair/assets/img/brand/brand_img03.png",
    },
    {
      name: "Travel loka",
      avatar: "https://themehut.co/html/geair/assets/img/brand/brand_img01.png",
    },
    {
      name: "Travel loka",
      avatar: "https://themehut.co/html/geair/assets/img/brand/brand_img04.png",
    },
    {
      name: "Travel loka",
      avatar: "https://themehut.co/html/geair/assets/img/brand/brand_img05.png",
    },
  ],
  places: [
    {
      avatar:
        "https://salt.tikicdn.com/cache/w1200/ts/product/51/b7/98/8c3fcf4f61d86edacd183379e74a6a3a.jpg",
      title: "Hợp tác cùng Sungroup",
      description: "20/12/2022",
      body: "Nội dung về tính năng<br />iPad Air 5 M1 hoàn toàn mới. Linh hoạt hơn bao giờ hết. Màn hình Liquid Retina 10.9 inch tuyệt đẹp với dải màu rộng cho bạn trải nghiệm thị giác vô cùng sống động và chi tiết khi xem ảnh hay video cũng như khi chơi game trên thiết bị (1). Chip Apple M1 nổi bật với hiệu năng vận hành mạnh mẽ và các tính năng máy học cao cấp, hỗ trợ tối đa tác vụ biên tập video 4K, soạn bài thuyết trình đẹp mắt hay thiết kế mô hình 3D - mọi tác vụ đều thật dễ dàng. Thiết bị giờ đã hỗ trợ Magic Keyboard và Apple Pencil (thế hệ thứ 2) (2), Touch ID nhanh nhạy, dễ sử dụng, bảo mật cao, camera sau 12MP góc siêu rộng hỗ trợ Center Stage và camera FaceTime HD 12MP góc rộng, cổng kết nối USB-C hỗ trợ sạc và phụ kiện, thời lượng pin bền bỉ cả ngày (3), công nghệ Wi-Fi 6. Với iPad Air 5 trong tay, bạn đã có đủ sức mạnh để hiện thực hóa mọi ý tưởng của bản thân.<br /><br />Tính năng nổi bật<br />Màn hình Liquid Retina 10.9 inch tuyệt đẹp với True Tone và dải màu rộng P3 (1)<br />Chip Apple M1<br />Xác thực bảo mật với Touch ID<br />Camera trước: 12MP góc rộng, Camera sau: 12MP góc siêu rộng hỗ trợ Center Stage<br />Hiện có các màu xám, trắng, hồng, tím, xanh<br />Âm thanh stereo rộng<br />Thời lượng pin lên đến 10 giờ (3)<br />Wi-Fi 6 (802.11ax)<br />Cổng kết nối USB-C để sạc và kết nối phụ kiện<br />Hỗ trợ Magic Keyboard, Smart Keyboard Folio và Apple Pencil (thế hệ thứ 2) (2)<br />iPadOS 15 mang đến cho bạn các chức năng mới được thiết kế dành riêng cho iPad<br />Pháp lý<br />Ứng dụng có sẵn trên App Store. Nội dung được cung cấp có thể thay đổi.<br /><br />(1) Màn hình có các góc bo tròn. Khi tính theo hình chữ nhật, kích thước màn hình iPad Air (thế hệ thứ 4) là 10.86 inch theo đường chéo. Diện tích hiển thị thực tế nhỏ hơn.<br /><br />(2) Phụ kiện được bán riêng. Khả năng tương thích tùy thuộc thế hệ sản phẩm.<br /><br />(3) Thời lượng pin khác nhau tùy theo cách sử dụng và cấu hình. Truy cập để biết thêm thông tin.<br /><br />(4) Cần có gói cước dữ liệu. Liên hệ với nhà mạng tại thị trường của bạn để biết thêm chi tiết. Tốc độ có thể thay đổi tùy địa điểm.<br /><br />Giá sản phẩm trên Tiki đã bao gồm thuế theo luật hiện hành. Bên cạnh đó, tuỳ vào loại sản phẩm, hình thức và địa chỉ giao hàng mà có thể phát sinh thêm chi phí khác như phí vận chuyển, phụ phí hàng cồng kềnh, thuế nhập khẩu (đối với đơn hàng giao từ nước ngoài có giá trị trên 1 triệu đồng)....",
      bestCity: "G53 Hà Nội",
    },
    {
      avatar:
        "https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/08/pho-co-hoi-an-e1504760193217.jpg",
      title: "G53 chuẩn bị có mặt tại Hội An",
      bestCity: "G53 Hà Nội",
    },
    {
      avatar:
        "https://vietnam.travel/sites/default/files/inline-images/shutterstock_1506184586_resize.jpg",
      title: "G53 có thêm các loại phòng mới",
      bestCity: "G53 Hà Nội",
    },
    {
      avatar:
        "https://vietnam.travel/sites/default/files/inline-images/shutterstock_1506184586_resize.jpg",
      title: "Quảng Nam",
      bestCity: "Hội An",
    },
    {
      avatar:
        "https://vietnam.travel/sites/default/files/inline-images/shutterstock_1506184586_resize.jpg",
      title: "Quảng Nam",
      bestCity: "Hội An",
    },
    {
      avatar:
        "https://vietnam.travel/sites/default/files/inline-images/shutterstock_1506184586_resize.jpg",
      title: "Quảng Nam",
      bestCity: "Hội An",
    },
  ],
  reviews: [
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 4,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn,Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn,Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 3,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 4,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
    {
      date: "14/12/2022",
      name: "Ho Quoc Bao",
      rate: 5,
      content:
        "Nhân viên cực kì thân thiện ân cần, chu đáo và check in nhanh gọn",
    },
  ],
  booked: [
    {
      id: "333333",
      roomType: "Phong Don",
      date: "21/07/2022",
      total: "300000",
    },
    {
      id: "333333",
      roomType: "Phong Don",
      date: "21/07/2022",
      total: "300000",
    },
    {
      id: "333333",
      roomType: "Phong Don",
      date: "21/07/2022",
      total: "300000",
    },
    {
      id: "333333",
      roomType: "Phong Don",
      date: "21/07/2022",
      total: "300000",
    },
    {
      id: "333333",
      roomType: "Phong Don",
      date: "21/07/2022",
      total: "300000",
    },
    {
      id: "333333",
      roomType: "Phong Don",
      date: "21/07/2022",
      total: "300000",
    },
  ],
};
