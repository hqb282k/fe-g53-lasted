import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setStatusModal } from "Redux/Slices/modal";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 24,
};

const BasicModal = ({ Element }) => {
  const show = useSelector((state) => state.modal.showing);
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(
      setStatusModal({
        isShowing: false,
      })
    );
  };

  return (
    <Modal
      open={show}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Element />
      </Box>
    </Modal>
  );
};

export default BasicModal;
