import React from "react";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import { Swiper, SwiperSlide } from "swiper/react";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import { Autoplay, EffectFade, Pagination, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/effect-fade";
import { Navigate, useNavigate } from "react-router-dom";
const Banner = ({ banners, spaceBetween, slidesPerView }) => {
  const navigate = useNavigate();
  return (
    <div>
      <Swiper
        autoplay={{
          delay: 2000,
          disableOnInteraction: false,
        }}
        effect={"fade"}
        spaceBetween={spaceBetween}
        slidesPerView={slidesPerView}
        modules={[Autoplay, EffectFade]}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {banners.map((banner, index) => (
          <SwiperSlide key={index}>
            <Box
              sx={{
                backgroundImage: `url(${banner})`,
                backgroundRepeat: "no-repeat",
                backgroundAttachment: "fixed",
                backgroundSize: "cover",
                color: "white",
                height: "100vh",
                backgroundPosition: "center",
                position: "relative",
              }}
            >
              <Box
                sx={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  backgroundColor: "black",
                  opacity: "0.65",
                }}
              ></Box>
              <Container maxwidth={"xl"}>
                <Box
                  sx={{
                    position: "absolute",
                    top: "35%",
                  }}
                >
                  {localStorage.getItem("token") == null ? (
                    <>
                      <Typography
                        variant="h3"
                        sx={{
                          py: 2,
                          fontWeight: "600",
                          color: "white",
                        }}
                      >
                        Chào mừng đến với khách sạn G53
                      </Typography>
                      <Typography
                        variant="h5"
                        sx={{
                          py: 2,
                          fontWeight: "400",
                          color: "white",
                        }}
                      >
                        Hãy đăng ký ngay để đặt phòng với ưu đãi tốt nhất
                      </Typography>
                      <Button
                        variant="contained"
                        sx={{
                          backgroundColor: "#FFA903",
                          color: "black",
                          ":hover": {
                            backgroundColor: "white",
                            color: "#FFA903",
                          },
                        }}
                        onClick={() => {
                          navigate("/register");
                          window.scrollTo(0, 0);
                        }}
                      >
                        Đăng Ký
                      </Button>
                    </>
                  ) : (
                    <>
                      <Typography
                        variant="h3"
                        sx={{
                          p: 2,
                          fontWeight: "600",
                          color: "white",
                        }}
                      >
                        Chào mừng{" "}
                        <Typography
                          variant="h3"
                          sx={{ color: "#FFA903", fontWeight: "600" }}
                        >
                          {JSON.parse(localStorage.getItem('user')).firstName+" "+JSON.parse(localStorage.getItem('user')).lastName}
                        </Typography>
                        đến với G53
                      </Typography>
                    </>
                  )}
                </Box>
              </Container>
            </Box>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default Banner;
