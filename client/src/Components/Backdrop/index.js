
import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from "react-redux";

export default function SimpleBackdrop() {
 const isOpen = useSelector((e) => e.backdrop.isOpen);
  return (
    <div>
      <Backdrop
        sx={{ color: '#57112F', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isOpen}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}