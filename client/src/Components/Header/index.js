import React, { useState } from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import AccountCircle from "@mui/icons-material/AccountCircle";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Container from "@mui/material/Container";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import images from "Assets";
import Link from "@mui/material/Link";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import { useNavigate } from "react-router-dom";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { setStatusModal } from "Redux/Slices/modal";
import { setStatusSnackbar } from "Redux/Slices/snackbar";
import style from "styled-components";
import { useFormik } from "formik";
import * as yup from "yup";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css"; // optional for styling
import { hideAll } from "tippy.js";
import axios from "axios";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import { Typography } from "@mui/material";
import LoginIcon from "@mui/icons-material/Login";
import TableRowsIcon from "@mui/icons-material/TableRows";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import StarBorder from "@mui/icons-material/StarBorder";
import Collapse from "@mui/material/Collapse";
import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import SendIcon from "@mui/icons-material/Send";
import Divider from "@mui/material/Divider";
import { alpha, styled } from "@mui/material/styles";
import { Drawer } from "Components";
import { setStatusDrawer } from "Redux/Slices/drawer";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import withReactContent from "sweetalert2-react-content";

import Swal from "sweetalert2";
import tippy from "tippy.js";
// React responsive library
import { useMediaQuery } from "react-responsive";

const checkStatusCode = (code) => {
  switch (code) {
    case 119:
      return "Xác thực không thành công";
      break;
    case 100:
      return "Email không tồn tại";
      break;
    case 200:
      return "Đã gửi yêu cầu";
      break;
    default:
      return "Lỗi không xác định";
  }
};

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#FFA903",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#ffa903",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#ffa903",
    },
    "&:hover fieldset": {
      borderColor: "#ffa903",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#ffa903",
    },
  },
});

const TippyStyle = style.div`
  .tippy-box {
    background-color: #1f252e;
    padding: 20px 0;
    max-width: 250px !important;
    border: 1px solid #faa903;
  }
  .tippy-box[data-placement^="bottom"] > .tippy-arrow:before {
    border-bottom-color: #ffa903;
  }
`;

const FormStyle = style.div`
input {
  color: #FFA903 !important;
}
label{
  color: #FFA903 !important;
}
#custom-css-outlined-input-helper-text{
color:#ff5703;
}
`;

const DropDownMenuStyle = style.div`
  .dropdown_button {
    color: white;
    border: none;
    padding: 20px 0;
    &:hover .arrow_icon{
      transform: rotate(90deg);
    }
  }

  .dropdown {
    position: relative;
    display: inline-block;
    cursor: pointer;
  }

  .dropdown_option {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 140px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
  }

  .dropdown_option a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }

  .dropdown_option a:hover {
    background-color: #ddd;
  }

  .dropdown:hover .dropdown_option {
    display: block;
  }

`;

const SlidingMenu = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [openNestedItem, setOpenNestedItem] = React.useState({
    account: false,
    pages: false,
  });

  const handleClickAccount = () => {
    setOpenNestedItem({ ...openNestedItem, account: !openNestedItem.account });
  };

  const handleClickPages = () => {
    setOpenNestedItem({ ...openNestedItem, pages: !openNestedItem.pages });
  };

  return (
    <Drawer
      anchor={"right"}
      Element={() => (
        <List
          sx={{
            width: "260px",
            backgroundColor: "#1F252E",
            color: "#FFA903",
            height: "100%",
          }}
          component="nav"
          aria-labelledby="nested-list-subheader"
        >
          {/* Hide when not login in mobile responsive */}
          {localStorage.getItem("token") != null ? (
            <>
              <ListItemButton onClick={handleClickAccount}>
                <AccountCircleIcon sx={{ px: 2 }} />
                <ListItemText
                  primary={`Xin Chào ${
                    JSON.parse(localStorage.getItem("user")).lastName
                  }`}
                />
                {openNestedItem.account ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
              <Collapse
                in={openNestedItem.account}
                timeout="auto"
                unmountOnExit
              >
                <List component="div" disablePadding>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    onClick={() => {
                      navigate("/user");
                      setOpenNestedItem({
                        ...openNestedItem,
                        pages: false,
                        account: false,
                      });
                      dispatch(setStatusDrawer({ isOpen: false }));
                    }}
                  >
                    <ListItemText primary="Thông tin tài khoản" />
                  </ListItemButton>
                  <ListItemButton
                    sx={{ pl: 4 }}
                    onClick={() => {
                      localStorage.clear();
                      navigate("/home");
                    }}
                  >
                    <ListItemText primary="Đăng xuất" />
                  </ListItemButton>
                </List>
              </Collapse>
              <Box sx={{ height: "0.1px", backgroundColor: "#FFA903" }} />
            </>
          ) : (
            <>
              <ListItemButton onClick={handleClickAccount}>
                <AccountCircleIcon sx={{ px: 2 }} />
                <ListItemText primary="Đăng Nhập" />
                {openNestedItem.account ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
              <Collapse
                in={openNestedItem.account}
                timeout="auto"
                unmountOnExit
              >
                <Box sx={{ px: 2, pb: 3 }}>
                  <LoginForm></LoginForm>
                </Box>
              </Collapse>
              <Box sx={{ height: "0.1px", backgroundColor: "#FFA903" }} />
            </>
          )}

          <ListItemButton
            onClick={() => {
              navigate("/home");
              setOpenNestedItem({
                ...openNestedItem,
                pages: false,
                account: false,
              });
              dispatch(setStatusDrawer({ isOpen: false }));
            }}
          >
            <ListItemText primary="Trang chủ" />
          </ListItemButton>
          <ListItemButton
            onClick={() => {
              navigate("/about-us");
              setOpenNestedItem({
                ...openNestedItem,
                pages: false,
                account: false,
              });
              dispatch(setStatusDrawer({ isOpen: false }));
            }}
          >
            <ListItemText primary="Về chúng tôi" />
          </ListItemButton>
          <ListItemButton onClick={handleClickPages}>
            <ListItemText primary="Trang" />
            {openNestedItem.pages ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
          <Collapse in={openNestedItem.pages} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItemButton
                sx={{ pl: 4 }}
                onClick={() => {
                  navigate("/rooms");
                  setOpenNestedItem({
                    ...openNestedItem,
                    pages: false,
                    account: false,
                  });
                  dispatch(setStatusDrawer({ isOpen: false }));
                }}
              >
                <ListItemText primary="Đặt Phòng" />
              </ListItemButton>
            </List>
          </Collapse>
        </List>
      )}
    />
  );
};

const ForgotPasswordForm = ({ setLoginMode }) => {
  const MySwal = withReactContent(Swal);
  const dispatch = useDispatch();
  const validationSchema = yup.object({
    email: yup
      .string("Enter your email")
      .email("Nhập đúng định dạng Email")
      .required("Không được bỏ trống"),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      dispatch(
        setStatusBackdrop({
          isOpen: true,
        })
      );
      const body = {
        objectRequest: {
          email: values.email,
        },
      };
      axios
        .post(
          process.env.REACT_APP_BASE_URL +
            // "http://localhost:" +
            //   process.env.REACT_APP_BASE_PORT +
            process.env.REACT_APP_USER_SEND_EMAIL_RESET_PASSWORD_API_PATH,
          body
        )
        .then((response) => {
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          if (response.data.code == 200) {
            Swal.fire({
              icon: "success",
              title: `${checkStatusCode(response.data.code)}`,
              text: "Chúng tôi đã gửi đường dẫn đặt lại mật khẩu tới email của bạn !",
            });
          } else {
            Swal.fire({
              icon: "warning",
              title: `${checkStatusCode(response.data.code)}`,
              text: "",
            });
          }
        })
        .catch((error) => {
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          Swal.fire({
            icon: "warning",
            title: `${checkStatusCode(error)}`,
            text: "Hãy thông báo với chúng tôi qua số: 0971769799",
          });
        });
      hideAll();
      setLoginMode(true);
      dispatch(setStatusDrawer({ isOpen: false }));
    },
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <Typography
        sx={{
          textAlign: "center",
          color: "#FFA903",
        }}
      >
        Nhập email của bạn:
      </Typography>
      <CustomTextField
        id="custom-css-outlined-input"
        fullWidth
        name="email"
        label="Email"
        type="text"
        value={formik.values.email}
        onChange={formik.handleChange}
        error={formik.touched.email && Boolean(formik.errors.email)}
        helperText={formik.touched.email && formik.errors.email}
        sx={{ my: 1, input: { color: "white" } }}
      />
      <Button
        color="warning"
        sx={{
          backgroundColor: "#FFA903",
        }}
        variant="contained"
        fullWidth
        type="submit"
      >
        <LoginIcon></LoginIcon>
      </Button>
      <Typography
        sx={{
          textAlign: "center",
          color: "#FFA903",
          opacity: "0.8",
          pt: 2,
          cursor: "pointer",
        }}
        onClick={() => {
          setLoginMode(true);
        }}
      >
        Quay lại đăng nhập
      </Typography>
    </form>
  );
};

const LoginForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const [loginMode, setLoginMode] = React.useState(true);
  const validationSchema = yup.object({
    user: yup
      .string("Nhập tên tài khoản của bạn")
      .required("Không được bỏ trống"),
    password: yup
      .string("Enter your password")
      .min(6, "Mật khẩu không đúng định dạng")
      .required("Không được bỏ trống"),
  });
  const formik = useFormik({
    initialValues: {
      user: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      const body = {
        objectRequest: {
          username: values.user,
          password: values.password,
        },
      };
      dispatch(
        setStatusBackdrop({
          isOpen: true,
        })
      );
      axios
        .post(
          // "http://localhost:" +
          //   process.env.REACT_APP_BASE_PORT +
          process.env.REACT_APP_BASE_URL +
            process.env.REACT_APP_USER_LOGIN_API_PATH,
          body
        )
        .then((response) => {
          // localStorage.setItem("username", values.user);
          localStorage.setItem("token", response.data.objects.access_token);
          localStorage.setItem(
            "user",
            JSON.stringify(response.data.objects.data)
          );
          console.log(response.data);
          dispatch(
            setStatusSnackbar({
              isShowing: true,
              title: "Đăng nhập thành công",
              severity: "success",
            })
          );
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          navigate(location.pathname);
        })
        .catch((error) => {
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          dispatch(
            setStatusSnackbar({
              isShowing: true,
              title: "Sai thông tin tài khoản hoặc mật khẩu",
              severity: "error",
            })
          );
        });
    },
  });

  return (
    <FormStyle>
      {loginMode ? (
        <div>
          <form onSubmit={formik.handleSubmit}>
            {loginMode ? (
              <div>
                <CustomTextField
                  id="custom-css-outlined-input"
                  fullWidth
                  name="user"
                  label="Tên Tài Khoản"
                  value={formik.values.user}
                  onChange={formik.handleChange}
                  error={formik.touched.user && Boolean(formik.errors.user)}
                  helperText={formik.touched.user && formik.errors.user}
                  placeholder={"Tên tài khoản"}
                  sx={{ my: 1, input: { color: "white" } }}
                />
                <CustomTextField
                  id="custom-css-outlined-input"
                  fullWidth
                  name="password"
                  label="Mật Khẩu"
                  type="password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                  sx={{ my: 1, input: { color: "white" } }}
                />
                <Button
                  color="warning"
                  sx={{
                    backgroundColor: "#FFA903",
                  }}
                  variant="contained"
                  fullWidth
                  type="submit"
                >
                  <LoginIcon></LoginIcon>
                </Button>
                <Typography
                  sx={{
                    textAlign: "center",
                    color: "#FFA903",
                    opacity: "0.7",
                    pt: 2,
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setLoginMode(false);
                  }}
                >
                  Quên mật khẩu
                </Typography>
              </div>
            ) : (
              <div>
                <Typography
                  sx={{
                    textAlign: "center",
                    color: "#FFA903",
                  }}
                >
                  Nhập email của bạn:
                </Typography>
                <CustomTextField
                  id="custom-css-outlined-input"
                  fullWidth
                  name="email"
                  label="Email"
                  type="text"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  helperText={formik.touched.email && formik.errors.email}
                  sx={{ my: 1, input: { color: "white" } }}
                />
                <Button
                  color="warning"
                  sx={{
                    backgroundColor: "#FFA903",
                  }}
                  variant="contained"
                  fullWidth
                  type="submit"
                >
                  <LoginIcon></LoginIcon>
                </Button>
              </div>
            )}
          </form>
        </div>
      ) : (
        <ForgotPasswordForm setLoginMode={setLoginMode}></ForgotPasswordForm>
      )}
    </FormStyle>
  );
};

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const isSmallScreen = useMediaQuery({ query: "(max-width: 900px)" });
  return (
    <Box
      sx={{
        position: "fixed",
        width: "100%",
        zIndex: "999",
        py: 2,
        backgroundColor: "#1f252e",
      }}
    >
      <Container maxwidth="xl">
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box
            component="img"
            sx={{
              maxWidth: "70px",
              borderRadius: "50%",
              cursor: "pointer",
            }}
            alt="The house from the offer."
            src={images.logo}
            onClick={() => {
              navigate("/home");
            }}
          />
          <Box sx={{ flexGrow: 1 }}></Box>
          {isSmallScreen && <SlidingMenu></SlidingMenu>}
          {!isSmallScreen && (
            <Box
              sx={{
                display: "flex",
                gap: 4,
                justifyContent: "end",
                flexGrow: 1,
                mx: "30px",
              }}
            >
              <Link
                href="/home"
                underline="none"
                color="#FFA903"
                sx={{
                  ":hover .test": {
                    backgroundColor: "#FFA903",
                  },
                }}
              >
                Trang chủ
                <Box
                  className="test"
                  sx={{
                    width: "100%",
                    height: "3px",
                    mt: "2px",
                    transition: "background-color 1s",
                    borderRadius: "1px",
                  }}
                ></Box>
              </Link>
              <Link
                href="/about-us"
                underline="none"
                color="#FFA903"
                sx={{
                  ":hover .test": {
                    backgroundColor: "#FFA903",
                  },
                }}
              >
                Về Chúng Tôi
                <Box
                  className="test"
                  sx={{
                    width: "100%",
                    height: "3px",
                    mt: "2px",
                    transition: "background-color 1s",
                    borderRadius: "1px",
                  }}
                ></Box>
              </Link>
              <TippyStyle>
                {" "}
                <Tippy
                  content={
                    <Box
                      sx={{ display: "flex", flexDirection: "column", gap: 2 }}
                    >
                      <Typography
                        sx={{
                          backgroundColor: "#FFA903",
                          color: "black",
                          p: 1,
                          borderRadius: "5px",
                          fontWeight: "600",
                          cursor: "pointer",
                          ":hover": {
                            backgroundColor: "#f5f3f0",
                            color: "#FFA903",
                          },
                        }}
                        variant="text"
                        onClick={() => {
                          hideAll();
                          navigate("/rooms");
                          window.scrollTo(0, 0);
                        }}
                      >
                        Đặt Phòng
                      </Typography>
                      <Typography
                        sx={{
                          backgroundColor: "#FFA903",
                          color: "black",
                          p: 1,
                          borderRadius: "5px",
                          fontWeight: "600",
                          cursor: "pointer",
                          ":hover": {
                            backgroundColor: "white",
                            color: "#FFA903",
                          },
                        }}
                        variant="text"
                        onClick={() => {
                          hideAll();
                          navigate("/contact");
                        }}
                      >
                        Liên Hệ
                      </Typography>
                    </Box>
                  }
                  animation="fade"
                  arrow={false}
                  theme="tomato"
                  trigger="click"
                  interactive="true"
                  placement="bottom-end"
                  appendTo="parent"
                >
                  <Box sx={{ display: "flex", alignItems: "center" }}>
                    <Typography
                      variant="text"
                      sx={{
                        color: "#FFA903",
                        cursor: "pointer",
                      }}
                    >
                      Trang
                    </Typography>
                  </Box>
                </Tippy>
              </TippyStyle>
            </Box>
          )}
          {/*  Button login show when have no token  */}
          <Box>
            {localStorage.getItem("token") == null && !isSmallScreen && (
              <Box>
                <Stack spacing={2} direction="row">
                  <TippyStyle>
                    <Tippy
                      content={<LoginForm></LoginForm>}
                      animation="fade"
                      arrow={true}
                      theme="tomato"
                      trigger="click"
                      interactive="true"
                      // placement={level === 1 ? "bottom" : "right-center"}
                      appendTo="parent"
                    >
                      <Button
                        variant="contained"
                        sx={{
                          backgroundColor: "#FAA903",
                          color: "black",
                          ":hover": {
                            backgroundColor: "white",
                            color: "#FFA903",
                          },
                        }}
                      >
                        Đăng Nhập
                      </Button>
                    </Tippy>
                  </TippyStyle>
                </Stack>
              </Box>
            )}
            {/*  User logo show when login success  */}
            {localStorage.getItem("token") != null && !isSmallScreen && (
              <TippyStyle>
                {" "}
                <Tippy
                  content={
                    <Box
                      sx={{ display: "flex", flexDirection: "column", gap: 2 }}
                    >
                      <Typography
                        sx={{
                          backgroundColor: "#FFA903",
                          color: "black",
                          p: 1,
                          borderRadius: "5px",
                          fontWeight: "600",
                          cursor: "pointer",
                          ":hover": {
                            backgroundColor: "white",
                            color: "#FFA903",
                          },
                        }}
                        variant="text"
                        onClick={() => {
                          hideAll();
                          navigate("/user");
                        }}
                      >
                        Thông tin tài khoản
                      </Typography>
                      <Typography
                        sx={{
                          backgroundColor: "#FFA903",
                          color: "black",
                          p: 1,
                          borderRadius: "5px",
                          fontWeight: "600",
                          cursor: "pointer",
                          ":hover": {
                            backgroundColor: "white",
                            color: "#FFA903",
                          },
                        }}
                        variant="text"
                        onClick={() => {
                          hideAll();
                          localStorage.clear();
                          if (location.pathname == "/user") {
                            navigate("/home");
                          } else {
                            navigate(location.pathname);
                          }
                        }}
                      >
                        Đăng xuất
                      </Typography>
                    </Box>
                  }
                  animation="fade"
                  arrow={false}
                  theme="tomato"
                  trigger="click"
                  interactive="true"
                  placement="bottom-end"
                  appendTo="parent"
                >
                  <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
                    <AccountCircleIcon
                      sx={{ color: "#FFA903" }}
                    ></AccountCircleIcon>
                    <Typography
                      variant="text"
                      sx={{
                        color: "#FFA903",
                        cursor: "pointer",
                        borderLeft: "1px dashed #FFA903",
                        px: 1,
                      }}
                    >
                      {JSON.parse(localStorage.getItem("user")).lastName}
                    </Typography>
                  </Box>
                </Tippy>
              </TippyStyle>
            )}
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default Header;
