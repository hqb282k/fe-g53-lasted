import React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import TableRowsIcon from "@mui/icons-material/TableRows";
import AccountCircle from "@mui/icons-material/AccountCircle";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Container from "@mui/material/Container";
import images from "Assets";
import { Typography } from "@mui/material";
import Link from "@mui/material/Link";
const SubHeader = ({ title }) => {
  return (
    <Box
      sx={{
        backgroundColor: "rgba(108, 122, 150, 0.4)",
      }}
    >
      <Box sx={{ padding: "100px", textAlign: "center" }}>
        <Typography variant="h3" sx={{ color: "white" }}>
          {title}
        </Typography>
        <Link href="/home" underline="none" color="white">
          Trang Chủ
        </Link>
        <Typography variant="text" sx={{ color: "#FFA903" }}>
          {` / ${title}`}
        </Typography>
      </Box>
    </Box>
  );
};

export default SubHeader;
