import React from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import FacebookIcon from "@mui/icons-material/Facebook";
import Container from "@mui/material/Container";
import images from "Assets";
import { useMediaQuery } from "react-responsive";

const Footer = () => {
  const isSmallAndMediumScreen = useMediaQuery({ query: "(max-width: 900px)" });

  return (
    <Box
      sx={{
        backgroundColor: "white",
        backgroundImage:
          "url('https://themehut.co/html/geair/assets/img/bg/footer_bg.jpg')",
        color: "white",
      }}
    >
      <Container
        maxwidth="xl"
        sx={{
          py: 7,
        }}
      >
        <Grid container spacing={2}>
          <Grid
            item
            xs={12}
            sx={{
              display: "flex",
              flexDirection: `${isSmallAndMediumScreen ? "column" : "row"}`,
              alignItems: "center",
              gap: 2,
            }}
          >
            <Box
              component="img"
              sx={{
                width: "200px",
                borderRadius: "50%",
              }}
              alt="The house from the offer."
              src={images.logo}
            />
            {/* <Typography
              variant="h4"
              sx={{
                color: "white",
                textAlign: `${isSmallAndMediumScreen ? "center" : "left"}`,
              }}
            >
              Chúng tôi làm những gì tốt nhất !
            </Typography> */}
          </Grid>
          <Grid
            item
            xs={4}
            sx={{ textAlign: `${isSmallAndMediumScreen ? "center" : "left"}` }}
          >
            <Typography variant="h6" sx={{ py: 2, color: "#FFA903" }}>
              TRỢ GIÚP
            </Typography>
            <Typography>Hướng dẫn của G53</Typography>
          </Grid>
          <Grid
            item
            xs={4}
            sx={{ textAlign: `${isSmallAndMediumScreen ? "center" : "left"}` }}
          >
            <Typography variant="h6" sx={{ py: 2, color: "#FFA903" }}>
              VỀ G53
            </Typography>
            <Typography>Thành viên</Typography>
          </Grid>
          <Grid
            item
            xs={4}
            sx={{ textAlign: `${isSmallAndMediumScreen ? "center" : "left"}` }}
          >
            <Typography variant="h6" sx={{ py: 2, color: "#FFA903" }}>
              THEO DÕI
            </Typography>
            <Box
              sx={{
                display: "flex",
                gap: 2,
                alignItems: "center",
                justifyContent: `${isSmallAndMediumScreen ? "center" : "left"}`,
              }}
            >
              <FacebookIcon></FacebookIcon>
              <Typography variant="text">Facebook</Typography>
            </Box>
          </Grid>
        </Grid>
      </Container>
      <Box
        sx={{
          textAlign: "center",
          backgroundColor: "#687481",
          py: 3,
          color: "white",
        }}
      >
        <Container maxwidth="xl">
          <Typography>
            CÔNG TY CỔ PHẦN THƯƠNG MẠI VÀ DỊCH VỤ G53 VIỆT NAM
          </Typography>
          <Typography>Đai học FPT Hà Nội</Typography>
          <Typography>Hotline:0971769799</Typography>
          <Typography> Email:G53VoDich@gmail.com</Typography>
          <Typography>
            {" "}
            Thông tin mã số doanh nghiệp: 0123456 do Phòng đăng ký kinh doanh -
            Sở Kế hoạch và Đầu tư thành phố Hà Nội cấp ngày 01/09/2020.
          </Typography>
        </Container>
      </Box>
    </Box>
  );
};

export default Footer;
