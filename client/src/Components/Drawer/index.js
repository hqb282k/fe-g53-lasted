import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import TableRowsIcon from "@mui/icons-material/TableRows";
import { useDispatch, useSelector } from "react-redux";
import { setStatusDrawer } from "Redux/Slices/drawer";

export default function TemporaryDrawer({ anchor, Element }) {
  const dispatch = useDispatch();

  const isOpen = useSelector((state) => state.drawer.isOpen);

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    dispatch(
      setStatusDrawer({
        isOpen: open,
      })
    );
  };

  return (
    <div>
      <TableRowsIcon
        sx={{
          color: "white",
          ":hover": {
            color: "#FFA903",
          },
        }}
        onClick={toggleDrawer(true)}
      ></TableRowsIcon>
      <Drawer anchor={anchor} open={isOpen} onClose={toggleDrawer(false)}>
        <Element />
      </Drawer>
    </div>
  );
}
