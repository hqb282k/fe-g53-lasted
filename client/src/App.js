import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Helmet, HelmetProvider } from "react-helmet-async";

import {
  Home,
  NotFound,
  Room,
  RoomDetail,
  BookingForm,
  Register,
  User,
  AboutUs,
  News,
  ResetPassword,
  ThanksPage,
  CancelPaymentPage,
} from "Pages";
import MenuLayout from "Layouts";

const LayoutWrapper = (Component) => {
  return (
    <MenuLayout>
      <Component />
    </MenuLayout>
  );
};

function App() {
  return (
    <HelmetProvider>
      <BrowserRouter>
        <Helmet titleTemplate="G53" defaultTitle="G53"></Helmet>
        <Routes>
          <Route path="home" element={LayoutWrapper(Home)} />
          <Route path="rooms" element={LayoutWrapper(Room)} />
          <Route path="rooms/:id" element={LayoutWrapper(RoomDetail)} />
          <Route path="rooms/:id/book" element={LayoutWrapper(BookingForm)} />
          <Route path="news/:id" element={LayoutWrapper(News)} />
          <Route path="user" element={LayoutWrapper(User)} />
          <Route path="about-us" element={LayoutWrapper(AboutUs)} />
          <Route
            path="reset-pasword/:token"
            element={LayoutWrapper(ResetPassword)}
          />
          <Route path="register" element={LayoutWrapper(Register)} />
          <Route path="" element={<Navigate to="/home" replace={true} />} />
          <Route
            path="*"
            element={<Navigate to="/not-found" replace={true} />}
          />
          <Route path="not-found" element={<NotFound />} />
          <Route path="pay/success" element={<ThanksPage />} />
          <Route path="pay/cancel" element={<CancelPaymentPage />} />
        </Routes>
      </BrowserRouter>
    </HelmetProvider>
  );
}

export default App;
