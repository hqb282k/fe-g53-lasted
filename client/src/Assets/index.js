export default {
  backgroundAllPage: require("./BackgroundAllPage/vector15.jpg"),
  yellowPage: {
    yellowBg: require("./Page1/1593524056655.jpg"),
  },
  home: {
    hottestHotel: {
      marriott: require("./Home/HotestHotel/jwMarriott.jpg"),
      rex: require("./Home/HotestHotel/rexHotel.jpg"),
      metropole: require("./Home/HotestHotel/metropole.jpg"),
      intercontinental: require("./Home/HotestHotel/intercontinentalNhaTrang.jpg"),
    },
    iconicCity: {
      benThanh: require("./Home/IconicCity/benThanh.jpg"),
      hoGuom: require("./Home/IconicCity/hoanKiem.jpg"),
      sonDoong: require("./Home/IconicCity/sonDoong.jpg"),
    },
  },
  room: {
    undefinedRoom: require("./Room/undifine_room.jpg"),
    busyRoom: require("./Room/room_busy.png"),
    detailHeading: require("./Room/roomDetailHeading.jpg"),
  },
  banners: [require("./Banner/banner_2.jpg"), require("./Banner/banner_1.jpg")],
  logo: require("./Logo/logo.png"),
  register: {
    background: require("./Register/login-background.webp"),
  },
  notFound: {
    logo: require("./NotFound/logo.png"),
  },
  successPay: {
    logo: require("./SuccessPay/success.png"),
  },
  cancelPay: {
    logo: require("./CancelPay/cancelPayment.png"),
  },
  payment: {
    paypalLogo: require("./Payment/PayPal_logo.png"),
  },
  aboutUs: {
    baoMember: require("./AboutUs/bao_member.jpg"),
    leMember: require("./AboutUs/le_member.jpg"),
    sonMember: require("./AboutUs/son_member.jpg"),
    hoangMember: require("./AboutUs/hoang_member.png"),
    datMember: require("./AboutUs/dat_member.png"),
  },
};
