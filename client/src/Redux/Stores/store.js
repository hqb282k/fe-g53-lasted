import { configureStore } from "@reduxjs/toolkit";
import snackbar from "Redux/Slices/snackbar";
import modal from "Redux/Slices/modal";
import drawer from "Redux/Slices/drawer";
import backdrop from "Redux/Slices/backdrop";

export default configureStore({
  reducer: {
    snackbar: snackbar,
    modal: modal,
    drawer: drawer,
    backdrop: backdrop,
  },
});
