import { createSlice } from "@reduxjs/toolkit";
export const modalSlice = createSlice({
  name: "modal",
  initialState: {
    showing: false,
  },
  reducers: {
    setStatusModal: (state, action) => {
      state.showing = action.payload.showing;
    },
  },
});

export const { setStatusModal } = modalSlice.actions;

export default modalSlice.reducer;
