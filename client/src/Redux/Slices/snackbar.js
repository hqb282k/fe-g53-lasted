import { createSlice } from "@reduxjs/toolkit";
export const snackbarSlice = createSlice({
  name: "modal",
  initialState: {
    isShowing: false,
    title: "",
    severity: "",
  },
  reducers: {
    setStatusSnackbar: (state, action) => {
      state.isShowing = action.payload.isShowing;
      state.title = action.payload.title;
      state.severity = action.payload.severity;
    },
  },
});

export const { setStatusSnackbar } = snackbarSlice.actions;

export default snackbarSlice.reducer;
