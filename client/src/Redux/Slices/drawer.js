import { createSlice } from "@reduxjs/toolkit";

export const drawerSlice = createSlice({
  name: "drawer",
  initialState: {
    isOpen: false,
  },
  reducers: {
    setStatusDrawer: (state, action) => {
      state.isOpen = action.payload.isOpen;
    },
  },
});

export const { setStatusDrawer } = drawerSlice.actions;

export default drawerSlice.reducer;
