import { createSlice } from "@reduxjs/toolkit";

export const backdropSlice = createSlice({
  name: "drawer",
  initialState: {
    isOpen: false,
  },
  reducers: {
    setStatusBackdrop: (state, action) => {
      state.isOpen = action.payload.isOpen;
    },
  },
});

export const { setStatusBackdrop } = backdropSlice.actions;

export default backdropSlice.reducer;
