import React from "react";
import { Helmet } from "react-helmet-async";
import {
  Header,
  Footer,
  SubHeader,
  BasicModal,
  SnackBar,
  Backdrop,
} from "Components";
import { Login } from "Pages";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import images from "Assets"
const MenuLayout = (props) => {
  const { children } = props;
  return (
    <React.Fragment>
      <Helmet>
        <title>This is Layout</title>
      </Helmet>
      <Box sx={{ backgroundColor: "#f8f3e7" }}>
        <Header />
        <Box
          sx={{
            paddingTop: "102px",
            background:
              `rgba(35,37,46,0.8) url(${images.backgroundAllPage}) no-repeat`,
              backgroundSize:"cover",
          }}
        >
          {children}
        </Box>
        <Footer />
        <SnackBar></SnackBar>
        <Backdrop></Backdrop>
      </Box>
    </React.Fragment>
  );
};
export default MenuLayout;
