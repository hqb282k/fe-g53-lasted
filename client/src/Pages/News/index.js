import * as React from "react";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import { SubHeader } from "Components";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import { useParams } from "react-router-dom";
import data from "Mocks";
import axios from "axios";
import Grid from "@mui/material/Grid";
import Posts from "Components";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import { Swiper, SwiperSlide } from "swiper/react";
import { Scrollbar } from "swiper";
import { useMediaQuery } from "react-responsive";
import Post from "../Home/post";
import DOMPurify from "dompurify";
import styled from "styled-components";

const PostStyle = styled.div`
  .post-content {
    width: auto;
    height: auto;
    margin: 0 auto;
  }

  .post-content img {
    width: 100%;
    height: 100%;
  }
`;

export default function News() {
  const params = useParams();
  const [post, setPost] = React.useState();
  const [news, setNews] = React.useState([]);
  React.useEffect(() => {
    axios
      .get(
        process.env.REACT_APP_BASE_URL +
          // "http://localhost:" +
          // process.env.REACT_APP_BASE_PORT +
          process.env.REACT_APP_NEWS_GET_BY_ID_API_PATH +
          params.id
      )
      .then((response) => {
        setPost(response.data.objects);
      })
      .catch((error) => {});
    axios
      .get(
        process.env.REACT_APP_BASE_URL +
          // "http://localhost:" +
          //   process.env.REACT_APP_BASE_PORT +
          process.env.REACT_APP_NEWS_GET_ALL_API_PATH
      )
      .then((response) => {
        setNews(response.data.objects);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [params.id]);

  return (
    <>
      {post ? (
        <>
          <SubHeader title={"Tin Tức"}></SubHeader>
          <Container maxwidth="xl">
            <Box sx={{ backgroundColor: "#fffff9", p: 2, marginTop: "-50px" }}>
              <Typography
                variant="h4"
                sx={{ fontWeight: "600", textAlign: "center" }}
              >
                {post.title}
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  fontWeight: "600",
                  opacity: "0.6",
                  textAlign: "center",
                }}
              >
                {post.description}
              </Typography>
              <Box
                sx={{
                  maxWidth: "60%",
                  width: "auto",
                  height: "auto",
                  margin: "0 auto",
                  py: 3,
                }}
              >
                <Box
                  component="img"
                  sx={{ width: "100%", height: "100%" }}
                  alt="The house from the offer."
                  src={post.photo}
                />
              </Box>
              <PostStyle>
                <div
                  className="post-content"
                  dangerouslySetInnerHTML={{
                    __html: DOMPurify.sanitize(post.body),
                  }}
                ></div>
              </PostStyle>
            </Box>
            <Box sx={{ backgroundColor: "#fffff9", mt: 2 }}>
              <Typography
                variant="h6"
                sx={{
                  backgroundColor: "#57112F",
                  color: "white",
                  p: 2,
                  textAlign: "center",
                  textTransform: "capitalize",
                  fontWeight: "600",
                }}
              >
              Tin tức khác
              </Typography>
              <Swiper
                slidesPerView={1}
                spaceBetween={20}
                scrollbar={{
                  hide: true,
                }}
                breakpoints={{
                  601: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                  },
                  1200: {
                    slidesPerView: 3,
                    spaceBetween: 20,
                  },
                }}
                modules={[Scrollbar]}
                className="mySwiper"
              >
                {news
                  .filter((post) => post.id != params.id)
                  .map((post) => (
                    <SwiperSlide>
                      <Post post={post}></Post>
                    </SwiperSlide>
                  ))}
              </Swiper>
            </Box>
          </Container>
        </>
      ) : (
        <Box
          sx={{ backgroundColor: "#fffff9", width: "100vw", height: "260px" }}
        >
          <Typography variant="h4" sx={{ py: 2, textAlign: "center" }}>
            {" "}
            Hệ thống có chút sự cố nhỏ, liên hệ: 0971769799 để được hỗ trợ
          </Typography>
        </Box>
      )}
    </>
  );
}
