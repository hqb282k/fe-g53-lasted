import React from "react";
import ReactDOM from "react-dom";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import images from "Assets";
import { Navigate, useNavigate, useSearchParams } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import Grid from "@mui/material/Grid";
import axios from "axios";
import CircularProgress from "@mui/material/CircularProgress";

const convertMoney = (price) => {
  let result = "";
  switch (true) {
    case price / 1000000 >= 1: {
      const temp = price / 1000000 + "";
      // For : 1400000
      if (temp.length == 3) {
        result = price / 1000000 + "00.000 VND";
      } else {
        // For : 1450000
        if (temp.length == 4) {
          result = price / 1000000 + "0.000 VND";
        }
        // For : 1456000
        else {
          result = price / 1000000 + ".000 VND";
        }
      }
      break;
    }
    // For : 999000
    case price / 100000 < 10 && price / 1000 < 1000: {
      result = price / 1000 + ".000 VND";
      break;
    }
    // For : 99000
    case price / 10000 < 10 && price / 1000 < 100: {
      result = price / 1000 + ".000 VND";
      break;
    }
    // For : 9000
    case price / 10000 < 1: {
      result = price / 1000 + ".000 VND";
      break;
    }
  }
  return result;
};

const CancelPaymentPage = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const isSmallScreen = useMediaQuery({ query: "(max-width: 700px)" });

  return (
    <Box
      sx={{
        width: "100vw",
        height: `${isSmallScreen ? "135vh" : "100vh"}`,
        position: "relative",
        backgroundColor: "#1a2238",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%,-50%)",
        }}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              variant={`${isSmallScreen ? "h4" : "h3"}`}
              sx={{
                color: "#FFA903",
                textTransform: "uppercase",
                textAlign: "center",
                p: 2,
                borderBottom: "1px dashed white",
              }}
            >
              Hủy Thanh Toán Thành Công
            </Typography>
          </Grid>
          <Grid item xs={12} lg={6}>
            <Box sx={{ border: "1px dashed white", p: 3 }}>
              <Typography
                variant="h5"
                sx={{
                  color: "white",
                  textTransform: "uppercase",
                  textAlign: "center",
                  py: 1,
                  borderBottom: "1px dashed white",
                }}
              >
                Hóa đơn của bạn
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  color: "white",
                }}
              >
                {`Họ tên: ${
                  JSON.parse(localStorage.getItem("user")).firstName
                } ${JSON.parse(localStorage.getItem("user")).lastName}`}
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  color: "white",
                }}
              >
                {`Ngày đặt: ${
                  JSON.parse(localStorage.getItem("receipt")).timeBook
                }`}
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  color: "white",
                }}
              >
                {`Checkin: ${
                  JSON.parse(localStorage.getItem("receipt")).checkIn
                }`}
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  color: "white",
                }}
              >
                {`CheckOut: ${
                  JSON.parse(localStorage.getItem("receipt")).checkOut
                }`}
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  color: "white",
                }}
              >
                {convertMoney(
                  JSON.parse(localStorage.getItem("receipt")).totalAmount * 1.1
                )}
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} lg={6}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box
                component="img"
                sx={{ width: "200px" }}
                alt="The house from the offer."
                src={images.cancelPay.logo}
              />
              <Typography
                variant="h5"
                sx={{
                  color: "white",
                  textTransform: "uppercase",
                  textAlign: "center",
                }}
              >
                Không sao, bạn có thể đặt các loại phòng khác
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box sx={{ display: "flex", gap: 2, justifyContent: "center" }}>
              <Button
                sx={{
                  width: "200px",
                  backgroundColor: "#FFA903",
                  fontWeight: "600",
                  color: "#1a2238",
                  my: 3,
                  ":hover": {
                    color: "white",
                    border: "1px dashed white",
                  },
                }}
                onClick={() => {
                  navigate("/home");
                }}
              >
                Quay lại trang chủ
              </Button>
              <Button
                sx={{
                  backgroundColor: "#FFA903",
                  fontWeight: "600",
                  color: "#1a2238",
                  my: 3,
                  ":hover": {
                    color: "white",
                    border: "1px dashed white",
                  },
                }}
                onClick={() => {
                  navigate("/home");
                }}
              >
                Quay lại đặt phòng
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
export default CancelPaymentPage;
