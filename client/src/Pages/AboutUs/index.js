import * as React from "react";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import images from "Assets";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import { SubHeader } from "Components";
import { Typography } from "@mui/material";

const members = [
  {
    name: "Chu Hoàng Sơn",
    role: "Lập trình viên backend",
    description: "Chủ tịch khách sạn G53",
    avatar: images.aboutUs.sonMember,
  },
  {
    name: "Hồ Quốc Bảo",
    role: "Lập trình viên frontend",
    description: "Chăm sóc khách hàng",
    avatar: images.aboutUs.baoMember,
  },
  {
    name: "Trịnh Xuân Lễ",
    role: "Lập trình viên frontend",
    description: "Quản lý",
    avatar: images.aboutUs.leMember,
  },
  {
    name: "Đặng Huy Hoàng",
    role: "Tài liệu",
    description: "Phó chủ tịch khách sạn G53",
    avatar: images.aboutUs.hoangMember,
  },
  {
    name: "Nguyễn Văn Đạt",
    role: "Tài liệu",
    description: "Phó quản lý",
    avatar: images.aboutUs.datMember,
  },
];

const MemberCard = ({ member }) => {
  return (
    <Box
      sx={{
        width: "100%",
        background: `url(${member.avatar})`,
        paddingBottom: "100%",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "70% 70%",
        borderRadius: "20px",
      }}
    />
  );
};

export default function AboutUs() {
  return (
    <>
      <SubHeader title={"Về chúng tôi"}></SubHeader>
      <Container
        maxwidth="xl"
        sx={{
          marginTop: "-50px",
          backgroundColor: "rgba(250,250,250,0.7)",
          p: 2,
        }}
      >
        <Typography
          variant="h3"
          sx={{
            textAlign: "center",
            py: 3,
            fontWeight: "600",
            fontStyle: "italic",
          }}
        >
          Thư Ngỏ
        </Typography>
        <Typography variant="h5" sx={{ fontStyle: "italic" }}>
          "Lời đầu tiên, khách sạn G53 thuộc Công ty Cổ phần Phát triển G53 xin
          gửi đến Quý khách hàng và Quý đối tác lời chúc sức khoẻ và lời chào
          trân trọng nhất! Khách sạn G53 là tổ hợp du lịch lưu trú, dịch vụ giải
          trí, ăn uống và thể thao toạ lạc tại thành phố HN, một trong những vị
          trí đắc địa bậc nhất Việt Nam. Khách sạn có địa thế đối mặt với các
          tuyến phố lớn. Với quy mô 7 tầng và 70 phòng nghỉ, Khách sạn G53 được
          thiết kế theo phong cách hiện đại, sang trọng đầy đủ tiện nghi với
          nhiều loại phòng nghỉ đạt chuẩn quốc tế. Bên cạnh dịch vụ lưu trú đạt
          chuẩn 4 sao, Khách sạn G53 còn sẵn sàng cung cấp đầy đủ các dịch vụ
          tiện ích nâng cao như: hồ bơi, phòng tập thể thao, spa, xông hơi, dịch
          vụ thể thao bãi biển hay tổ chức các tour du lịch kỳ thú hấp dẫn trong
          địa bàn. Đặc biệt, khách sạn Khách sạn G53 còn sở hữu nhà hàng với
          view thành phố rộng lớn phục vụ buffet sáng, tiệc bánh cùng các phòng
          ăn VIP sang trọng, riêng tư cùng các dịch vụ, tiện nghi ăn uống hấp
          dẫn. Khách sạn G53 còn được trang bị sảnh tiệc/hội nghị lớn với sức
          chứa lên đến 200 thực khách với không gian thoáng đãng, view rừng xanh
          mát có sân khấu với hệ thống âm thanh ánh sáng, màn hình LED hiện đại
          phù hợp với tất cả các chương trình hội nghị, hội thảo, tiệc cơ quan,
          đoàn thể, tiệc gala, tiệc cưới.. "
        </Typography>
        {members.map((member, index) => (
          <div data-aos="zoom-in" style={{ width: "100%" }}>
            <Grid
              container
              spacing={2}
              sx={{
                mt: 2,
                mb: 5,
                display: "flex",
                flexDirection: `${index % 2 == 0 ? "row-reverse" : "row"}`,
              }}
            >
              <Grid item xs={4}>
                <MemberCard member={member}></MemberCard>
              </Grid>
              <Grid item xs={8}>
                <Typography
                  variant="h4"
                  sx={{
                    textAlign: "center",
                    py: 2,
                    fontWeight: "600",
                    fontStyle: "italic",
                  }}
                >
                  {member.name}
                </Typography>
                <Typography
                  variant="h6"
                  sx={{
                    textAlign: "center",
                    pb: 1,
                    fontWeight: "500",
                    fontStyle: "italic",
                    opacity: 0.8,
                  }}
                >
                  {member.description}
                </Typography>
                <Typography
                  variant="h6"
                  sx={{
                    textAlign: "center",
                    py: 1,
                    fontWeight: "600",
                    fontStyle: "italic",
                    opacity: 0.7,
                  }}
                >
                  {member.role}
                </Typography>
              </Grid>
            </Grid>
          </div>
        ))}
      </Container>
    </>
  );
}
