import React from "react";
import ReactDOM from "react-dom";
import { useDispatch } from "react-redux";
import { Banner } from "Components";
import { Sale, TopSale } from "Pages";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import { useNavigate } from "react-router-dom";
import images from "Assets";
import { textAlign } from "@mui/system";

const Post = ({ post }) => {
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        p: 2,
        borderRadius: "20px",
        backgroundColor: "#f5f3f0",
      }}
    >
      {/* Avatar */}
      <Box
        sx={{
          background: `url(${post.photo})`,
          backgroundSize: "cover",
          height: "200px",
          flexGrow: "1",
          borderRadius: "20px 20px 0 0",
        }}
        alt="The house from the offer."
      />
      {/* Content */}
      <Box
        sx={{ backgroundColor: "#f5f3f0", p: 2, borderRadius: "0 0 20px 20px" }}
      >
        <Box sx={{ pb: 2 }}>
          <Typography variant="h6">{post.title}</Typography>
          <Typography
            variant="text"
            sx={{ color: "#FFA903", fontWeight: "600" }}
          >
            G53 Ha Noi
          </Typography>
        </Box>
      </Box>
      <Box
        // onClick={() => navigate(`/room/${room.id}`)}
        sx={{
          backgroundColor: "#FFA903",
          p: 1,
          textAlign: "center",
          borderRadius: "0 0 20px 20px",
          cursor: "pointer",
          ":hover": { opacity: 0.5 },
        }}
        onClick={() => {
          window.scrollTo(0, 0);
          navigate(`/news/${post.id}`);
        }}
      >
        <Typography variant="text">Xem thêm</Typography>
      </Box>
    </Box>
  );
};

export default Post;
