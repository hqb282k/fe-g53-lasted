import React from "react";
import ReactDOM from "react-dom";
import { useDispatch } from "react-redux";
import { Banner } from "Components";
import { Sale, TopSale } from "Pages";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import images from "Assets";
import data from "Mocks";
import Grid from "@mui/material/Grid";
import Post from "./post";
import { Swiper, SwiperSlide } from "swiper/react";
import { Scrollbar } from "swiper";
// Import Swiper styles
import "swiper/css";
import "swiper/css/scrollbar";
import axios from "axios";

const TopPlace = () => {
  const [news, setNews] = React.useState([]);
  React.useEffect(() => {
    axios
      .get(
        process.env.REACT_APP_BASE_URL +
          // "http://localhost:" +
          //   process.env.REACT_APP_BASE_PORT +
          process.env.REACT_APP_NEWS_GET_ALL_API_PATH
      )
      .then((response) => {
        setNews(response.data.objects);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <>
      {news ? (
        <div data-aos="fade-up" data-aos-delay="100" data-aos-offset="200">
          <Box
            sx={{
              my: 5,
              backgroundColor: "#fffff9",
              borderRadius: "20px",
              py: 2,
              boxShadow:
                "rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px",
            }}
          >
            <Box sx={{ textAlign: "center" }}>
              <Typography
                variant="h4"
                sx={{
                  color: "#FFA903",
                  fontWeight: "600",
                  textTransform: "uppercase",
                }}
              >
                Tin Tức
              </Typography>
              <Typography
                variant="h6"
                sx={{ fontWeight: "700", color: "#21283F" }}
              >
                Hành trình vẫn đang tiếp tục...
              </Typography>
            </Box>
            <Swiper
              slidesPerView={1}
              spaceBetween={20}
              scrollbar={{
                hide: true,
              }}
              breakpoints={{
                601: {
                  slidesPerView: 2,
                  spaceBetween: 20,
                },
                1200: {
                  slidesPerView: 3,
                  spaceBetween: 20,
                },
              }}
              modules={[Scrollbar]}
              className="mySwiper"
            >
              {news.map((post,index) => (
                <SwiperSlide  key={index}>
                  <Post post={post}></Post>
                </SwiperSlide>
              ))}
            </Swiper>
          </Box>
        </div>
      ) : (
        <h1>nothing</h1>
      )}
    </>
  );
};

export default TopPlace;
