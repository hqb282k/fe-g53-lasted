import React from "react";
import ReactDOM from "react-dom";
import { useDispatch } from "react-redux";
import { Banner } from "Components";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import images from "Assets";
import data from "Mocks";
import Grid from "@mui/material/Grid";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination, Navigation } from "swiper";

const Sponsor = ({ sponsors, delay }) => {
  return (
    <Box sx={{ backgroundColor: "#632a4b" }}>
      <Container maxwidth="xl">
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          centeredSlides={true}
          autoplay={{
            delay: delay,
            disableOnInteraction: false,
          }}
          pagination={{
            clickable: false,
          }}
          navigation={false}
          modules={[Autoplay]}
          className="mySwiper"
        >
          <SwiperSlide>
            <Grid
              container
              sx={{
                display: "flex",
                py: 5,
              }}
            >
              {data.sponsors.map((sponsor, index) => (
                <Grid
                  item
                  xs={3}
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  key={index}
                >
                  <Box
                    component="img"
                    sx={{
                      maxWidth: "100%",
                      minHeight: "73px",
                    }}
                    alt="The house from the offer."
                    src={sponsor.avatar}
                  />
                </Grid>
              ))}
            </Grid>
          </SwiperSlide>
          <SwiperSlide>
            <Grid
              container
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                py: 5,
              }}
            >
              {data.sponsors.map((sponsor) => (
                <Grid
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  item
                  xs={3}
                >
                  <Box
                    component="img"
                    sx={{
                      maxWidth: "100%",
                      minHeight: "73px",
                    }}
                    alt="The house from the offer."
                    src={sponsor.avatar}
                  />
                </Grid>
              ))}
            </Grid>
          </SwiperSlide>
        </Swiper>
      </Container>
    </Box>
  );
};

export default Sponsor;
