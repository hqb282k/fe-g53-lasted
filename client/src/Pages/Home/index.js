import React from "react";
import ReactDOM from "react-dom";
import { useDispatch } from "react-redux";
import { setStatusModal } from "Redux/Slices/modal";
import { Banner, BasicModal } from "Components";
import { Sale, TopSale } from "Pages";
import Offer from "./offer";
import Card from "./card";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import images from "Assets";
import data from "Mocks";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import TopRoom from "./topPlace";
import Sponsor from "./sponsor";
import AutoAwesomeIcon from "@mui/icons-material/AutoAwesome";
import { useNavigate } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";
import RoomPreferencesIcon from "@mui/icons-material/RoomPreferences";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import LocalAirportIcon from "@mui/icons-material/LocalAirport";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
// import required modules
import { Pagination } from "swiper";
import TopPlace from "./topPlace";
// Import react responsive
import { useMediaQuery } from "react-responsive";
import AOS from "aos";
import "aos/dist/aos.css"; // You can also use <link> for styles
// ..
AOS.init();
const Home = () => {
  const isSmallScreen = useMediaQuery({ query: "(max-width: 600px)" });
  const isMiddleScreen = useMediaQuery({ query: "(max-width: 900px)" });
  const isLargeScreen = useMediaQuery({ query: "(min-width: 1200px)" });
  const dispatch = useDispatch();
  const navigate = useNavigate();
  return (
    <>
      {/* About banner */}
      <Banner
        spaceBetween={50}
        slidesPerView={1}
        banners={images.banners}
      ></Banner>
      {/* Hotel offer deal */}
      <div data-aos="fade-up" data-aos-delay="100" data-aos-offset="200">
        <Box sx={{ py: 4, backgroundColor: "#fffff9" }}>
          <Container maxwidth="xl">
            <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
              <Box sx={{ flexGrow: 1 }}>
                <Typography
                  variant="h6"
                  sx={{ color: "#FFA903", textTransform: "uppercase" }}
                >
                  Tận hưởng phong cách sống
                </Typography>
                <Typography
                  variant={`${isSmallScreen ? "h4" : "h3"}`}
                  sx={{ fontWeight: "600" }}
                >
                  TỪ KHÁCH SẠN
                </Typography>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  cursor: "pointer",
                  fontWeight: "550",
                  ":hover": {
                    color: "#FFA903",
                  },
                  justifyContent: "center",
                }}
              >
                <Typography
                  variant={`${isSmallScreen ? "text" : "h6"}`}
                  sx={{
                    textAlign: `${isSmallScreen ? "end" : ""}`,
                    textTransform: "uppercase",
                    fontWeight: "600",
                    opacity: "0.5",
                  }}
                  onClick={() => {
                    navigate("/rooms");
                    window.scrollTo(0, 0);
                  }}
                >
                  Xem Thêm
                </Typography>
                <ArrowRightAltIcon
                  sx={{ px: 1, opacity: "0.5" }}
                ></ArrowRightAltIcon>
              </Box>
            </Box>
          </Container>
          <Splide
            options={{
              rewind: false,
              perPage: 3,
              perMove: 3,
              gap: 20,
              padding: "1rem",
              pagination: false,
              breakpoints: {
                650: {
                  perPage: 1,
                  perMove: 1,
                  padding: "1rem",
                },
                1200: {
                  perPage: 2,
                  perMove: 2,
                },
              },
            }}
            aria-label="My Favorite Images"
          >
            {data.offers.map((offer,index) => (
              <SplideSlide>
                <Offer key={index} offer={offer}></Offer>
              </SplideSlide>
            ))}
          </Splide>
        </Box>
      </div>

      {/* Hottest hotel */}
      <Container maxwidth="xl">
        <div data-aos="fade-up" data-aos-delay="100" data-aos-offset="200">
          <Box
            sx={{
              my: 5,
              p: 2,
              backgroundColor: "rgba(255,255,249,1)",
              borderRadius: "20px",
              boxShadow:
                "rgba(0, 0, 0, 0.25) 0px 14px 28px, rgba(0, 0, 0, 0.22) 0px 10px 10px",
            }}
          >
            <Typography
              variant="h4"
              sx={{
                pb: 3,
                color: "#FFA903",
                textTransform: "uppercase",
                fontWeight: "600",
              }}
            >
              Nâng tầm trải nghiệm của bạn
            </Typography>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                sm={6}
                lg={4}
                sx={{ display: "flex", flexDirection: "column" }}
              >
                <Typography variant="h5" sx={{ pb: 3, flexGrow: 1 }}>
                  Bạn không còn xa lạ với những khách sạn tuyệt vời hàng đầu tại
                  các thành phố lớn, nhưng lại quá khó để tiếp cận về mức giá,
                  địa điểm, thời gian ?
                </Typography>
                <Typography variant="h6" sx={{ pb: 3, fontWeight: "600" }}>
                  Đừng lo, G53 Hotel có những lựa chọn tuyệt vời khác cho bạn
                </Typography>
                <Box
                  sx={{
                    backgroundColor: "#FFA903",
                    width: "200px",
                    margin: "0 auto",
                    borderRadius: "30px",
                    mb: 3,
                    cursor: "pointer",
                    ":hover": {
                      backgroundColor: "white",
                      color: "#FFA903",
                    },
                  }}
                >
                  <Typography
                    variant="h6"
                    sx={{
                      px: 2,
                      py: 1,
                      textAlign: "center",
                      fontWeight: "600",
                    }}
                    onClick={() => {
                      navigate("/rooms");
                      window.scrollTo(0, 0);
                    }}
                  >
                    Khám Phá Ngay
                  </Typography>
                </Box>
                <div data-aos="flip-right">
                  <Box
                    sx={{
                      background: `url(${images.home.hottestHotel.marriott})`,
                      width: "100%",
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "cover",
                      paddingBottom: "100%",
                      position: "relative",
                      borderRadius: "20px",
                    }}
                  >
                    <Box
                      sx={{
                        width: "70%",
                        backgroundColor: "rgba(31, 37, 46, 0.6)",
                        position: "absolute",
                        bottom: "0",
                        p: 2,
                        borderRadius: "0 20px 0 20px ",
                      }}
                    >
                      <Typography
                        variant="h6"
                        sx={{
                          color: "white",
                          textAlign: "center",
                        }}
                      >
                        JW Marriott Hanoi
                      </Typography>
                    </Box>
                  </Box>
                </div>
              </Grid>
              <Grid item xs={12} sm={6} lg={4}>
                <div data-aos="flip-down" style={{ height: "100%" }}>
                  <Box
                    sx={{
                      background: `url(${images.home.hottestHotel.metropole})`,
                      width: "100%",
                      height: `${isSmallScreen ? "none" : "100%"}`,
                      paddingBottom: `${isSmallScreen ? "100%" : "none"}`,
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "cover",
                      backgroundPosition: "bottom center",
                      position: "relative",
                      borderRadius: "20px",
                    }}
                  >
                    <Box
                      sx={{
                        width: "60%",
                        backgroundColor: "rgba(31, 37, 46, 0.6)",
                        position: "absolute",
                        bottom: "0",
                        p: 2,
                        borderRadius: "0 20px 0 20px ",
                      }}
                    >
                      <Typography
                        variant="h6"
                        sx={{
                          color: "white",
                          textAlign: "center",
                        }}
                      >
                        Metropole Hanoi
                      </Typography>
                    </Box>
                  </Box>
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                lg={4}
                sx={{
                  display: "flex",
                  flexDirection: `${
                    !isSmallScreen && !isLargeScreen ? "row" : "column"
                  }`,
                  gap: 2,
                }}
              >
                <Box sx={{ width: "100%" }}>
                  <div data-aos="flip-right">
                    <Box
                      sx={{
                        background: `url(${images.home.hottestHotel.rex})`,
                        width: "100%",
                        paddingBottom: "100%",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "40% 20%",
                        backgroundSize: "cover",
                        position: "relative",
                        borderRadius: "20px",
                      }}
                    >
                      <Box
                        sx={{
                          width: "60%",
                          backgroundColor: "rgba(31, 37, 46, 0.6)",
                          position: "absolute",
                          bottom: "0",
                          p: 2,
                          borderRadius: "0 20px 0 20px ",
                        }}
                      >
                        <Typography
                          variant="h6"
                          sx={{
                            color: "white",
                            textAlign: "center",
                          }}
                        >
                          Rex Sai Gon
                        </Typography>
                      </Box>
                    </Box>
                  </div>
                </Box>
                <Box
                  sx={{
                    width: "100%",
                  }}
                >
                  <div data-aos="flip-left">
                    <Box
                      sx={{
                        background: `url(${images.home.hottestHotel.intercontinental})`,
                        paddingBottom: "100%",
                        backgroundRepeat: "no-repeat",
                        backgroundSize: "cover",
                        backgroundPosition: "30% 25%",
                        position: "relative",
                        borderRadius: "20px",
                      }}
                    >
                      <Box
                        sx={{
                          width: "70%",
                          backgroundColor: "rgba(31, 37, 46, 0.6)",
                          position: "absolute",
                          bottom: "0",
                          p: 2,
                          borderRadius: "0 20px 0 20px ",
                        }}
                      >
                        <Typography
                          variant="h6"
                          sx={{
                            color: "white",
                            textAlign: "center",
                          }}
                        >
                          Intercontinental Nha Trang
                        </Typography>
                      </Box>
                    </Box>
                  </div>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </div>
      </Container>
      {/* iconic cities */}
      <Box sx={{ py: 5, backgroundColor: "#fffff9" }}>
        <Grid container spacing={4}>
          <Grid item xs={12} lg={7}>
            <Box
              sx={{
                background: `url(${images.home.iconicCity.sonDoong})`,
                width: "100%",
                height: "646px",
                backgroundPosition: "center",
                position: "relative",
              }}
            >
              <Box
                sx={{
                  position: "absolute",
                  bottom: "0",
                  color: "white",
                  backgroundColor: "rgba(31, 37, 46, 0.7)",
                  p: 2,
                }}
              >
                <Typography
                  variant="h5"
                  sx={{ color: "#FFA903", fontWeight: "600" }}
                >
                  Hành trình nào đang đợi bạn ?
                </Typography>
                <Typography variant="text">
                  Hãy khám phá ngay các địa điểm của G53, nơi nghỉ dưỡng cùng
                  những dịch vụ tốt nhất tại các thành phố lớn
                </Typography>
              </Box>
            </Box>
          </Grid>

          <Grid
            item
            xs={12}
            lg={5}
            sx={{ display: "flex", flexDirection: "column", gap: 2 }}
          >
            <Grid container spacing={2} sx={{ height: "50%", px: 1 }}>
              <Grid item xs={6} lg={5}>
                <div data-aos="flip-down">
                  <Box
                    sx={{
                      background: `url(${images.home.iconicCity.hoGuom})`,
                      width: "100%",
                      paddingBottom: "100%",
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "cover",
                      backgroundPosition: "30% 25%",
                      position: "relative",
                      borderRadius: "20px",
                    }}
                  />
                </div>
              </Grid>
              <Grid item xs={6} lg={5}>
                <div data-aos="zoom-in">
                  <Typography
                    variant="h4"
                    sx={{
                      pt: 2,
                      textTransform: "uppercase",
                      fontWeight: "600",
                    }}
                  >
                    Ha Noi
                  </Typography>
                  <Box
                    sx={{
                      backgroundColor: "black",
                      height: "2px",
                      width: "50px",
                      my: 2,
                    }}
                  ></Box>
                  <Typography variant="h6" sx={{ color: "#707070", pb: 2 }}>
                    Thủ đô Hà Nội
                  </Typography>
                  <Typography
                    variant="text"
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      cursor: "pointer",
                      ":hover": {
                        color: "#FFA903",
                      },
                    }}
                    onClick={() => {
                      navigate("/rooms");
                      window.scrollTo(0, 0);
                    }}
                  >
                    Xem Thêm<LocalAirportIcon sx={{ px: 1 }}></LocalAirportIcon>
                  </Typography>
                </div>
              </Grid>
            </Grid>
            <Grid
              container
              spacing={2}
              sx={{
                height: "50%",
                flexDirection: "row-reverse",
                justifyContent: "start",
                px: 1,
              }}
            >
              <Grid item xs={6} lg={5}>
                <div data-aos="flip-up">
                  <Box
                    component="img"
                    sx={{
                      background: `url(${images.home.iconicCity.benThanh})`,
                      width: "100%",
                      paddingBottom: "100%",
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "cover",
                      backgroundPosition: "30% 25%",
                      position: "relative",
                      borderRadius: "20px",
                    }}
                  />
                </div>
              </Grid>
              <Grid item xs={6} lg={5}>
                <div data-aos="zoom-in">
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "end",
                      textAlign: "end",
                    }}
                  >
                    <Typography
                      variant="h4"
                      sx={{
                        pt: 2,
                        textTransform: "uppercase",
                        fontWeight: "600",
                      }}
                    >
                      Ho Chi Minh
                    </Typography>
                    <Box
                      sx={{
                        backgroundColor: "black",
                        height: "2px",
                        width: "50px",
                        my: 2,
                      }}
                    ></Box>
                    <Typography variant="h6" sx={{ color: "#707070", pb: 2 }}>
                      Thành phố Hồ Chí Minh
                    </Typography>
                    <Typography
                      variant="text"
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                    >
                      Sắp hoàn thành
                      <RoomPreferencesIcon sx={{ px: 1 }}></RoomPreferencesIcon>
                    </Typography>
                  </Box>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
      {/* About Banner */}
      <Box
        sx={{
          backgroundImage: `url("https://www.tripsavvy.com/thmb/FwmQ-JvBEBDDlVb-j_zdEo0iVsA=/2048x1152/smart/filters:no_upscale()/beach-5b59c9b7c9e77c004b3e0ff0.jpg")`,
          backgroundRepeat: "no-repeat",
          backgroundAttachment: "fixed",
          backgroundSize: "cover",
          color: "white",
          height: "500px",
          backgroundPosition: "center",
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "black",
            opacity: "0.3",
          }}
        ></Box>

        <Container maxwidth={"xl"}>
          <Box
            sx={{
              position: "absolute",
              top: `${isSmallScreen ? "10%" : "15%"}`,
              backgroundColor: "rgba(31, 37, 46, 0.3)",
              p: 4,
              borderRadius: "20px",
            }}
          >
            <div data-aos="fade-down-right">
              <Typography
                variant="h5"
                sx={{
                  py: 1,
                  color: "#FFA903",
                  fontWeight: "600",
                }}
              >
                Khách hàng cảm nhận thế nào ?
              </Typography>
              <Typography
                variant="h7"
                sx={{
                  py: 2,
                }}
              >
                Hãy đăng ký ngay để đặt phòng với ưu đãi tốt nhất
              </Typography>
              <Grid
                container
                spacing={2}
                sx={{
                  py: 4,
                }}
              >
                <Grid item xs={5} sm={6}>
                  <Box
                    sx={{
                      backgroundColor: "#f5f3f0",
                      display: "flex",
                      alignItems: "center",
                      flexDirection: "column",
                      borderRadius: "20px",
                      px: 3,
                      py: 2,
                    }}
                  >
                    <PersonOutlineIcon
                      sx={{ color: "#632a4b", fontSize: "40px" }}
                    ></PersonOutlineIcon>
                    <Typography
                      variant={`${isSmallScreen ? "h6" : "h4"}`}
                      sx={{ color: "black" }}
                    >
                      5830+
                    </Typography>
                    <Typography
                      variant={`${isSmallScreen ? "text" : "h6"}`}
                      sx={{ color: "black" }}
                    >
                      Hài Lòng
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={5} sm={6}>
                  <Box
                    sx={{
                      backgroundColor: "#f5f3f0",
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      borderRadius: "20px",
                      px: 3,
                      py: 2,
                    }}
                  >
                    <AutoAwesomeIcon
                      sx={{ color: "#632a4b", fontSize: "40px" }}
                    ></AutoAwesomeIcon>
                    <Typography
                      variant={`${isSmallScreen ? "h6" : "h4"}`}
                      sx={{ color: "black" }}
                    >
                      98%
                    </Typography>
                    <Typography
                      variant={`${isSmallScreen ? "text" : "h6"}`}
                      sx={{ color: "black" }}
                    >
                      Đáp ứng
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
              <Box sx={{}}>
                <Typography variant="text">Bạn có thắc mắc ?</Typography>
                <Button
                  variant="contained"
                  sx={{
                    backgroundColor: "#FFA903",
                    color: "black",
                    mx: 2,
                    ":hover": {
                      backgroundColor: "#f5f3f0",
                      color: "#FFA903",
                    },
                  }}
                >
                  Liên Hệ Ngay
                </Button>
              </Box>
            </div>
          </Box>
        </Container>
      </Box>
      {/* Top room */}
      <Container maxwidth="xl">
        <TopPlace></TopPlace>
      </Container>
      {/* Sponsor */}
      <Sponsor sponsors={data.sponsors} delay={1000}></Sponsor>
    </>
  );
};

export default Home;
