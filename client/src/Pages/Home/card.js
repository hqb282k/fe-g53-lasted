import React from "react";
import ReactDOM from "react-dom";
import { useDispatch } from "react-redux";
import { Banner } from "Components";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import images from "Assets";
// Import react responsive
import { useMediaQuery } from "react-responsive";
const Card = ({ room }) => {
  const isMiddleScreen = useMediaQuery({ query: "(max-width: 700px)" });
  return (
    <Box
      sx={{
        backgroundColor: "white",
        borderRadius: "20px",
        boxShadow:
          "rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px",
      }}
    >
      <Box
        component="img"
        sx={{
          width: "100%",
          borderRadius: "20px 20px 0 0",
        }}
        alt="The house from the offer."
        src={
          "https://cdn.cet.edu.vn/wp-content/uploads/2018/03/khach-san-phan-loai-theo-chuc-nang.jpg"
        }
      />
      <Box
        sx={{ backgroundColor: "#f5f3f0", p: 2, borderRadius: "0 0 20px 20px" }}
      >
        <Typography variant="h4">{room.roomName}</Typography>
        <Typography variant="h6">{room.type}</Typography>
        <Box sx={{ display: "flex", alignItems: "center", gap: 1 }}>
          <AttachMoneyIcon></AttachMoneyIcon>
          <Typography variant="h5">{room.price}</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default Card;
