import React from "react";
import ReactDOM from "react-dom";
import { useDispatch } from "react-redux";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import { useMediaQuery } from "react-responsive";
import { Navigate, useNavigate } from "react-router-dom";

const Offer = ({ offer }) => {
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        background: `url(${offer.avatar})`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundSize: "cover",
        position: "relative",
        width: "100%",
        paddingBottom: "100%",
        borderRadius: "20px",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: "70%",
          paddingBottom: "100%",
          backgroundColor: "rgba(240,240,240,0.6)",
          textAlign: "center",
          borderRadius: "20px",
          p: 1,
        }}
      >
        <Typography
          variant="text"
          sx={{
            color: "black",
            textTransform: "uppercase",
            fontWeight: "500",
            opacity: "0.7",
          }}
        >
          {offer.valid}
        </Typography>
        <Box
          sx={{
            backgroundColor: "black",
            width: "50px",
            height: "2px",
            margin: "0 auto",
            my: 2,
          }}
        ></Box>
        <Typography variant="text" sx={{ fontWeight: "600", display: "block" }}>
          {offer.title}
        </Typography>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#FFA903",
            color: "black",
            borderRadius: "20px",
            my: 2,
            ":hover": {
              backgroundColor: "#f5f3f0",
              color: "#FFA903",
            },
          }}
          onClick={() => {
            navigate("/rooms");
            window.scrollTo(0, 0);
          }}
        >
          Chi Tiết
        </Button>
      </Box>
    </Box>
  );
};

export default Offer;
