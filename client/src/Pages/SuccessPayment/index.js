import React from "react";
import ReactDOM from "react-dom";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import images from "Assets";
import { Navigate, useNavigate, useSearchParams } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import Grid from "@mui/material/Grid";
import axios from "axios";
import CircularProgress from "@mui/material/CircularProgress";
import { NotFound } from "Pages";

const convertMoney = (price) => {
  let result = "";
  switch (true) {
    case price / 1000000 >= 1: {
      const temp = price / 1000000 + "";
      // For : 1400000
      if (temp.length == 3) {
        result = price / 1000000 + "00.000 VND";
      } else {
        // For : 1450000
        if (temp.length == 4) {
          result = price / 1000000 + "0.000 VND";
        }
        // For : 1456000
        else {
          result = price / 1000000 + ".000 VND";
        }
      }
      break;
    }
    // For : 999000
    case price / 100000 < 10 && price / 1000 < 1000: {
      result = price / 1000 + ".000 VND";
      break;
    }
    // For : 99000
    case price / 10000 < 10 && price / 1000 < 100: {
      result = price / 1000 + ".000 VND";
      break;
    }
    // For : 9000
    case price / 10000 < 1: {
      result = price / 1000 + ".000 VND";
      break;
    }
  }
  return result;
};

const ThanksPage = () => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const [paymentStatus, setPaymentStatus] = React.useState(false);
  const isSmallScreen = useMediaQuery({ query: "(max-width: 700px)" });
  React.useEffect(() => {
    axios
      .post(
        // process.env.REACT_APP_BASE_PORT +
        process.env.REACT_APP_BASE_URL +
          process.env.REACT_APP_ROOM_BOOK_API_PATH +
          `?bookingId=${searchParams.get("bookingId")}`
      )
      .then((response) => {
        setPaymentStatus(true);
      })
      .catch((error) => {
        console.log("not success");
      });
  });
  return (
    <>
      {localStorage.getItem("user") !== null &&
      localStorage.getItem("receipt") != null ? (
        <Box
          sx={{
            width: "100vw",
            height: `${isSmallScreen ? "135vh" : "100vh"}`,
            position: "relative",
            backgroundColor: "#1a2238",
          }}
        >
          <Box
            sx={{
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%,-50%)",
            }}
          >
            <Grid container spacing={3}>
              <Grid item xs={12}>
                {paymentStatus ? (
                  <Typography
                    variant={`${isSmallScreen ? "h4" : "h3"}`}
                    sx={{
                      color: "#FFA903",
                      textTransform: "uppercase",
                      textAlign: "center",
                      p: 2,
                      borderBottom: "1px dashed white",
                    }}
                  >
                    Thanh Toán Thành Công
                  </Typography>
                ) : (
                  <Typography
                    variant={`${isSmallScreen ? "h4" : "h3"}`}
                    sx={{
                      color: "#FFA903",
                      textTransform: "uppercase",
                      textAlign: "center",
                      p: 2,
                      borderBottom: "1px dashed white",
                    }}
                  >
                    Đang kiểm tra giao dịch
                  </Typography>
                )}
              </Grid>
              <Grid item xs={12} lg={6}>
                <Box sx={{ border: "1px dashed white", p: 3 }}>
                  <Typography
                    variant="h5"
                    sx={{
                      color: "white",
                      textTransform: "uppercase",
                      textAlign: "center",
                      py: 1,
                      borderBottom: "1px dashed white",
                    }}
                  >
                    Hóa đơn của bạn
                  </Typography>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "white",
                    }}
                  >
                    {`Họ tên: ${
                      JSON.parse(localStorage.getItem("user")).firstName
                    } ${JSON.parse(localStorage.getItem("user")).lastName}`}
                  </Typography>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "white",
                    }}
                  >
                    {`Ngày đặt: ${
                      JSON.parse(localStorage.getItem("receipt")).timeBook
                    }`}
                  </Typography>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "white",
                    }}
                  >
                    {`Checkin: ${
                      JSON.parse(localStorage.getItem("receipt")).checkIn
                    }`}
                  </Typography>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "white",
                    }}
                  >
                    {`CheckOut: ${
                      JSON.parse(localStorage.getItem("receipt")).checkOut
                    }`}
                  </Typography>
                  <Typography
                    variant="h6"
                    sx={{
                      color: "white",
                    }}
                  >
                    {
                     convertMoney( JSON.parse(localStorage.getItem("receipt")).totalAmount *
                     1.1)
                    }
                  </Typography>
                </Box>
              </Grid>
              <Grid item xs={12} lg={6}>
                {paymentStatus ? (
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Box
                      component="img"
                      sx={{ width: "200px" }}
                      alt="The house from the offer."
                      src={images.successPay.logo}
                    />
                    <Typography
                      variant="h5"
                      sx={{
                        color: "white",
                        textTransform: "uppercase",
                        textAlign: "center",
                      }}
                    >
                      Chúc bạn có một trải nghiệm tuyệt vời cùng G53
                    </Typography>
                  </Box>
                ) : (
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                      gap: 2,
                    }}
                  >
                    <CircularProgress />
                    <Typography
                      variant="h5"
                      sx={{
                        color: "white",
                        textTransform: "uppercase",
                        textAlign: "center",
                      }}
                    >
                      Chúng tôi đang kiểm tra giao dịch, chờ chút nhé...
                    </Typography>
                  </Box>
                )}
              </Grid>
              {paymentStatus && (
                <Grid item xs={12}>
                  <Box
                    sx={{ display: "flex", gap: 2, justifyContent: "center" }}
                  >
                    <Button
                      sx={{
                        width: "200px",
                        backgroundColor: "#FFA903",
                        fontWeight: "600",
                        color: "#1a2238",
                        my: 3,
                        ":hover": {
                          color: "white",
                          border: "1px dashed white",
                        },
                      }}
                      onClick={() => {
                        navigate("/home");
                      }}
                    >
                      Quay lại trang chủ
                    </Button>
                    <Button
                      sx={{
                        backgroundColor: "#FFA903",
                        fontWeight: "600",
                        color: "#1a2238",
                        my: 3,
                        ":hover": {
                          color: "white",
                          border: "1px dashed white",
                        },
                      }}
                      onClick={() => {
                        navigate("/home");
                      }}
                    >
                      Quay lại đặt phòng
                    </Button>
                  </Box>
                </Grid>
              )}
            </Grid>
          </Box>
        </Box>
      ) : (
        <NotFound />
      )}
    </>
  );
};
export default ThanksPage;
