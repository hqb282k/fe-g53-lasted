import React from "react";
import ReactDOM from "react-dom";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import { SnackBar } from "Components";
import * as yup from "yup";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { setStatusSnackbar } from "Redux/Slices/snackbar";
import { useDispatch, useSelector } from "react-redux";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import Container from "@mui/material/Container";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { alpha, styled } from "@mui/material/styles";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { useParams } from "react-router-dom";

import dayjs from "dayjs";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import images from "Assets";

const backgroundStyle = {
  background: {
    backgroundSize: "cover",
    objectFit: "center",
    positon: "relative",
    width: "100vw",
    height: "100vh",
  },
};

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#57112f",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#57112f",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#57112f",
    },
    "&:hover fieldset": {
      borderColor: "#57112f",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#57112f",
    },
  },
});

const checkStatusCode = (code) => {
  switch (code) {
    case 119: {
      return "Yêu cầu không còn hiệu lực";
      break;
    }
    case 200: {
      return "Đổi mật khẩu thành công";
      break;
    }
    default: {
      return "Lỗi không xác định";
    }
  }
};

const ResetPassword = () => {
  const MySwal = withReactContent(Swal);
  const params = useParams();
  const validationSchema = yup.object({
    password: yup
      .string("Enter your password")
      .min(6, "Mật khẩu tối thiểu 6 ký tự")
      .required("Vui lòng nhập mật khẩu mới"),
    rePassword: yup
      .string("Enter again your re-password")
      .min(6, "Mật khẩu tối thiểu 6 ký tự")
      .required("Vui lòng nhập lại mật khẩu mới"),
  });
  const [birthDate, setBirthDate] = React.useState(dayjs());
  const [gender, setGender] = React.useState(false);

  const handleChangeBirthDate = (newValue) => {
    setBirthDate(newValue);
  };
  const handleChangeGender = (newValue) => {
    setGender(newValue);
  };

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      password: "",
      rePassword: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (values.rePassword != values.password) {
        Swal.fire({
          icon: "warning",
          title: "Lỗi",
          text: "Bạn đã xác nhận mật khẩu sai",
        });
      } else {
        dispatch(
          setStatusBackdrop({
            isOpen: true,
          })
        );
        const body = {
          objectRequest: {
            password: values.password,
          },
        };
        axios
          .post(
            process.env.REACT_APP_BASE_URL +
              process.env.REACT_APP_USER_RESET_PASSWORD_API_PATH +
              params.token,
            body
          )
          .then(function (response) {
            if (response.data.code == 200) {
              dispatch(
                setStatusBackdrop({
                  isOpen: false,
                })
              );
              Swal.fire({
                icon: "success",
                title: `${checkStatusCode(response.data.code)}`,
                text: "Hãy đăng nhập lại nhé <3",
                confirmButtonText: "Đồng ý",
              });
              navigate("/home");
            } else {
              dispatch(
                setStatusBackdrop({
                  isOpen: false,
                })
              );
              Swal.fire({
                icon: "warning",
                title: `${checkStatusCode(response.data.code)}`,
                text: "Hãy kiểm tra lại nhé !",
                confirmButtonText: "Đồng ý",
              });
            }
          })
          .catch(function (error) {
            dispatch(
              setStatusBackdrop({
                isOpen: false,
              })
            );
            Swal.fire({
              icon: "error",
              title: `${checkStatusCode(error)}`,
              text: "Hãy thông báo với chúng tôi qua số: 0971769799",
            });
          });
      }
    },
  });

  React.useEffect(() => {
    localStorage.clear();
  }, []);

  return (
    <Box style={backgroundStyle.background}>
      <Box
        sx={{
          minWidth: "350px",
          maxWidth: "400px",
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%,-50%)",
        }}
      >
        <Typography
          variant="h5"
          sx={{
            textAlign: "center",
            fontWeight: "bold",
            backgroundColor: "#57112f",
            color: "white",
            py: 2,
          }}
        >
          Đặt lại mật khẩu
        </Typography>
        <Box
          className="login-form"
          sx={{ p: 2, maxWidth: "400px", backgroundColor: "#f5f3f0" }}
        >
          <form onSubmit={formik.handleSubmit}>
            <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
              <CustomTextField
                fullWidth
                id="custom-css-outlined-input"
                name="password"
                label="Mật khẩu"
                type="password"
                value={formik.values.password}
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                onChange={formik.handleChange}
                helperText={formik.touched.password && formik.errors.password}
              />
              <CustomTextField
                fullWidth
                id="rePassword"
                name="rePassword"
                label="Nhập lại mật khẩu"
                type="password"
                value={formik.values.rePassword}
                error={
                  formik.touched.rePassword && Boolean(formik.errors.rePassword)
                }
                onChange={formik.handleChange}
                helperText={
                  formik.touched.rePassword && formik.errors.rePassword
                }
              />
              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                sx={{
                  marginTop: "20px",
                  fontWeight: "600",
                  backgroundColor: "#57112f",
                  ":hover": {
                    backgroundColor: "#FFA903",
                    color: "#57112f",
                  },
                }}
              >
                Xác nhận
              </Button>
            </Box>
          </form>
        </Box>
      </Box>
      <SnackBar></SnackBar>
    </Box>
  );
};

export default ResetPassword;
