import React from "react";
import ReactDOM from "react-dom";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "./card";
import data from "Mocks";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import { SubHeader } from "Components";
import Container from "@mui/material/Container";
import TextField from "@mui/material/TextField";
import Slider from "@mui/material/Slider";
import Checkbox from "@mui/material/Checkbox";
import FormLabel from "@mui/material/FormLabel";
import FormControl from "@mui/material/FormControl";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import { setStatusSnackbar } from "Redux/Slices/snackbar";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import Rating from "@mui/material/Rating";
import { set } from "date-fns";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import IconButton from "@mui/material/IconButton";
import style from "styled-components";
import { useDispatch } from "react-redux";

const InputStyle = style.div`
input {
  width:30px;
  border:none;
}
`;

const PriceRange = ({ price, priceToString, setPrice, setPriceToString }) => {
  const handleChangePrice = (event, newValue) => {
    const cloneNewValue = newValue.slice(0);
    cloneNewValue.map((price, index) => {
      if (price == 0) {
        cloneNewValue[index] = "0 VND";
      } else {
        if (price % 1000000 == 0) {
          cloneNewValue[index] = Math.floor(price / 1000000) + ".000.000 VND";
        } else {
          if (price / 1000000 > 1) {
            cloneNewValue[index] = price / 1000000 + "00.000 VND";
          } else {
            cloneNewValue[index] = Math.floor(price / 100000) + "00.000 VND";
          }
        }
      }
    });
    setPrice(newValue);
    setPriceToString(cloneNewValue);
  };

  return (
    <>
      <Typography
        variant="h6"
        sx={{
          color: "#57112F",
          borderBottom: "1px dashed black",
          py: 2,
          fontWeight: "600",
        }}
      >
        Khoảng Giá
      </Typography>
      <Slider
        getAriaLabel={() => "Temperature range"}
        value={price}
        min={0}
        max={4000000}
        step={200000}
        onChange={handleChangePrice}
        valueLabelDisplay="auto"
      />
      <Typography variant="text" sx={{ color: "#57112F", fontWeight: "600" }}>
        {`Giá: ${priceToString[0]} - ${priceToString[1]}`}
      </Typography>
    </>
  );
};

const BasicRating = ({ rate, setRate }) => {
  return (
    <>
      <Typography
        variant="h6"
        sx={{
          color: "#57112F",
          borderBottom: "1px dashed black",
          mb: 3,
          py: 2,
          fontWeight: "600",
        }}
      >
        Đánh Giá
      </Typography>
      <Rating
        name="simple-controlled"
        value={rate}
        onChange={(event, newValue) => {
          setRate(newValue);
        }}
      />
    </>
  );
};

const Services = ({ roomServices, setRoomServices }) => {
  const handleChangeRoomService = (event) => {
    const cloneArray = roomServices.slice(0);
    const index = cloneArray.findIndex((obj) => obj.name == event.target.name);
    cloneArray[index].selected = event.target.checked;
    setRoomServices((state) => [...cloneArray]);
  };
  return (
    <>
      <Typography
        variant="h6"
        sx={{
          color: "#57112F",
          borderBottom: "1px dashed black",
          mb: 3,
          py: 2,
          textAlign: "center",
          fontWeight: "600",
        }}
      >
        Dịch Vụ
      </Typography>
      <Box sx={{}}>
        {roomServices.map((service, index) => (
          <FormControlLabel
            control={
              <Checkbox
                checked={service.selected}
                onChange={handleChangeRoomService}
                name={service.name}
              />
            }
            label={service.name}
            sx={{ color: "#57112F" }}
          />
        ))}
      </Box>
    </>
  );
};

const Filter = ({ rooms, setRooms }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [filter, setFilter] = React.useState({});
  const [quantityPeople, setQuantityPeople] = React.useState({
    adult: 0,
    kid: 0,
  });
  const [roomServices, setRoomServices] = React.useState([
    {
      name: "Ngoài trời",
      selected: false,
    },
    {
      name: "Vòi sen",
      selected: false,
    },
    {
      name: "Nhà vệ sinh phụ",
      selected: false,
    },
    {
      name: "Máy sấy tóc",
      selected: false,
    },
    {
      name: "Vật dụng tắm rửa",
      selected: false,
    },
    {
      name: "Điện thoại",
      selected: false,
    },
    {
      name: "TV",
      selected: false,
    },
    {
      name: "Wi-Fi",
      selected: false,
    },
    {
      name: "Dép đi trong nhà",
      selected: false,
    },
    {
      name: "Nước rửa tay",
      selected: false,
    },
    {
      name: "Quạt",
      selected: false,
    },
    {
      name: "Đồng hồ báo thức",
      selected: false,
    },
  ]);
  const [rate, setRate] = React.useState(2);
  const [price, setPrice] = React.useState([0, 4000000]);
  const [priceToString, setPriceToString] = React.useState([
    "0",
    "4.000.000 VND",
  ]);

  const [roomsOriginal, setRoomsOriginal] = React.useState(rooms);

  const handleFilterRooms = () => {
    let cloneArray = roomsOriginal.slice(0);
    cloneArray = cloneArray.filter(
      (room) =>
        room.priceDay <= price[1] &&
        room.priceDay >= price[0] &&
        roomServices
          .filter((option) => option.selected == true)
          .map((option) => option.name)
          .every((element) => {
            return room.roomOption
              .map((option) => option.name)
              .includes(element);
          }) &&
        room.avgRate >= rate
    );
    cloneArray.sort(function (currentRoom, nextRoom) {
      return currentRoom.priceDay - nextRoom.priceDay;
    });
    setRooms(cloneArray);
    dispatch(
      setStatusSnackbar({
        isShowing: true,
        title: "Đã lọc theo tiêu chí",
        severity: "success",
      })
    );
  };

  React.useEffect(() => {
    setRoomsOriginal(rooms);
  }, []);

  const handleClearRequire = () => {
    setRoomServices(
      roomServices.map((service) => {
        if (service.selected == true) {
          return { ...service, selected: false };
        }
        return service;
      })
    );
    setPrice([0, 4000000]);
    setPriceToString(["0", "4.000.000 VND"]);
    setRate(1);
    setRooms(roomsOriginal);
    dispatch(
      setStatusSnackbar({
        isShowing: true,
        title: "Đã xóa tiêu chí",
        severity: "info",
      })
    );
  };

  return (
    <Box
      sx={{
        backgroundSize: "cover",
        boxSizing: "border-box",
      }}
    >
      <Typography
        variant="h6"
        sx={{
          color: "white",
          textAlign: "center",
          py: 3,
          fontWeight: "600",
          textTransform: "capitalize",
          backgroundColor: "#57112F",
        }}
      >
        Tiêu chí
      </Typography>
      <Box
        sx={{
          backgroundColor: "white",
          pb: 2,
          textAlign: "center",
        }}
      >
        {/* Price range */}
        <Box sx={{ px: 3, py: 2 }}>
          <PriceRange
            price={price}
            priceToString={priceToString}
            setPrice={setPrice}
            setPriceToString={setPriceToString}
          ></PriceRange>
        </Box>
        {/* Room services */}
        <Box sx={{ borderTop: "3px solid #e8e8e8" }}>
          <Box sx={{ px: 3, textAlign: "left" }}>
            <Services
              roomServices={roomServices}
              setRoomServices={setRoomServices}
            ></Services>
          </Box>
        </Box>
        {/* Rating */}
        <Box sx={{ borderTop: "3px solid #e8e8e8", textAlign: "center" }}>
          <Box sx={{ px: 3 }}>
            <BasicRating rate={rate} setRate={setRate}></BasicRating>
          </Box>
        </Box>
        <Box sx={{ display: "flex", px: 3, py: 3, gap: 2 }}>
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#57112F",
              ":hover": {
                backgroundColor: "#FFA903",
                color: "#57112F",
                opacity: "0.7",
              },
            }}
            onClick={handleFilterRooms}
          >
            Lọc
          </Button>
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#57112F",
              ":hover": {
                backgroundColor: "#FFA903",
                color: "#57112F",
                opacity: "0.7",
              },
            }}
            onClick={handleClearRequire}
          >
            Xóa bỏ tiêu chí
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default Filter;
