import React from "react";
import ReactDOM from "react-dom";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "./card";
import data from "Mocks";
import Typography from "@mui/material/Typography";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import {
  DateTimePicker,
  DateTimePickerTabs,
} from "@mui/x-date-pickers/DateTimePicker";
import LightModeIcon from "@mui/icons-material/LightMode";
import AcUnitIcon from "@mui/icons-material/AcUnit";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import { useNavigate, useParams } from "react-router-dom";
import { SubHeader } from "Components";
import Container from "@mui/material/Container";
import TextField from "@mui/material/TextField";
import RoomServiceIcon from "@mui/icons-material/RoomService";
import HotelIcon from "@mui/icons-material/Hotel";
import Button from "@mui/material/Button";
import StarIcon from "@mui/icons-material/Star";
import { alpha, styled } from "@mui/material/styles";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import axios from "axios";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useTheme } from "@mui/material/styles";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { useMediaQuery } from "react-responsive";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css"; // optional for styling
import style from "styled-components";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import SearchIcon from "@mui/icons-material/Search";
import TodayIcon from "@mui/icons-material/Today";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import { useDispatch, useSelector } from "react-redux";
import { DateRangePicker } from "@mui/x-date-pickers-pro/DateRangePicker";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const InputStyle = style.div`
input {
  width:30px;
  border:none;
}
`;

const TippyStyle = style.div`
  .tippy-box {
    background-color: #1f252e;
    padding: 20px 0;
    max-width: 250px !important;
    border: 1px solid #faa903;
  }
  .tippy-box[data-placement^="bottom"] > .tippy-arrow:before {
    border-bottom-color: #ffa903;
  }
`;

const DateStyle = style.div`
.MuiTextField-root{
  background-color:#f5f3f0 !important;
}

label{
color: white!important;
top:-9px !important;
}
`;

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#57112f",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#57112f",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#57112f",
    },
    "&:hover fieldset": {
      borderColor: "#57112f",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#57112f",
    },
  },
});

const CustomTabs = (props) => (
  <React.Fragment>
    <DateTimePickerTabs {...props} />
    <Box sx={{ backgroundColor: "blueviolet", height: 5 }} />
  </React.Fragment>
);

const PeopleQuantity = ({ numberPassenger, setNumberPassenger }) => {
  return (
    <InputStyle>
      <TippyStyle></TippyStyle>
      <Box sx={{ backgroundColor: "#f5f3f0", color: "#57112F" }}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "",
            alignItems: "center",
            gap: 2,
          }}
        >
          <Typography sx={{ flexGrow: 1 }}>Người lớn</Typography>
          <IconButton
            aria-label="delete"
            onClick={() =>
              setNumberPassenger({
                ...numberPassenger,
                adult: numberPassenger.adult + 1,
              })
            }
          >
            <AddIcon />
          </IconButton>
          <Typography>{numberPassenger.adult}</Typography>
          <IconButton
            aria-label="delete"
            onClick={() => {
              if (numberPassenger.adult - 1 < 0) return;
              setNumberPassenger({
                ...numberPassenger,
                adult: numberPassenger.adult - 1,
              });
            }}
          >
            <RemoveIcon />
          </IconButton>
        </Box>
        <Box sx={{ display: "flex", alignItems: "center", gap: 2 }}>
          <Typography sx={{ flexGrow: 1 }}>Trẻ em</Typography>
          <IconButton
            aria-label="delete"
            onClick={() =>
              setNumberPassenger({
                ...numberPassenger,
                kid: numberPassenger.kid + 1,
              })
            }
          >
            <AddIcon />
          </IconButton>
          <Typography>{numberPassenger.kid}</Typography>
          <IconButton
            aria-label="delete"
            onClick={() => {
              if (numberPassenger.kid - 1 < 0) return;
              setNumberPassenger({
                ...numberPassenger,
                kid: numberPassenger.kid - 1,
              });
            }}
          >
            <RemoveIcon />
          </IconButton>
        </Box>
      </Box>
    </InputStyle>
  );
};

const SubFilter = ({ setRooms }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();
  var customParseFormat = require("dayjs/plugin/customParseFormat");
  dayjs.extend(customParseFormat);
  var localizedFormat = require("dayjs/plugin/localizedFormat");
  dayjs.extend(localizedFormat);
  var isSameOrBefore = require("dayjs/plugin/isSameOrBefore");
  dayjs.extend(isSameOrBefore);
  const MySwal = withReactContent(Swal);
  const [dateRange, setDateRange] = React.useState([dayjs(), dayjs()]);
  const [filterRoom, setFilterRoom] = React.useState({
    checkIn: dayjs(dateRange[0]).format("dd/MM/YYYY"),
    checkOut: dayjs(dateRange[1]).format("dd/MM/YYYY"),
  });

  const [numberPassenger, setNumberPassenger] = React.useState({
    kid: 0,
    adult: 0,
  });

  const isSmallScreen = useMediaQuery({ query: "(max-width: 700px)" });

  const handleGetAllRoomTypes = () => {
    if (
      !dayjs(
        dayjs(dateRange[0]).format("DD-MM-YYYY"),
        "DD-MM-YYYY"
      ).isValid() ||
      !dayjs(dayjs(dateRange[1]).format("DD-MM-YYYY"), "DD-MM-YYYY").isValid()
    ) {
      Swal.fire({
        icon: "warning",
        title: `Khoảng ngày không hợp lệ`,
        text: "Vui lòng kiểm tra lại ngày tháng (DD/MM/YYYY)",
      });
    } else {
      if (!dateRange[0].isSameOrBefore(dateRange[1])) {
        Swal.fire({
          icon: "warning",
          title: `Khoảng ngày không hợp lệ`,
          text: "Thời gian checkout đang ở trước checkin",
        });
      } else {
        dispatch(
          setStatusBackdrop({
            isOpen: true,
          })
        );
        localStorage.setItem(
          "bookingRange",
          JSON.stringify({
            checkIn: dayjs(dateRange[0]).format("DD/MM/YYYY"),
            checkOut: dayjs(dateRange[1]).format("DD/MM/YYYY"),
          })
        );

        const body = {
          checkIn: dateRange[0],
          checkOut: dateRange[1],
          numberPassenger: numberPassenger.kid + numberPassenger.adult,
        };
        axios
          .post(
            process.env.REACT_APP_BASE_URL +
              // "http://localhost:8899" +
              process.env.REACT_APP_ROOM_SEARCH_API_PATH,
            body
          )
          .then(function (response) {
            dispatch(
              setStatusBackdrop({
                isOpen: false,
              })
            );
            console.log(response.data);
            setRooms(response.data.objects);
          })
          .catch(function (error) {
            dispatch(
              setStatusBackdrop({
                isOpen: false,
              })
            );
            console.log(error);
          });
      }
    }
  };

  React.useEffect(() => {
    localStorage.setItem(
      "bookingRange",
      JSON.stringify({
        checkIn: dayjs(dateRange[0]).format("DD/MM/YYYY"),
        checkOut: dayjs(dateRange[1]).format("DD/MM/YYYY"),
      })
    );
  }, []);

  return (
    <Box sx={{ marginTop: "-50px", backgroundColor: "#57112F" }}>
      <Typography
        variant="h6"
        sx={{
          py: 2,
          color: "white",
          fontWeight: "600",
          textTransform: "capitalize",
          textAlign: "center",
        }}
      >
        Danh sách phòng của chúng tôi
      </Typography>
      <Box
        sx={{
          p: 3,
          backgroundSize: "cover",
        }}
      >
        <Grid
          container
          spacing={3}
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Grid item xs={12} sm={6}>
            <DateStyle>
              <LocalizationProvider
                dateAdapter={AdapterDayjs}
                localeText={{ start: "Từ ngày", end: "Đến ngày" }}
              >
                <DateRangePicker
                  inputFormat="DD/MM/YYYY"
                  value={dateRange}
                  onChange={(newValue) => {
                    setDateRange(newValue);
                  }}
                  renderInput={(startProps, endProps) => (
                    <React.Fragment>
                      <CustomTextField {...startProps} />
                      <Box sx={{ mx: 2 }}></Box>
                      <CustomTextField {...endProps} />
                    </React.Fragment>
                  )}
                />
              </LocalizationProvider>
            </DateStyle>
          </Grid>
          <Grid item xs={6} sm={3}>
            {" "}
            <Tippy
              content={
                <PeopleQuantity
                  filterRoom={filterRoom}
                  setFilterRoom={setFilterRoom}
                  numberPassenger={numberPassenger}
                  setNumberPassenger={setNumberPassenger}
                ></PeopleQuantity>
              }
              animation="fade"
              arrow={false}
              theme="tomato"
              trigger="click"
              interactive="true"
              placement="bottom-start"
              appendTo="parent"
            >
              <Button
                variant="contained"
                fullWidth
                sx={{
                  backgroundColor: "#FAA903",
                  color: "black",
                  height: "56px",
                  ":hover": {
                    backgroundColor: "#FAA903",
                    opacity: 0.8,
                  },
                }}
              >
                Số Người
              </Button>
            </Tippy>
          </Grid>
          <Grid item xs={2}>
            <Box
              sx={{
                borderLeft: "1px dashed #FFA903",
                px: 3,
                display: "flex",
              }}
            >
              <IconButton
                aria-label="search"
                sx={{
                  backgroundColor: "#FFA903",
                  p: 3,
                  ":hover": {
                    backgroundColor: "rgba(255,169,3,0.5)",
                  },
                }}
                onClick={handleGetAllRoomTypes}
              >
                <SearchIcon />
              </IconButton>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default SubFilter;
