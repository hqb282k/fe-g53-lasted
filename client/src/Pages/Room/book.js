import React from "react";
import ReactDOM from "react-dom";
import { useFormik } from "formik";
import * as yup from "yup";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import { setStatusSnackbar } from "Redux/Slices/snackbar";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import Typography from "@mui/material/Typography";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import {
  DateTimePicker,
  DateTimePickerTabs,
} from "@mui/x-date-pickers/DateTimePicker";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import Stack from "@mui/material/Stack";
import Grid from "@mui/material/Grid";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import LightModeIcon from "@mui/icons-material/LightMode";
import AcUnitIcon from "@mui/icons-material/AcUnit";
import data from "Mocks";
import Container from "@mui/material/Container";
import { useNavigate, useParams } from "react-router-dom";
import { SubHeader } from "Components";
import Radio from "@mui/material/Radio";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { alpha, styled } from "@mui/material/styles";
import { BasicModal } from "Components";
import { useDispatch } from "react-redux";
import { setStatusModal } from "Redux/Slices/modal";
import images from "Assets";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import axios from "axios";
import Rating from "@mui/material/Rating";
const MySwal = withReactContent(Swal);

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#57112f",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#57112f",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#57112f",
    },
    "&:hover fieldset": {
      borderColor: "#57112f",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#57112f",
    },
  },
});

const CustomTabs = (props) => (
  <React.Fragment>
    <DateTimePickerTabs {...props} />
    <Box sx={{ backgroundColor: "blueviolet", height: 5 }} />
  </React.Fragment>
);

const ConfirmForm = ({ successBooking }) => {
  const dispatch = useDispatch();
  const handleBookRoom = () => {
    dispatch(setStatusModal({ showing: false }));
    successBooking();
  };
  return (
    <Box sx={{ textAlign: "center", p: 5 }}>
      <Typography variant="h5" sx={{ py: 2 }}>
        Bạn đã sẵn sàng đón nhận trải nghiệm từ G53 ?
      </Typography>
      <Box sx={{ display: "flex", gap: 2, justifyContent: "end" }}>
        <Button variant="contained" sx={{ backgroundColor: "#57112F" }}>
          Quay Lại
        </Button>
        <Button
          variant="contained"
          sx={{ backgroundColor: "#57112F" }}
          onClick={handleBookRoom}
        >
          Đồng Ý
        </Button>
      </Box>
    </Box>
  );
};

const BookingForm = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const validationSchema = yup.object({
    customerName: yup
      .string("Nhập tên khách hàng")
      .required("Tên khách hàng không được bỏ trống"),
    identifyNumber: yup
      .string("Nhập Số CMND khách hàng")
      .required("Số CMND khách hàng không được bỏ trống"),
  });
  var localizedFormat = require("dayjs/plugin/localizedFormat");
  dayjs.extend(localizedFormat);
  const params = useParams();
  const [roomSelecting, setRoomSelecting] = React.useState();
  const [filterType, setFilterType] = React.useState("");
  const startDate =
    JSON.parse(localStorage.getItem("receipt")) != null
      ? dayjs(JSON.parse(localStorage.getItem("receipt")).checkIn, "DD/MM/YYYY")
      : null;
  const endDate =
    JSON.parse(localStorage.getItem("receipt")) != null
      ? dayjs(
          JSON.parse(localStorage.getItem("receipt")).checkOut,
          "DD/MM/YYYY"
        )
      : null;
  const [value, setValue] = React.useState("paypal");
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const convertMoney = (price) => {
    let result = "";
    switch (true) {
      case price / 1000000 >= 1: {
        const temp = price / 1000000 + "";
        // For : 1400000
        if (temp.length == 3) {
          result = price / 1000000 + "00.000 VND";
        } else {
          // For : 1450000
          if (temp.length == 4) {
            result = price / 1000000 + "0.000 VND";
          }
          // For : 1456000
          else {
            result = price / 1000000 + ".000 VND";
          }
        }
        break;
      }
      // For : 999000
      case price / 100000 < 10 && price / 1000 < 1000: {
        result = price / 1000 + ".000 VND";
        break;
      }
      // For : 99000
      case price / 10000 < 10 && price / 1000 < 100: {
        result = price / 1000 + ".000 VND";
        break;
      }
      // For : 9000
      case price / 10000 < 1: {
        result = price / 1000 + ".000 VND";
        break;
      }
    }
    return result;
  };

  const formik = useFormik({
    initialValues: {
      roomType: roomSelecting ? roomSelecting.roomTypeName : "",
      roomPrice: roomSelecting ? roomSelecting.priceDay : "",
      customerName:
        JSON.parse(localStorage.getItem("user")) != null
          ? JSON.parse(localStorage.getItem("user")).firstName +
            " " +
            JSON.parse(localStorage.getItem("user")).lastName
          : null,
      identifyNumber:
        JSON.parse(localStorage.getItem("user")) != null
          ? JSON.parse(localStorage.getItem("user")).identityCardNumber
          : null,
      customerNote: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      dispatch(
        setStatusBackdrop({
          isOpen: true,
        })
      );
      const receipt = JSON.parse(localStorage.getItem("receipt"));
      //Chuyen tien te o day
      axios
        .get(
          `https://api.apilayer.com/currency_data/convert?to=USD&from=VND&amount=${receipt.totalAmount}`,
          {
            headers: {
              apikey: process.env.REACT_APP_CURRENCY_API_PATH,
            },
          }
        )
        .then((response) => {
          const body = {
            checkIn: receipt.checkIn,
            checkOut: receipt.checkOut,
            totalAmount: response.data.result,
            timeBook: receipt.timeBook,
            customerId: receipt.customerId,
            staffId: null,
            roomId: receipt.roomId,
            bookOnline: true,
            customerDTO: JSON.parse(localStorage.getItem("user")),
          };
          axios
            .post(
              process.env.REACT_APP_BASE_URL +
                process.env.REACT_APP_ROOM_CREATE_BOOK_API_PATH,
              body
            )
            .then(function (response) {
              dispatch(
                setStatusBackdrop({
                  isOpen: false,
                })
              );
              Swal.fire({
                icon: "success",
                title: "Chuyển hướng",
                text: "Bạn sẽ được chuyển đến trang thanh toán",
              });
              window.location.replace(
                `${response.data.replace("redirect:", "")}`
              );
            })
            .catch(function (error) {
              dispatch(
                setStatusBackdrop({
                  isOpen: false,
                })
              );
              Swal.fire({
                icon: "error",
                title: "Lỗi thanh toán",
                text: "Vui lòng thử lại sau, bạn cũng có thể gọi hotline: 0971769799 để được hỗ trợ gấp",
                confirmButtonText: "Đồng ý",
              });
            });
        })
        .catch((error) => {
          Swal.fire({
            icon: "error",
            title: "Lỗi chuyển đổi tiền tệ",
            text: "Vui lòng thử lại sau, bạn cũng có thể gọi hotline: 0971769799 để được hỗ trợ gấp",
            confirmButtonText: "Đồng ý",
          });
        });
    },
  });

  React.useEffect(() => {
    axios
      .get(
        process.env.REACT_APP_BASE_URL +
          process.env.REACT_APP_ROOM_GET_DETAIL_API_PATH +
          params.id
      )
      .then((response) => {
        console.log(response.data.objects);
        setRoomSelecting(response.data.objects);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <>
      {roomSelecting &&
      JSON.parse(localStorage.getItem("user")) != null &&
      JSON.parse(localStorage.getItem("receipt")) != null ? (
        <>
          <SubHeader title={"Thanh Toán"}></SubHeader>
          <Container maxwidth="xl" sx={{ marginTop: "-50px", pb: 5 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <Box
                  sx={{
                    backgroundColor: "#f5f3f0",
                  }}
                >
                  <Typography
                    variant="h6"
                    gutterBottom
                    sx={{
                      py: 2,
                      background: "#57112F",
                      textTransform: "capitalize",
                      fontWeight: "600",
                      textAlign: "center",
                      color: "white",
                    }}
                  >
                    Thông tin phòng
                  </Typography>
                  <Box sx={{ p: 2 }}>
                    <Typography variant="h6" sx={{ fontWeight: "600" }}>
                      Loại phòng:{" "}
                      <Typography variant="text" sx={{ fontWeight: "200" }}>
                        {roomSelecting.roomTypeName}
                      </Typography>
                    </Typography>
                    <Typography variant="h6" sx={{ fontWeight: "600" }}>
                      Giá:{" "}
                      <Typography variant="text" sx={{ fontWeight: "200" }}>
                        {convertMoney(roomSelecting.priceDay)}/ 1 ngày
                      </Typography>
                    </Typography>
                    <Typography variant="h6" sx={{ fontWeight: "600" }}>
                      Khoảng thời gian đặt:{" "}
                      <Typography variant="text" sx={{ fontWeight: "200" }}>
                        {endDate.diff(startDate, "day") + 1 > 1
                          ? `${startDate.format(
                              "DD/MM/YYYY"
                            )} tới ${endDate.format("DD/MM/YYYY")} `
                          : `trong ngày ${endDate.format("DD/MM/YYYY")}`}
                      </Typography>
                    </Typography>
                    <Typography variant="h6" sx={{ fontWeight: "600" }}>
                      Tạm tính:{" "}
                      <Typography variant="text" sx={{ fontWeight: "200" }}>
                        {endDate.diff(startDate, "day") + 1} ngày
                      </Typography>
                    </Typography>
                  </Box>
                </Box>
                {/* Payment */}
                <Box
                  sx={{
                    backgroundColor: "#f5f3f0",
                    color: "white",
                    textAlign: "center",
                  }}
                >
                  <Typography
                    variant="h6"
                    gutterBottom
                    sx={{
                      py: 2,
                      background: "#57112F",
                      textTransform: "capitalize",
                      fontWeight: "600",
                    }}
                  >
                    Phương thức thanh toán
                  </Typography>
                  <Box sx={{ color: "#57112F", py: 2 }}>
                    <FormControl>
                      <RadioGroup
                        aria-labelledby="demo-controlled-radio-buttons-group"
                        name="controlled-radio-buttons-group"
                        value={value}
                        onChange={handleChange}
                      >
                        {/* <FormControlLabel
                          value="postpaid"
                          control={<Radio />}
                          label="Trả Sau"
                        /> */}
                        <Box sx={{ display: "flex", justifyContent: "center" }}>
                          <FormControlLabel
                            value="paypal"
                            control={<Radio />}
                            label=""
                          />
                          <Box
                            component="img"
                            sx={{
                              width: "30%",
                            }}
                            alt="The house from the offer."
                            src={images.payment.paypalLogo}
                          />
                        </Box>
                        {/* <FormControlLabel
                          value="bank"
                          control={<Radio />}
                          label="Ngân Hàng"
                        /> */}
                      </RadioGroup>
                    </FormControl>
                  </Box>
                </Box>
                {/* Note hotel rule */}
                <Box
                  sx={{
                    backgroundColor: "#f5f3f0",
                    color: "white",
                  }}
                >
                  <Typography
                    variant="h6"
                    gutterBottom
                    sx={{
                      py: 2,
                      background: "#57112F",
                      textTransform: "capitalize",
                      fontWeight: "600",
                      textAlign: "center",
                    }}
                  >
                    Lưu ý đặt phòng
                  </Typography>
                  <Box sx={{ color: "#57112F", py: 2, px: 2 }}>
                    <Typography
                      variant="h6"
                      sx={{
                        textTransform: "capitalize",
                        fontWeight: "600",
                      }}
                    >
                      * Chính sách đảm bảo
                    </Typography>
                    <Typography>
                      - Phòng của quý khách sẽ được giữ khi việc thanh toán
                      thành công
                    </Typography>
                    <Typography
                      variant="h6"
                      sx={{
                        textTransform: "capitalize",
                        fontWeight: "600",
                      }}
                    >
                      * Chính sách hoàn tiền
                    </Typography>
                    - Trên <b>15 ngày</b> trước thời gian đặt: hoàn lại{" "}
                    <b>100%</b>
                    tiền phòng. <br />- Từ <b>7 đến 15 ngày</b>: hoàn lại{" "}
                    <b>60%</b>
                    tiền phòng. <br />
                    - Khác: không hỗ trợ hoàn tiền. <br />
                    <Typography
                      variant="h6"
                      sx={{
                        fontWeight: "600",
                        py: 2,
                      }}
                    >
                      * Quý khách vui lòng kiểm tra nội quy trước khi đặt phòng,
                      rất hân hạnh được đón tiếp quý khách.
                    </Typography>
                  </Box>
                </Box>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Box
                  sx={{
                    backgroundColor: "#f5f3f0",
                    color: "white",
                    textAlign: "center",
                  }}
                >
                  <Typography
                    variant="h6"
                    gutterBottom
                    sx={{
                      py: 2,
                      background: "#57112F",
                      textTransform: "capitalize",
                      fontWeight: "600",
                    }}
                  >
                    Thông tin của bạn
                  </Typography>
                  <Box sx={{ p: 2 }}>
                    <form onSubmit={formik.handleSubmit}>
                      <CustomTextField
                        fullWidth
                        id="custom-css-outlined-input"
                        name="customerName"
                        label="Tên của bạn"
                        disabled
                        value={formik.values.customerName}
                        onChange={formik.handleChange}
                        error={
                          formik.touched.customerName &&
                          Boolean(formik.errors.customerName)
                        }
                        helperText={
                          formik.touched.customerName &&
                          formik.errors.customerName
                        }
                        margin="normal"
                      />
                      <CustomTextField
                        fullWidth
                        id="custom-css-outlined-input"
                        name="identifyNumber"
                        label="Số CMND"
                        disabled
                        value={formik.values.identifyNumber}
                        onChange={formik.handleChange}
                        error={
                          formik.touched.identifyNumber &&
                          Boolean(formik.errors.identifyNumber)
                        }
                        helperText={
                          formik.touched.identifyNumber &&
                          formik.errors.identifyNumber
                        }
                        margin="normal"
                      />
                      <CustomTextField
                        fullWidth
                        color="secondary"
                        id="custom-css-outlined-input"
                        label="Ghi chú"
                        multiline
                        rows={4}
                        defaultValue={formik.values.password}
                        onChange={formik.handleChange}
                        margin="normal"
                      />
                      <Box sx={{ color: "#7e4a65" }}>
                        <Typography
                          variant="h5"
                          sx={{
                            p: 2,
                            borderBottom: "1px dashed black",
                            borderTop: "3px solid #f8f3e7",
                            fontWeight: "700",
                          }}
                        >
                          Thông Tin Hóa Đơn
                        </Typography>

                        {/* list must paid */}
                        <Box sx={{ p: 2 }}>
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "space-between",
                              backgroundColor: "#f0ecec",
                              my: 2,
                              p: 2,
                              fontWeight: "500",
                            }}
                          >
                            <Typography variant="text">Ghi chú</Typography>
                            <Typography variant="text">Tổng</Typography>
                          </Box>
                          {/* must paid 1*/}
                          <Box sx={{ py: 2, borderBottom: "1px solid black" }}>
                            <Box
                              sx={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <Typography variant="text">
                                {`Thời gian: ${
                                  endDate.diff(startDate, "day") + 1
                                } ngày`}
                              </Typography>
                              <Typography variant="text">{`${convertMoney(
                                JSON.parse(localStorage.getItem("receipt"))
                                  .totalAmount
                              )}`}</Typography>
                            </Box>
                            <Box
                              sx={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <Typography variant="text">Thuế(10%):</Typography>
                              <Typography variant="text">{`${convertMoney(
                                JSON.parse(localStorage.getItem("receipt"))
                                  .totalAmount * 0.1
                              )}`}</Typography>
                            </Box>
                          </Box>
                          {/* must paid 1*/}
                          <Box sx={{ py: 2 }}>
                            <Box
                              sx={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              <Typography variant="text">{`Tổng thanh toán:`}</Typography>
                              <Typography variant="text">{`${convertMoney(
                                JSON.parse(localStorage.getItem("receipt"))
                                  .totalAmount +
                                  JSON.parse(localStorage.getItem("receipt"))
                                    .totalAmount *
                                    0.1
                              )}`}</Typography>
                            </Box>
                          </Box>
                          <Button
                            variant="contained"
                            sx={{
                              backgroundColor: "#57112F",
                              ":hover": {
                                backgroundColor: "#FFA903",
                                color: "#57112F",
                                opacity: "0.7",
                              },
                            }}
                            type="submit"
                          >
                            Thanh Toán
                          </Button>
                        </Box>
                      </Box>
                    </form>
                  </Box>
                </Box>
              </Grid>
            </Grid>
          </Container>
        </>
      ) : (
        <Box sx={{ height: "268px" }}>
          <Container maxwidth="xl">
            <Typography
              variant="h4"
              sx={{
                textAlign: "center",
                backgroundColor: "rgba(255,255,255,0.8)",
                width: "90%",
                margin: "0 auto",
                py: 3,
                my: 3,
              }}
            >
              {`${
                JSON.parse(localStorage.getItem("user")) != null
                  ? " Đang có lỗi hãy thử truy cập lại sau nhé"
                  : "Vui lòng đăng nhập trước khi sử dụng"
              }`}
            </Typography>
          </Container>
        </Box>
      )}
    </>
  );
};

export default BookingForm;
