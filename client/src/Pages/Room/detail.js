import React from "react";
import ReactDOM from "react-dom";
import * as yup from "yup";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { useParams } from "react-router-dom";
import data from "Mocks";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import { useNavigate } from "react-router-dom";
import Container from "@mui/material/Container";
import styled from "styled-components";
import { Swiper, SwiperSlide } from "swiper/react";
import { SubHeader } from "Components";
import SubFilter from "./subFilter";
import StarIcon from "@mui/icons-material/Star";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { useFormik } from "formik";
import TextField from "@mui/material/TextField";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import BathtubIcon from "@mui/icons-material/Bathtub";
import HotelIcon from "@mui/icons-material/Hotel";
import Rating from "@mui/material/Rating";
import axios from "axios";
import "swiper/css/pagination";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/navigation";
import "swiper/css/thumbs";
import "@splidejs/react-splide/css";
// or other themes
import "@splidejs/react-splide/css/skyblue";
import "@splidejs/react-splide/css/sea-green";
// or only core styles
import "@splidejs/react-splide/css/core";
// import required modules
import { FreeMode, Navigation, Thumbs, Mousewheel, Pagination } from "swiper";
import Swal from "sweetalert2";
import dayjs from "dayjs";
import images from "Assets";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import OutdoorGrillIcon from "@mui/icons-material/OutdoorGrill";
import BalconyIcon from "@mui/icons-material/Balcony";
import BathroomIcon from "@mui/icons-material/Bathroom";
import HvacIcon from "@mui/icons-material/Hvac";
import {
  faCoffee,
  faClock,
  faWindowRestore,
  faShoePrints,
  faShower,
} from "@fortawesome/free-solid-svg-icons";
import WifiIcon from "@mui/icons-material/Wifi";
import TvIcon from "@mui/icons-material/Tv";
import WashIcon from "@mui/icons-material/Wash";
import WcIcon from "@mui/icons-material/Wc";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import PersonalVideoIcon from "@mui/icons-material/PersonalVideo";
import AlarmIcon from "@mui/icons-material/Alarm";
import ShowerIcon from "@mui/icons-material/Shower";
import ComputerIcon from "@mui/icons-material/Computer";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import KitchenIcon from "@mui/icons-material/Kitchen";
import TableBarIcon from "@mui/icons-material/TableBar";
import BedIcon from "@mui/icons-material/Bed";
import AirlineSeatFlatIcon from "@mui/icons-material/AirlineSeatFlat";
import FamilyRestroomIcon from "@mui/icons-material/FamilyRestroom";
import DryIcon from "@mui/icons-material/Dry";
import HotTubIcon from "@mui/icons-material/HotTub";
import PersonIcon from "@mui/icons-material/Person";
import PeopleIcon from "@mui/icons-material/People";
import { useDispatch, useSelector } from "react-redux";
import { setStatusBackdrop } from "Redux/Slices/backdrop";

const checkRoomServices = (name) => {
  switch (name.toLowerCase()) {
    // Dac trung phong
    case "ban công/sân hiên": {
      return <BalconyIcon />;
      break;
    }
    case "hướng phòng: ngoài trời": {
      return <OutdoorGrillIcon />;
      break;
    }
    case "giường đôi": {
      return <BedIcon />;
      break;
    }
    case "giường đơn": {
      return <AirlineSeatFlatIcon />;
      break;
    }
    case "kiểu phòng: phòng gia đình": {
      return <FamilyRestroomIcon />;
      break;
    }
    case "kiểu phòng: phòng đơn": {
      return <PersonIcon />;
      break;
    }
    case "kiểu phòng: phòng đôi": {
      return <PeopleIcon />;
      break;
    }
    // Noi that
    case "cửa sổ": {
      return <FontAwesomeIcon icon={faWindowRestore} />;
      break;
    }
    case "bàn, ghế": {
      return <TableBarIcon />;
      break;
    }
    // Phong tam va vat dung phong tam
    case "vòi sen": {
      return <FontAwesomeIcon icon={faShower} />;
      break;
    }
    case "bồn tắm": {
      return <HotTubIcon />;
      break;
    }
    case "vật dụng tắm rửa": {
      return <ShowerIcon />;
      break;
    }
    case "nhà vệ sinh phụ": {
      return <WcIcon />;
      break;
    }
    case "máy sấy tóc": {
      return <FontAwesomeIcon icon={faCoffee} />;
      break;
    }
    case "phòng tắm đứng": {
      return <BathroomIcon />;
      break;
    }
    case "máy sấy tay": {
      return <DryIcon />;
      break;
    }
    // Giải Trí
    case "máy tính": {
      return <ComputerIcon />;
      break;
    }
    case "tv": {
      return <TvIcon />;
      break;
    }
    case "tv [màn hình phẳng]": {
      return <TvIcon />;
      break;
    }
    // TIen nghi
    case "điều hòa": {
      return <HvacIcon />;
      break;
    }
    case "wi-fi miễn phí trong tất cả các phòng!": {
      return <WifiIcon />;
      break;
    }
    case "wi-fi": {
      return <WifiIcon />;
      break;
    }
    case "quạt": {
      return <FontAwesomeIcon icon={faClock} />;
      break;
    }
    case "đồng hồ báo thức": {
      return <FontAwesomeIcon icon={faClock} />;
      break;
    }
    case "nước rửa tay": {
      return <WashIcon />;
      break;
    }
    case "dịch vụ báo thức": {
      return <AlarmIcon />;
      break;
    }
    case "dép đi trong nhà": {
      return <FontAwesomeIcon icon={faShoePrints} />;
      break;
    }
    case "điện thoại": {
      return <LocalPhoneIcon />;
      break;
    }
    case "tủ lạnh": {
      return <KitchenIcon />;
      break;
    }
    default: {
      return <HelpOutlineIcon />;
    }
  }
};

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#57112f",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#57112f",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#57112f",
    },
    "&:hover fieldset": {
      borderColor: "#57112f",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#57112f",
    },
  },
});

const RemovePaddingSlide = styled.div`
  .splide {
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 20px !important;
    padding-bottom: 50px;
  }
`;

const ArrowSwiperStyle = styled.div`
  .swiper-button-next,
  .swiper-button-prev {
    color: #57112f;
  }
`;

const Review = ({ review }) => {
  const convertTimeStampToDate = (timeStamp)=>{
    const date = new Date(timeStamp);
    return dayjs(date).format("DD/MM/YYYY");
  }

  return (
    <Box
      sx={{
        p: 2,
        display: "flex",
        flexDirection: "column",
        gap: 1,
        backgroundColor: "rgba(87,17,47,0.2)",
        borderBottom: "5px solid white",
      }}
    >
      <Box sx={{ display: "flex", gap: 2 }}>
        <Typography variant="h6">
          {review.customer.firstName + "  " + review.customer.lastName}
        </Typography>
        <Typography
          variant="h6"
          sx={{ borderLeft: "1px dashed #57112F", px: 1 }}
        >{`Đặt ngày: ${convertTimeStampToDate(review.createdDate)}`}</Typography>
      </Box>
      <Rating
        name="half-rating-read"
        defaultValue={review.rate}
        precision={0.1}
        readOnly
      />
      <Typography>{review.comment}</Typography>
    </Box>
  );
};

const validationSchema = yup.object({
  customerName: yup
    .string("Nhập tên khách hàng")
    .required("Tên khách hàng không được bỏ trống"),
  identifyNumber: yup
    .string("Nhập Số CMND khách hàng")
    .required("Số CMND khách hàng không được bỏ trống"),
});

const BasicRating = () => {
  const [value, setValue] = React.useState(0);
  return (
    <Box
      sx={{
        "& > legend": { mt: 2 },
      }}
    >
      <Rating
        name="simple-controlled"
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
    </Box>
  );
};

const RoomDetail = () => {
  const params = useParams();
  const navigate = useNavigate();
  const [thumbsSwiper, setThumbsSwiper] = React.useState(null);
  const [roomSelecting, setRoomSelecting] = React.useState();
  const dispatch = useDispatch();

  const convertMoney = (price) => {
    let result = "";
    switch (true) {
      case price / 1000000 >= 1: {
        const temp = price / 1000000 + "";
        // For : 1400000
        if (temp.length == 3) {
          result = price / 1000000 + "00.000 VND";
        } else {
          // For : 1450000
          if (temp.length == 4) {
            result = price / 1000000 + "0.000 VND";
          }
          // For : 1456000
          else {
            result = price / 1000000 + ".000 VND";
          }
        }
        break;
      }
      // For : 999000
      case price / 100000 < 10 && price / 1000 < 1000: {
        result = price / 1000 + ".000 VND";
        break;
      }
      // For : 99000
      case price / 10000 < 10 && price / 1000 < 100: {
        result = price / 1000 + ".000 VND";
        break;
      }
      // For : 9000
      case price / 10000 < 1: {
        result = price / 1000 + ".000 VND";
        break;
      }
    }
    return result;
  };

  const handleBookRoom = () => {
    //Check user is login or not if yes go to form of booking, if no show alert make user must login
    if (localStorage.getItem("token") != null) {
      const startDate = dayjs(
        JSON.parse(localStorage.getItem("bookingRange")).checkIn,
        "DD/MM/YYYY"
      );
      const endDate = dayjs(
        JSON.parse(localStorage.getItem("bookingRange")).checkOut,
        "DD/MM/YYYY"
      );
      const receipt = {
        bookOnline: true,
        checkIn: startDate.format("DD/MM/YYYY"),
        checkOut: endDate.format("DD/MM/YYYY"),
        customerId: JSON.parse(localStorage.getItem("user")).id,
        roomId: params.id,
        timeBook: dayjs().format("DD-MM-YYYY hh:mm a "),
        totalAmount:
          (dayjs(endDate).diff(dayjs(startDate), "day") + 1) *
          roomSelecting.priceDay,
      };
      localStorage.setItem("receipt", JSON.stringify(receipt));
      window.scrollTo(0, 0);
      navigate(`book`);
    } else {
      Swal.fire({
        title: "Hãy đăng nhập để đặt phòng ?",
        text: "Nếu chưa có tài khoản, hãy tạo ngay",
        icon: "warning",
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        cancelButtonText: "Tạo tài khoản",
        confirmButtonText: "Đồng ý",
        allowOutsideClick: false,
        customerDTO: JSON.parse(localStorage.getItem("bookingRange")).user,
      }).then((result) => {
        if (result.isDismissed) {
          // If no go to form of register
          navigate(`/register`);
        }
      });
    }
  };

  React.useEffect(() => {
    dispatch(
      setStatusBackdrop({
        isOpen: true,
      })
    );
    axios
      .get(
        process.env.REACT_APP_BASE_URL +
          process.env.REACT_APP_ROOM_GET_DETAIL_API_PATH +
          params.id
      )
      .then((response) => {
        dispatch(
          setStatusBackdrop({
            isOpen: false,
          })
        );
        console.log(response.data.objects);
        setRoomSelecting(response.data.objects);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <>
      {roomSelecting ? (
        <>
          {" "}
          <SubHeader title={"Thông Tin Phòng"}></SubHeader>
          <Container maxwidth="xl">
            {/* Room Detail */}
            <Box
              sx={{
                marginTop: "-50px",
                backgroundColor: "#fffff9",
                boxShadow:
                  "rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px",
              }}
            >
              {/* Room Title */}
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: 2,
                  p: 2,
                  alignItems: "center",
                  color: "white",
                  backgroundColor: "#57112F",
                }}
              >
                <Typography variant="h4" sx={{ textTransform: "capitalize" }}>
                  {roomSelecting.roomTypeName}
                </Typography>
                <Rating
                  name="half-rating-read"
                  defaultValue={roomSelecting.avgRate}
                  precision={0.1}
                  readOnly
                />
                <Typography variant="text">{`${roomSelecting.rateRequests.length} bình luận`}</Typography>
              </Box>
              <Grid container spacing={2}>
                {/* image */}
                <Grid item xs={12} md={5}>
                  {/* Show image */}
                  <ArrowSwiperStyle>
                    <Box sx={{ backgroundColor: "#fffff9", p: 2 }}>
                      <Swiper
                        style={{
                          "--swiper-navigation-color": "#fff",
                          "--swiper-pagination-color": "#fff",
                        }}
                        spaceBetween={10}
                        navigation={true}
                        thumbs={{
                          swiper:
                            thumbsSwiper && !thumbsSwiper.destroyed
                              ? thumbsSwiper
                              : null,
                        }}
                        modules={[FreeMode, Navigation, Thumbs]}
                        className="mySwiper2"
                      >
                        {roomSelecting.photos.map((image) => (
                          <SwiperSlide>
                            <Box
                              component="img"
                              sx={{
                                width: "100%",
                              }}
                              alt="The house from the offer."
                              src={`${
                                image.url
                                  ? image.url
                                  : "https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX24249284.jpg"
                              }`}
                            />
                          </SwiperSlide>
                        ))}
                      </Swiper>
                      {/* Garley menu */}
                      <Swiper
                        onSwiper={setThumbsSwiper}
                        spaceBetween={10}
                        slidesPerView={3}
                        freeMode={true}
                        watchSlidesProgress={true}
                        modules={[FreeMode, Navigation, Thumbs]}
                        className="mySwiper"
                      >
                        {roomSelecting.photos.map((image) => (
                          <SwiperSlide>
                            <Box
                              component="img"
                              sx={{
                                width: "100%",
                              }}
                              alt="Anh khach san"
                              src={`${
                                image.url
                                  ? image.url
                                  : `${images.room.undefinedRoom}`
                              }`}
                            />
                          </SwiperSlide>
                        ))}
                      </Swiper>
                    </Box>
                  </ArrowSwiperStyle>
                </Grid>
                {/* image */}
                <Grid item xs={12} md={7}>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      height: "100%",
                      boxSizing: "border-box",
                      p: 5,
                    }}
                  >
                    <Box
                      sx={{
                        display: "flex",
                        gap: 2,
                        flexDirection: "column",
                        color: "#57112F",
                      }}
                    >
                      <Typography
                        variant="h5"
                        sx={{ fontWeight: "600", textTransform: "capitalize" }}
                      >
                        {roomSelecting.roomName}
                      </Typography>
                      <Typography
                        variant="text"
                        sx={{ color: "rgba(87,17,47,0.7)" }}
                      >
                        {roomSelecting.describe}
                      </Typography>
                      {/* Detail room with scroll */}
                      <Box
                        sx={{
                          width: "60px",
                          height: "3px",
                          backgroundColor: "#57112F",
                        }}
                      ></Box>
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "column",
                          gap: 2,
                          overflow: "auto",
                          maxHeight: "280px",
                        }}
                      >
                        <Box className="traditionalRoom_services">
                          <Typography
                            variant="h7"
                            sx={{
                              textTransform: "capitalize",
                              fontWeight: "500",
                              width: "200px",
                              borderRadius: "5px",
                              backgroundColor: "#57112F",
                              color: "white",
                              px: 1,
                            }}
                          >
                            Đặc trưng phòng
                          </Typography>
                          {roomSelecting.roomOption
                            .filter(
                              (option) => option.type === "Điểm đặc trưng"
                            )
                            .map((option) => (
                              <Typography
                                variant="text"
                                sx={{
                                  alignItems: "center",
                                  display: "flex",
                                  gap: 2,
                                }}
                              >
                                {checkRoomServices(option.name)}
                                {`${option.name}`}
                              </Typography>
                            ))}
                        </Box>
                        <Box className="layout_services">
                          <Typography
                            variant="h7"
                            sx={{
                              textTransform: "capitalize",
                              fontWeight: "500",
                              width: "200px",
                              borderRadius: "5px",
                              backgroundColor: "#57112F",
                              color: "white",
                              px: 1,
                            }}
                          >
                            Nội thất
                          </Typography>
                          {roomSelecting.roomOption
                            .filter(
                              (option) => option.type === "Bố trí và nội thất"
                            )
                            .map((option) => (
                              <Typography
                                variant="text"
                                sx={{
                                  alignItems: "center",
                                  display: "flex",
                                  gap: 2,
                                }}
                              >
                                {checkRoomServices(option.name)}
                                {`${option.name}`}
                              </Typography>
                            ))}
                        </Box>
                        <Box className="bathroom_services">
                          <Typography
                            variant="h7"
                            sx={{
                              textTransform: "capitalize",
                              fontWeight: "500",
                              width: "200px",
                              borderRadius: "5px",
                              backgroundColor: "#57112F",
                              color: "white",
                              px: 2,
                              mb: 1,
                            }}
                          >
                            Phòng tắm
                          </Typography>
                          {roomSelecting.roomOption
                            .filter(
                              (option) =>
                                option.type ===
                                "Phòng tắm và vật dụng phòng tắm"
                            )
                            .map((option) => (
                              <Typography
                                variant="text"
                                sx={{
                                  alignItems: "center",
                                  display: "flex",
                                  gap: 2,
                                }}
                              >
                                {checkRoomServices(option.name)}
                                {`${option.name}`}
                              </Typography>
                            ))}
                        </Box>
                        <Box className="entertainment_services">
                          <Typography
                            variant="h7"
                            sx={{
                              textTransform: "capitalize",
                              fontWeight: "500",
                              width: "200px",
                              borderRadius: "5px",
                              backgroundColor: "#57112F",
                              color: "white",
                              px: 2,
                              mb: 1,
                            }}
                          >
                            Giải trí
                          </Typography>
                          {roomSelecting.roomOption
                            .filter((option) => option.type === "Giải trí")
                            .map((option) => (
                              <Typography
                                variant="text"
                                sx={{
                                  alignItems: "center",
                                  display: "flex",
                                  gap: 2,
                                }}
                              >
                                {checkRoomServices(option.name)}
                                {`${option.name}`}
                              </Typography>
                            ))}
                        </Box>
                        <Box className="security_services">
                          <Typography
                            variant="h7"
                            sx={{
                              textTransform: "capitalize",
                              fontWeight: "500",
                              width: "200px",
                              borderRadius: "5px",
                              backgroundColor: "#57112F",
                              color: "white",
                              px: 2,
                              mb: 1,
                            }}
                          >
                            Tiện nghi
                          </Typography>
                          {roomSelecting.roomOption
                            .filter((option) => option.type === "Tiện nghi")
                            .map((option) => (
                              <Typography
                                variant="text"
                                sx={{
                                  alignItems: "center",
                                  display: "flex",
                                  gap: 2,
                                  width: "200px",
                                }}
                              >
                                {checkRoomServices(option.name)}
                                {`${option.name}`}
                              </Typography>
                            ))}
                        </Box>
                      </Box>
                      <Typography variant="h6" sx={{ fontWeight: 600 }}>
                        Giá:
                        <Typography variant="text" sx={{ fontWeight: 400 }}>
                          {` ${convertMoney(roomSelecting.priceDay)}`} / ngày
                        </Typography>
                      </Typography>
                      <Button
                        variant="contained"
                        sx={{
                          backgroundColor: "#57112F",
                          width: "150px",
                          ":hover": {
                            backgroundColor: "#FFA903",
                            color: "#57112F",
                            opacity: "0.7",
                          },
                        }}
                        onClick={handleBookRoom}
                      >
                        Đặt phòng
                      </Button>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Box>
            {/* Room Review */}
            <Box sx={{ py: 2 }}>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <Box sx={{ backgroundColor: "#fffff9" }}>
                    <Typography
                      variant="h6"
                      sx={{
                        textAlign: "center",
                        py: 2,
                        color: "white",
                        backgroundColor: "#57112F",
                        textTransform: "capitalize",
                        fontWeight: "600",
                      }}
                    >
                      Đánh giá từ khách hàng
                    </Typography>
                    <RemovePaddingSlide>
                      <Splide
                        options={{
                          classes: {
                            pagination:
                              "splide__pagination your-class-pagination",
                            page: "splide__pagination__page your-class-page",
                          },
                          direction: "ttb",
                          paginationDirection: "ltr",
                          perPage: 4,
                          arrows: false,
                          wheel: true,
                          rewind: false,
                          items: "button",
                          gap: 10,
                          autoplay: true,
                          autoHeight: true,
                          height: "34rem",
                        }}
                        aria-label="My Favorite Images"
                      >
                        {roomSelecting.rateRequests.map((review) => (
                          <SplideSlide>
                            <Review review={review}></Review>
                          </SplideSlide>
                        ))}
                      </Splide>
                    </RemovePaddingSlide>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </>
      ) : (
        <Box sx={{ height: "268px" }}>
          <Container maxwidth="xl">
            <Typography
              variant="h4"
              sx={{
                textAlign: "center",
                backgroundColor: "rgba(255,255,255,0.8)",
                width: "90%",
                margin: "0 auto",
                py: 3,
                my: 3,
              }}
            >
              {" "}
              Đang có lỗi hãy thử truy cập lại sau nhé{" "}
            </Typography>
          </Container>
        </Box>
      )}
    </>
  );
};

export default RoomDetail;
