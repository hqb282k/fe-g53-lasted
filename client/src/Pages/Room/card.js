import * as React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Banner } from "Components";
import StarIcon from "@mui/icons-material/Star";
import WifiIcon from "@mui/icons-material/Wifi";
import LocationCityIcon from "@mui/icons-material/LocationCity";
import BrunchDiningIcon from "@mui/icons-material/BrunchDining";
import ElevatorIcon from "@mui/icons-material/Elevator";
import { useMediaQuery } from "react-responsive";
import { Navigate, useNavigate } from "react-router-dom";
import images from "Assets";
export default function RoomCard({ room }) {
  const isSmallScreen = useMediaQuery({ query: "(max-width: 600px)" });
  const navigate = useNavigate();

  const convertMoney = (price) => {
    let result = "";
    switch (true) {
      case price / 1000000 >= 1: {
        const temp = price / 1000000 + "";
        // For : 1400000
        if (temp.length == 3) {
          result = price / 1000000 + "00.000 VND";
        } else {
          // For : 1450000
          if (temp.length == 4) {
            result = price / 1000000 + "0.000 VND";
          }
          // For : 1456000
          else {
            result = price / 1000000 + ".000 VND";
          }
        }
        break;
      }
      // For : 999000
      case price / 100000 < 10 && price / 1000 < 1000: {
        result = price / 1000 + ".000 VND";
        break;
      }
      // For : 99000
      case price / 10000 < 10 && price / 1000 < 100: {
        result = price / 1000 + ".000 VND";
        break;
      }
      // For : 9000
      case price / 10000 < 1: {
        result = price / 1000 + ".000 VND";
        break;
      }
    }
    return result;
  };

  return (
    <Box sx={{ backgroundColor: "#f5f3f0", color: "#57112F" }}>
      <Box
        sx={{
          display: "flex",
          gap: "20px",
          borderBottom: "1px dashed ",
          p: 2,
        }}
      >
        <Box
          component="img"
          sx={{
            width: "50%",
            maxWidth: "120px",
          }}
          alt="The room image"
          src={
            room?.photos[0]?.url
              ? room.photos[0].url
              : images.room.undefinedRoomsss
          }
        />
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flexGrow: 1,
            gap: `${isSmallScreen ? "10px" : "20px"}`,
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between" }}>
            <Typography variant="h6">{room?.roomTypeName}</Typography>
            {!isSmallScreen && (
              <Typography
                variant="text"
                sx={{
                  display: "flex",
                  alignItems: "center",
                  px: 1,
                  backgroundColor: "#57112F",
                  color: "white",
                  borderRadius: "10px",
                  height: "30px",
                }}
              >
                {room.avgRate}
                <StarIcon></StarIcon>
              </Typography>
            )}
          </Box>
          <Typography variant={`${isSmallScreen ? "text" : "h6"}`}>
            {room.type}
          </Typography>
          {isSmallScreen ? (
            <>
              <Typography
                variant="text"
                sx={{ textTransform: "capitalize" }}
              >{`Giá từ: ${convertMoney(room.priceDay)}`}</Typography>
              <Button
                variant="contained"
                sx={{
                  maxWidth: "150px",
                  backgroundColor: "#57112F",
                  ":hover": {
                    backgroundColor: "#FFA903",
                    color: "#57112F",
                  },
                }}
                onClick={() => {
                  navigate(`${room.id}`);
                  window.scrollTo(0, 0);
                }}
              >
                Xem Thêm
              </Button>
            </>
          ) : (
            <Box sx={{ display: "flex", gap: 2 }}>
              <LocationCityIcon />
              <WifiIcon />
              <BrunchDiningIcon />
              <ElevatorIcon />
            </Box>
          )}
        </Box>
        {!isSmallScreen && (
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: "20px",
              borderLeft: "1px dashed ",
              px: 2,
              alignItems: "center",
              textAlign: "center",
            }}
          >
            <Typography
              variant="h6"
              sx={{ textTransform: "capitalize" }}
            >{`Giá từ:`}</Typography>
            <Typography
              variant="text"
              sx={{ fontWeight: "500" }}
            >{`${convertMoney(room.priceDay)}`}</Typography>
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#57112F",
                ":hover": {
                  backgroundColor: "#FFA903",
                  color: "#57112F",
                  opacity: "0.7",
                },
              }}
              onClick={() => {
                navigate(`${room.id}`);
                // navigate(`205`);
                window.scrollTo(0, 0);
              }}
            >
              Xem Thêm
            </Button>
          </Box>
        )}
      </Box>
    </Box>
  );
}
