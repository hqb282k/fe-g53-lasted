import React from "react";
import ReactDOM from "react-dom";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "./card";
import data from "Mocks";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import { SubHeader } from "Components";
import SubFilter from "./subFilter";
import Filter from "./filter";
import Container from "@mui/material/Container";
import styled from "styled-components";
import axios from "axios";
// Splice react library
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import { useMediaQuery } from "react-responsive";
import dayjs from "dayjs";
import { useDispatch, useSelector } from "react-redux";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import images from "Assets";

const RemovePaddingSlide = styled.div`
  .splide {
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    padding-bottom: 50px;
  }
`;

const ListRoom = ({ rooms }) => {
  const navigate = useNavigate();
  const isMediumScreen = useMediaQuery({ query: "(max-width: 900px)" });
  return (
    <>
      <Box sx={{ textAlign: "center", backgroundColor: "#57112F" }}>
        <Typography
          variant="h6"
          sx={{
            color: "white",
            py: 3,
            fontWeight: "600",
            textTransform: "capitalize",
          }}
        >
          Phòng
        </Typography>
      </Box>
      {rooms != null ? (
        <Box sx={{ backgroundColor: "#fffff9" }}>
          {rooms.length > 0 ? (
            <RemovePaddingSlide>
              <Splide
                options={{
                  classes: {
                    pagination: "splide__pagination your-class-pagination",
                    page: "splide__pagination__page your-class-page",
                  },
                  direction: "ttb",
                  paginationDirection: "ltr",
                  perPage: 3,
                  perMove: 3,
                  arrows: false,
                  wheel: true,
                  rewind: false,
                  items: "button",
                  gap: "0",
                  autoplay: false,
                  height: "650px",
                  breakpoints: {
                    623: {
                      perPage: 3,
                      perMove: 1,
                    },
                    1200: {
                      perPage: 2,
                      perMove: 2,
                    },
                  },
                }}
                aria-label="My Favorite Images"
                
              >
                {rooms.map((room, index) => (
                  <SplideSlide key={index}>
                    <Card room={room}></Card>
                  </SplideSlide>
                ))}
              </Splide>
            </RemovePaddingSlide>
          ) : (
            <Box sx={{ px: 2 }}>
              <Box
                component="img"
                sx={{ width: "100%", height: "100%" }}
                alt="BusyRoom"
                src={images.room.busyRoom}
              />
              <Typography
                variant="h6"
                sx={{
                  py: 3,
                  fontWeight: "600",
                }}
              >
                Hiện tại loại phòng này đang hết, hãy thử tìm phòng với các tiêu
                chí khác nhé !
              </Typography>
            </Box>
          )}
        </Box>
      ) : (
        <Box sx={{ backgroundColor: "white", height: "300px" }}>
          <Typography variant="h5" sx={{ textAlign: "center", py: 2 }}>
            Không có dữ liệu phòng
          </Typography>
        </Box>
      )}
    </>
  );
};

const Room = () => {
  const [rooms, setRooms] = React.useState();
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(
      setStatusBackdrop({
        isOpen: true,
      })
    );
    const body = {
      checkIn: dayjs().format("DD/MM/YYYY"),
      checkOut: dayjs().format("DD/MM/YYYY"),
      numberPassenger: 0,
    };
    axios
      .post(
        process.env.REACT_APP_BASE_URL +
          process.env.REACT_APP_ROOM_SEARCH_API_PATH,
        body
      )
      .then(function (response) {
        console.log(response.data);
        setRooms(response.data.objects);
        dispatch(
          setStatusBackdrop({
            isOpen: false,
          })
        );
      })
      .catch(function (error) {
        console.log(error);
        dispatch(
          setStatusBackdrop({
            isOpen: false,
          })
        );
      });
  }, []);

  return (
    <Box sx={{ pb: 5 }}>
      <SubHeader title={"Danh Sách Phòng"}></SubHeader>
      <Container maxwidth="xl">
        <Box
          sx={{
            boxShadow:
              "rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px",
          }}
        >
          <SubFilter setRooms={setRooms} />
          <Box sx={{ pt: 3 }}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={4}>
                {rooms != null && <Filter rooms={rooms} setRooms={setRooms} />}
              </Grid>
              <Grid item xs={12} md={8}>
                <ListRoom rooms={rooms} />
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </Box>
  );
};

export default Room;
