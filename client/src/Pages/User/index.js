import * as React from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import data from "Mocks";
import TextField, { TextFieldProps } from "@mui/material/TextField";
import Container from "@mui/material/Container";
import styled from "styled-components";
import { BasicModal } from "Components";
import { setStatusModal } from "Redux/Slices/modal";
import { setStatusSnackbar } from "Redux/Slices/snackbar";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { SubHeader } from "Components";
//  Formilk
import { useFormik } from "formik";
import * as yup from "yup";
// Splice react library
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-default.min.css";
import Rating from "@mui/material/Rating";
import { Navigate, useNavigate } from "react-router-dom";
import { NotFound } from "Pages";
import { useMediaQuery } from "react-responsive";

const RemovePaddingSlide = styled.div`
  .splide {
    padding-right: 0;
    padding-left: 0;
  }
`;

const checkRegisterStatusCode = (code) => {
  switch (code) {
    case 200: {
      return "Đăng ký thành công";
      break;
    }
    case 101: {
      return "Tên tài khoản sai định dạng";
      break;
    }
    case 103: {
      return "Mật khẩu sai định dạng (Có ít nhất 1 chữ viết hoa và ký tự đặc biệt)";
      break;
    }
    case 106: {
      return "Tên tài khoản đã được sử dụng trên tài khoản khác";
      break;
    }
    case 107: {
      return "Số điện thoại đã được sử dụng trên tài khoản khác";
      break;
    }
    case 109: {
      return "Email đã được sử dụng trên tài khoản khác";
      break;
    }
    case 110: {
      return "Số CMND đã sử dụng trên tài khoản khác";
      break;
    }
    case 111: {
      return "Số hộ chiếu đã sử dụng trên tài khoản khác";
      break;
    }
    default: {
      return "Lỗi không xác định";
    }
  }
};

const checkChangePasswordStatusCode = (code) => {
  switch (code) {
    case 200: {
      return "Đổi mật khẩu thành công";
      break;
    }
    case 120: {
      return "Nhập sai mật khẩu hiện tại";
      break;
    }
    default: {
      return "Lỗi không xác định";
    }
  }
};

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#57112f",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#57112f",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#57112f",
    },
    "&:hover fieldset": {
      borderColor: "#57112f",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#57112f",
    },
  },
});

const convertMoney = (price) => {
  let result = "";
  switch (true) {
    case price / 1000000 >= 1: {
      const temp = price / 1000000 + "";
      // For : 1400000
      if (temp.length == 3) {
        result = price / 1000000 + "00.000 VND";
      } else {
        // For : 1450000
        if (temp.length == 4) {
          result = price / 1000000 + "0.000 VND";
        }
        // For : 1456000
        else {
          result = price / 1000000 + ".000 VND";
        }
      }
      break;
    }
    // For : 999000
    case price / 100000 < 10 && price / 1000 < 1000: {
      result = price / 1000 + ".000 VND";
      break;
    }
    // For : 99000
    case price / 10000 < 10 && price / 1000 < 100: {
      result = price / 1000 + ".000 VND";
      break;
    }
    // For : 9000
    case price / 10000 < 1: {
      result = price / 1000 + ".000 VND";
      break;
    }
  }
  return result;
};

const BookedCard = ({ setBookedSelecting, booked }) => {
  const dispatch = useDispatch();
  const isMobileScreen = useMediaQuery({ query: "(max-width: 600px)" });
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: `${isMobileScreen ? "column" : "row"}`,
        borderRadius: "10px",
        gap: 2,
        backgroundColor: "rgba(87,17,47,0.2)",
        alignItems: "center",
        p: 2,
      }}
    >
      <Box sx={{ flexGrow: "1" }}>
        <Typography variant="h5" sx={{fontWeight:"600"}}>{booked.room.roomTypeMap.roomTypeName}</Typography>
        <Typography>Đặt ngày: {booked.checkIn}</Typography>
      </Box>
      <Box>
        <Typography>Tổng: {convertMoney(booked.finalPrice)}</Typography>
      </Box>
      <Box>
        {booked.bookingStatus == "DONE" && (
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#57112F",
              ":hover": {
                backgroundColor: "#FFA903",
                color: "#57112F",
              },
            }}
            fullWidth
            disabled={booked.rate == true}
            onClick={() => {
              setBookedSelecting(booked);
              dispatch(setStatusModal({ showing: true }));
            }}
          >
            {booked.rate == true ? "Đã đánh giá" : "  Đánh Giá"}
          </Button>
        )}
        {booked.bookingStatus == "SUCCESS" && (
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#57112F",
              ":hover": {
                backgroundColor: "#FFA903",
                color: "#57112F",
              },
            }}
            fullWidth
            onClick={() => {
              Swal.fire({
                title: "Xác nhận hủy phòng ?",
                text: "Có vẻ như dịch vụ của chúng tôi chưa đáp ứng được quý khách, nếu có bất cứ vấn đề cần cải thiện, quý khách hãy góp ý với G53 nhé ",
                icon: "warning",
                showCancelButton: true,
                cancelButtonText: "Quay lại",
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Đồng ý",
              }).then((result) => {
                if (result.isConfirmed) {
                  const body = {
                    bookingId: booked.id,
                    note: "",
                  };
                  dispatch(
                    setStatusBackdrop({
                      isOpen: true,
                    })
                  );
                  axios
                    .post(
                      process.env.REACT_APP_BASE_URL +
                        process.env.REACT_APP_USER_CANCEL_BOOKED_API_PATH,
                      body
                    )
                    .then((response) => {
                      dispatch(
                        setStatusBackdrop({
                          isOpen: false,
                        })
                      );
                      if (
                        response.data.code == 200 ||
                        response.data.message == "success"
                      ) {
                        Swal.fire(
                          "Đã hủy phòng thành công",
                          "Tiền sẽ đưực hoàn về sớm nhất",
                          "success"
                        );
                      } else {
                        Swal.fire({
                          title: `Lỗi Hủy Phòng`,
                          text: "Hãy thử lại",
                          icon: "warning",
                          showConfirmButton: true,
                          confirmButtonColor: "#3085d6",
                          confirmButtonText: "Đồng ý",
                        });
                      }
                    })
                    .catch((error) => {
                      dispatch(
                        setStatusBackdrop({
                          isOpen: false,
                        })
                      );
                      Swal.fire({
                        title: `Lỗi hủy phòng`,
                        text: "Hãy liên hệ 0971769799",
                        icon: "error",
                        showConfirmButton: true,
                        confirmButtonColor: "#d33",
                        confirmButtonText: "Đồng ý",
                      });
                    });
                }
              });
            }}
          >
            Hủy phòng
          </Button>
        )}
        {booked.bookingStatus == "DRAFT" && (
          <Typography variant="text">Chưa thanh toán</Typography>
        )}
        {booked.bookingStatus == "CANCEL" && (
          <Typography variant="text">Đẫ hủy phòng</Typography>
        )}
      </Box>
    </Box>
  );
};

const AccountInformationForm = () => {
  const dispatch = useDispatch();
  var customParseFormat = require("dayjs/plugin/customParseFormat");
  dayjs.extend(customParseFormat);
  const [birthDate, setBirthDate] = React.useState(
    dayjs(JSON.parse(localStorage.getItem("user")).birthDay, "DD-MM-YYYY")
  );

  const handleChangeBirthDate = (newValue) => {
    setBirthDate(newValue);
  };
  const MySwal = withReactContent(Swal);
  const validationSchema = yup.object({
    firstName: yup.string("Enter your name").required("Vui lòng nhập họ"),
    lastName: yup.string("Enter your last name").required("Vui lòng nhập tên"),
    email: yup
      .string("Nhập email")
      .email("Nhập đúng định dạng Email")
      .required("Không được bỏ trống"),
    phoneNumber: yup
      .string("Enter your phone number")
      .required("Vui lòng nhập số điện thoại của bạn")
      .matches(/^(0[3|5|7|8|9])([\d]{8})$/, "Nhập sai số điện thoại (10 số)"),
    identifyNumber: yup
      .string("Enter your identify number")
      .required("Không được bỏ trống")
      .matches(
        /^([\d]{12})$|^([\d]{9})$/,
        "Số CMND sai định dạng (9 hoặc 12 số)"
      ),
    passportNumber: yup
      .string("Vui lòng nhập số hộ chiếu của bạn")
      .matches(/^([A-Z]{1}[\d]{7})$/, "Số hộ chiếu không đúng"),
  });
  const formik = useFormik({
    initialValues: {
      firstName: JSON.parse(localStorage.getItem("user")).firstName,
      lastName: JSON.parse(localStorage.getItem("user")).lastName,
      identifyNumber: JSON.parse(localStorage.getItem("user"))
        .identityCardNumber,
      passportNumber:
        JSON.parse(localStorage.getItem("user")).passPortNo != null
          ? JSON.parse(localStorage.getItem("user")).passPortNo
          : "",
      address: JSON.parse(localStorage.getItem("user")).address,
      email: JSON.parse(localStorage.getItem("user")).email,
      phoneNumber: JSON.parse(localStorage.getItem("user")).phone,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (
        dayjs(dayjs(birthDate).format("DD/MM/YYYY"), "DD-MM-YYYY").isValid()
      ) {
        Swal.fire({
          title: "Bạn chắc chắn muốn thay đổi ?",
          text: "Đừng lo, bạn có thể thay đổi lại sau",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          cancelButtonText: "Quay lại",
          confirmButtonText: "Đồng ý",
        }).then((result) => {
          if (result.isConfirmed) {
            dispatch(
              setStatusBackdrop({
                isOpen: true,
              })
            );
            const body = {
              objectRequest: {
                isStaff: false,
                customer: {
                  firstName: values.firstName,
                  lastName: values.lastName,
                  isActive: true,
                  email: values.email,
                  phone: values.phoneNumber,
                  address: values.address,
                  gender: JSON.parse(localStorage.getItem("user")).gender,
                  birthDay: dayjs(birthDate).format("DD/MM/YYYY"),
                  identityCardNumber: values.identifyNumber,
                  passPortNo: values.passportNumber,
                  photoLink: null,
                },
              },
            };
            axios
              .post(
                process.env.REACT_APP_BASE_URL +
                  process.env.REACT_APP_USER_UPDATE_API_PATH,
                body,
                {
                  headers: {
                    Authorization:
                      "Bearer " + `${localStorage.getItem("token")}`,
                  },
                }
              )
              .then((response) => {
                dispatch(
                  setStatusBackdrop({
                    isOpen: false,
                  })
                );
                if (response.data.code == 200) {
                  Swal.fire({
                    icon: "success",
                    title: "Cập nhật thành công",
                    text: "Bạn có thể cập nhật thêm sau",
                  });
                  localStorage.setItem(
                    "user",
                    JSON.stringify(response.data.objects.customer)
                  );
                } else {
                  Swal.fire({
                    icon: "warning",
                    title: `${checkRegisterStatusCode(response.data.code)}`,
                    text: "Kiểm tra lại các thông tin",
                  });
                }
              })
              .catch((error) => {
                dispatch(
                  setStatusBackdrop({
                    isOpen: false,
                  })
                );
                Swal.fire({
                  icon: "warning",
                  title: "Lỗi",
                  text: "Hãy thông báo với chúng tôi qua số: 0971769799",
                });
              });
          }
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Ngày sinh không hợp lệ",
          text: "Hãy kiểm tra lại thông tin",
          confirmButtonText: "Đồng ý",
        });
      }
    },
  });

  return (
    <Box sx={{ backgroundColor: "#f5f3f0" }}>
      <Typography
        variant="h6"
        sx={{
          py: 2,
          color: "white",
          fontWeight: "600",
          backgroundColor: "#57112F",
          textAlign: "center",
        }}
      >
        Thông Tin Tài Khoản
      </Typography>
      <Box sx={{ p: 2 }}>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={{ display: "flex", gap: 2, flexDirection: "column" }}>
            <Box sx={{ display: "flex", gap: 2 }}>
              <CustomTextField
                fullWidth
                id="custom-css-outlined-input"
                name="firstName"
                label="Họ"
                type="text"
                value={formik.values.firstName}
                error={
                  formik.touched.firstName && Boolean(formik.errors.firstName)
                }
                onChange={formik.handleChange}
                helperText={formik.touched.firstName && formik.errors.firstName}
              />
              <CustomTextField
                fullWidth
                id="custom-css-outlined-input"
                name="lastName"
                label="Tên"
                type="text"
                value={formik.values.lastName}
                error={
                  formik.touched.lastName && Boolean(formik.errors.lastName)
                }
                onChange={formik.handleChange}
                helperText={formik.touched.lastName && formik.errors.lastName}
              />
            </Box>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DesktopDatePicker
                disableFuture
                label="Ngày Sinh"
                inputFormat="DD/MM/YYYY"
                value={dayjs(birthDate)}
                onChange={handleChangeBirthDate}
                renderInput={(params) => (
                  <CustomTextField fullWidth {...params} />
                )}
              />
            </LocalizationProvider>
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="email"
              label="Email"
              value={formik.values.email}
              onChange={formik.handleChange}
              error={formik.touched.email && Boolean(formik.errors.email)}
              helperText={formik.touched.email && formik.errors.email}
            />
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="phoneNumber"
              label="Số điện thoại"
              value={formik.values.phoneNumber}
              onChange={formik.handleChange}
              error={
                formik.touched.phoneNumber && Boolean(formik.errors.phoneNumber)
              }
              helperText={
                formik.touched.phoneNumber && formik.errors.phoneNumber
              }
            />
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="address"
              label="Địa chỉ"
              type="text"
              value={formik.values.address}
              error={formik.touched.address && Boolean(formik.errors.address)}
              onChange={formik.handleChange}
              helperText={formik.touched.address && formik.errors.address}
            />
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="identifyNumber"
              label="Số CMND/Hộ Chiếu"
              type="text"
              value={formik.values.identifyNumber}
              error={
                formik.touched.identifyNumber &&
                Boolean(formik.errors.identifyNumber)
              }
              onChange={formik.handleChange}
              helperText={
                formik.touched.identifyNumber && formik.errors.identifyNumber
              }
            />{" "}
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="passportNumber"
              label="Hộ Chiếu (Tùy chọn)"
              placeholder="A1231231"
              type="text"
              value={formik.values.passportNumber}
              error={
                formik.touched.passportNumber &&
                Boolean(formik.errors.passportNumber)
              }
              onChange={formik.handleChange}
              helperText={
                formik.touched.passportNumber && formik.errors.passportNumber
              }
            />
            <Button
              color="primary"
              variant="contained"
              type="submit"
              sx={{
                width: "150px",
                backgroundColor: "#57112F",
                ":hover": {
                  backgroundColor: "#FFA903",
                  color: "#57112F",
                },
              }}
            >
              Cập Nhật
            </Button>
          </Box>
        </form>
      </Box>
    </Box>
  );
};

const ChangePasswordForm = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const validationSchema = yup.object({
    oldPassword: yup
      .string("Nhập mật khẩu")
      .min(6, "Mật khẩu tối thiểu 6 ký tự")
      .required("Không được bỏ trống"),
    newPassword: yup
      .string("Nhập mật khẩu")
      .min(6, "Mật khẩu tối thiểu 6 ký tự")
      .required("Không được bỏ trống"),
  });

  const formik = useFormik({
    initialValues: {
      oldPassword: "",
      newPassword: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Gọi api để kiểm tra mật khẩu hiện tại có đúng không
      const body = {
        objectRequest: {
          username: JSON.parse(localStorage.getItem("user")).username,
          oldPass: values.oldPassword,
          newPass: values.newPassword,
        },
      };
      dispatch(
        setStatusBackdrop({
          isOpen: true,
        })
      );
      axios
        .post(
          process.env.REACT_APP_BASE_URL +
            process.env.REACT_APP_USER_CHANGE_PASSWORD_API_PATH,
          body
        )
        .then((response) => {
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          if (response.data.code == 200) {
            Swal.fire({
              title: `${checkChangePasswordStatusCode(response.data.code)}`,
              text: "Bạn sẽ phải đăng nhập lại",
              icon: "success",
              showConfirmButton: true,
              confirmButtonColor: "#3085d6",
              confirmButtonText: "Đồng ý",
            });
            localStorage.clear();
            navigate("/home");
          } else {
            Swal.fire({
              title: `${checkChangePasswordStatusCode(response.data.code)}`,
              text: "Hãy thử lại",
              icon: "warning",
              showConfirmButton: true,
              confirmButtonColor: "#3085d6",
              confirmButtonText: "Đồng ý",
            });
          }
        })
        .catch((error) => {
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          Swal.fire({
            title: `${checkChangePasswordStatusCode(error.response.status)}`,
            text: "Hãy liên hệ 0971769799",
            icon: "error",
            showConfirmButton: true,
            confirmButtonColor: "#d33",
            confirmButtonText: "Đồng ý",
          });
        });
    },
  });
  return (
    <Box sx={{ mt: 2, backgroundColor: "#f5f3f0" }}>
      <Box sx={{ textAlign: "center", backgroundColor: "#57112F" }}>
        <Typography
          variant="h6"
          sx={{ py: 2, color: "white", fontWeight: "600" }}
        >
          Đổi Mật Khẩu
        </Typography>
      </Box>
      <Box sx={{ p: 2 }}>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={{ display: "flex", gap: 2, flexDirection: "column" }}>
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="oldPassword"
              label="Mật khẩu hiện tại"
              value={formik.values.oldPassword}
              type="password"
              onChange={formik.handleChange}
              error={
                formik.touched.oldPassword && Boolean(formik.errors.oldPassword)
              }
              helperText={
                formik.touched.oldPassword && formik.errors.oldPassword
              }
            />
            <CustomTextField
              fullWidth
              id="custom-css-outlined-input"
              name="newPassword"
              label="Mật khẩu mới"
              type="password"
              value={formik.values.newPassword}
              onChange={formik.handleChange}
              error={
                formik.touched.newPassword && Boolean(formik.errors.newPassword)
              }
              helperText={
                formik.touched.newPassword && formik.errors.newPassword
              }
            />
            <Button
              color="primary"
              variant="contained"
              fullWidth
              type="submit"
              sx={{
                backgroundColor: "#57112F",
                width: "150px",
                ":hover": {
                  backgroundColor: "#FFA903",
                  color: "#57112F",
                },
              }}
            >
              Xác nhận
            </Button>
          </Box>
        </form>
      </Box>
    </Box>
  );
};

const BookedHistoryForm = ({ setBookedSelecting, bookeds }) => {
  return (
    <>
      {bookeds ? (
        <Box sx={{ backgroundColor: "#57112F" }}>
          <Typography
            variant="h6"
            sx={{
              color: "white",
              py: 2,
              textAlign: "center",
              fontWeight: "600",
            }}
          >
            Lịch Sử Đặt Phòng
          </Typography>
          <RemovePaddingSlide>
            <Box sx={{ backgroundColor: "#f5f3f0", px: 2 }}>
              <Splide
                options={{
                  rewind: false,
                  perPage: 5,
                  perMove: 5,
                  pagination: false,
                  direction: "ttb",
                  height: "37rem",
                  autoHeight: true,
                  gap: 20,
                  breakpoints: {
                    600: {
                      perPage: 3,
                      perMove: 3,
                      height: "34rem",
                    },
                    601: {
                      perPage: 4,
                      perMove: 4,
                      height: "38rem",
                    },
                    1200: {
                      perPage: 5,
                      perMove: 5,
                    },
                  },
                }}
                aria-label="My Favorite Images"
              >
                {bookeds
                  .filter((booked) => booked.bookingStatus!=="DRAFT")
                  .map((booked, index) => (
                    <SplideSlide key={index}>
                      <BookedCard
                        setBookedSelecting={setBookedSelecting}
                        booked={booked}
                      ></BookedCard>
                    </SplideSlide>
                  ))}
              </Splide>
            </Box>
          </RemovePaddingSlide>
        </Box>
      ) : (
        <Box sx={{ backgroundColor: "#57112F" }}>
          <Typography
            variant="h6"
            sx={{
              color: "white",
              py: 2,
              textAlign: "center",
              fontWeight: "600",
            }}
          >
            Lịch Sử Đặt Phòng
          </Typography>
          <Box sx={{ height: "300px", backgroundColor: "#f5f3f0", p: 2 }}>
            <Typography variant="h4" sx={{ py: 2, textAlign: "center" }}>
              Chưa có dữ liệu
            </Typography>
          </Box>
        </Box>
      )}
    </>
  );
};

const ReviewForm = ({ bookedSelecting, bookeds, setBookeds }) => {
  const validationSchema = yup.object({
    reviewContent: yup
      .string("Nhập nội dung đánh giá")
      .required("Không được bỏ trống"),
  });
  const dispatch = useDispatch();
  const [rate, setRate] = React.useState(0);
  const formik = useFormik({
    initialValues: {
      reviewContent: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      dispatch(setStatusModal({ show: false }));
      const body = {
        bookingId: bookedSelecting.id,
        comment: values.reviewContent,
        rate: rate,
      };

      axios
        .post(
          process.env.REACT_APP_BASE_URL +
            process.env.REACT_APP_USER_REVIEW_BOOKED_API_PATH,
          body
        )
        .then(function (response) {
          dispatch(
            setStatusBackdrop({
              isOpen: false,
            })
          );
          const indexNeedUpdate = bookeds.findIndex(
            (booked) => bookedSelecting.id == booked.id
          );
          const cloneBookeds = bookeds.slice(0);
          cloneBookeds[indexNeedUpdate].rate = true;
          setBookeds(cloneBookeds);
          Swal.fire({
            icon: "success",
            title: "Đã gửi đánh giá",
            text: "Cảm ơn vì những đánh giá của bạn, chúng tôi luôn nỗ lực lắng nghe ",
          });
        })
        .catch(function (error) {
          dispatch(setStatusBackdrop({ showing: false }));
          Swal.fire({
            icon: "error",
            title: "Lỗi gửi đánh giá",
            text: "Vui lòng thử lại sau, bạn cũng có thể gọi hotline: 0971769799 để được hỗ trợ gấp",
            confirmButtonText: "Đồng ý",
          });
        });
    },
  });
  return (
    <Box
      sx={{
        height: "100%",
        backgroundColor: "#f5f3f0",
      }}
    >
      <Typography
        variant="h6"
        gutterBottom
        sx={{
          py: 2,
          backgroundColor: "#57112F",
          color: "white",
          textAlign: "center",
          textTransform: "capitalize",
          fontWeight: "600",
        }}
      >
        Đánh giá của bạn
      </Typography>
      <Box sx={{ p: 2 }}>
        <Box
          sx={{
            display: "flex",
            borderRadius: "10px",
            gap: 2,
            backgroundColor: "rgba(87,17,47,0.2)",
            p: 2,
          }}
        >
          <Box sx={{ flexGrow: "1" }}>
            <Typography>
              {bookedSelecting.room.roomTypeMap.roomTypeName}
            </Typography>
            <Typography>Đặt ngày: {bookedSelecting.checkIn}</Typography>
            <Typography>Thanh toán: {bookedSelecting.finalPrice}$</Typography>
          </Box>
        </Box>
        <Box
          sx={{
            "& > legend": { mt: 2 },
            pt: 2,
          }}
          s
        >
          <Rating
            name="simple-controlled"
            value={rate}
            onChange={(event, newValue) => {
              setRate(newValue);
            }}
          />
        </Box>
        <form onSubmit={formik.handleSubmit}>
          <Box sx={{ display: "flex", flexDirection: "column" }}>
            <CustomTextField
              fullWidth
              name="reviewContent"
              color="secondary"
              id="custom-css-outlined-input"
              label="Nội dung"
              multiline
              rows={10}
              value={formik.values.reviewContent}
              onChange={formik.handleChange}
              margin="normal"
              error={
                formik.touched.reviewContent &&
                Boolean(formik.errors.reviewContent)
              }
              helperText={
                formik.touched.reviewContent && formik.errors.reviewContent
              }
              sx={{ flexGrow: "1" }}
            />
            <Button
              variant="contained"
              sx={{
                backgroundColor: "#57112F",
                ":hover": {
                  backgroundColor: "#FFA903",
                  color: "#57112F",
                },
              }}
              fullWidth
              type="submit"
            >
              Gửi Đánh Giá
            </Button>
          </Box>
        </form>
      </Box>
    </Box>
  );
};

const User = () => {
  const [bookedSelecting, setBookedSelecting] = React.useState();
  const [bookeds, setBookeds] = React.useState();
  React.useEffect(() => {
    axios
      .post(
        process.env.REACT_APP_BASE_URL +
          process.env.REACT_APP_USER_GET_BOOKED_API_PATH,
        null,
        {
          headers: {
            Authorization: "Bearer " + `${localStorage.getItem("token")}`,
          },
        }
      )
      .then((response) => {
        console.log(response.data);
        setBookeds(response.data.objects);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return (
    <>
      {localStorage.getItem("user") ? (
        <>
          <SubHeader title={"Tài Khoản"}></SubHeader>
          <Container maxwidth="xl" sx={{ marginTop: "-50px", pb: 5 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={5}>
                <AccountInformationForm></AccountInformationForm>
                <ChangePasswordForm></ChangePasswordForm>
              </Grid>
              <Grid item xs={12} sm={7}>
                <BookedHistoryForm
                  setBookedSelecting={setBookedSelecting}
                  bookeds={bookeds}
                ></BookedHistoryForm>
              </Grid>
            </Grid>
            <BasicModal
              Element={() => (
                <Box sx={{ minWidth: "320px" }}>
                  <ReviewForm
                    bookedSelecting={bookedSelecting}
                    bookeds={bookeds}
                    setBookeds={setBookeds}
                  ></ReviewForm>
                </Box>
              )}
            ></BasicModal>
          </Container>
        </>
      ) : (
        <NotFound></NotFound>
      )}
    </>
  );
};
export default User;
