import React from "react";
import ReactDOM from "react-dom";
import { useFormik } from "formik";
import { useNavigate } from "react-router-dom";
import { SnackBar } from "Components";
import * as yup from "yup";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { setStatusSnackbar } from "Redux/Slices/snackbar";
import { setStatusBackdrop } from "Redux/Slices/backdrop";
import { SubHeader } from "Components";
import { useDispatch, useSelector } from "react-redux";
import Container from "@mui/material/Container";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { alpha, styled } from "@mui/material/styles";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import images from "Assets";

const checkStatusCode = (code) => {
  switch (code) {
    case 200: {
      return "Đăng ký thành công";
      break;
    }
    case 101: {
      return "Tên tài khoản sai định dạng";
      break;
    }
    case 103: {
      return "Mật khẩu sai định dạng (Tối thiểu 6 ký tự)";
      break;
    }
    case 106: {
      return "Tên tài khoản đã được sử dụng trên tài khoản khác";
      break;
    }
    case 107: {
      return "Số điện thoại đã được sử dụng trên tài khoản khác";
      break;
    }
    case 109: {
      return "Email đã được sử dụng trên tài khoản khác";
      break;
    }
    case 110: {
      return "Số CMND đã sử dụng trên tài khoản khác";
      break;
    }
    case 111: {
      return "Số hộ chiếu đã sử dụng trên tài khoản khác";
      break;
    }
    case 115: {
      return "Số hộ chiếu sai định dạng";
      break;
    }
    default: {
      return "Lỗi không xác định";
    }
  }
};

const backgroundStyle = {
  form: {
    maxWidth: "400px",
    margin: "0 auto",
    width: "85%",
    backgroundColor: "#f5f3f0",
  },
};

const CustomTextField = styled(TextField)({
  "& label.Mui-focused": {
    color: "#57112f",
  },
  "& .MuiInput-underline:after": {
    borderBottomColor: "#57112f",
  },
  "& .MuiOutlinedInput-root": {
    "& fieldset": {
      borderColor: "#57112f",
    },
    "&:hover fieldset": {
      borderColor: "#57112f",
    },
    "&.Mui-focused fieldset": {
      borderColor: "#57112f",
    },
  },
});

const Register = () => {
  const MySwal = withReactContent(Swal);
  const validationSchema = yup.object({
    firstName: yup.string("Enter your name").required("Vui lòng nhập họ"),
    lastName: yup.string("Enter your last name").required("Vui lòng nhập tên"),
    email: yup
      .string("Enter your email")
      .required("Vui lòng nhập email")
      .email("Nhập đúng định dạng Email"),
    username: yup.string("Enter your username").required("Không được bỏ trống"),
    address: yup.string("Enter your adress").required("Vui lòng nhập địa chỉ"),
    identifyNumber: yup
      .string("Enter your identify number")
      .required("Vui lòng nhập CMND của bạn")
      .matches(
        /^([\d]{12})$|^([\d]{9})$/,
        "Số CMND sai định dạng (9 hoặc 12 số)"
      ),
    passportNumber: yup
      .string("Vui lòng nhập số hộ chiếu của bạn")
      .matches(/^([A-Z]{1}[\d]{7})$/, "Số hộ chiếu không đúng"),
    phoneNumber: yup
      .string("Enter your phone number")
      .required("Vui lòng nhập số điện thoại của bạn")
      .matches(/^(0[3|5|7|8|9])([\d]{8})$/, "Nhập sai số điện thoại (10 số)"),
    password: yup
      .string("Enter your password")
      .min(6, "Mật khẩu tối thiểu 6 ký tự")
      .required("Vui lòng nhập mật khẩu"),
    rePassword: yup
      .string("Enter again your re-password")
      .min(6, "Mật khẩu tối thiểu 6 ký tự")
      .required("Vui lòng nhập lại mật khẩu"),
  });
  const [birthDate, setBirthDate] = React.useState(dayjs());
  const [gender, setGender] = React.useState(false);
  var customParseFormat = require("dayjs/plugin/customParseFormat");
  dayjs.extend(customParseFormat);
  const handleChangeBirthDate = (newValue) => {
    setBirthDate(newValue);
  };
  const handleChangeGender = (newValue) => {
    setGender(newValue);
  };
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      username: "",
      email: "",
      phoneNumber: "",
      address: "",
      identifyNumber: "",
      passportNumber: "",
      password: "",
      rePassword: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      if (values.rePassword != values.password) {
        Swal.fire({
          icon: "info",
          title: "Lỗi",
          text: "Bạn đã xác nhận mật khẩu sai",
        });
      } else {
        if (
          dayjs(dayjs(birthDate).format("DD/MM/YYYY"), "DD-MM-YYYY").isValid()
        ) {
          dispatch(
            setStatusBackdrop({
              isOpen: true,
            })
          );
          const body = {
            objectRequest: {
              username: values.username,
              password: values.password,
              firstName: values.firstName,
              lastName: values.lastName,
              email: values.email,
              phone: values.phoneNumber,
              gender: gender,
              address: values.address,
              birthDay: dayjs(birthDate).format("DD/MM/YYYY"),
              passportNo: values.passportNumber,
              identityCardNumber: values.identifyNumber,
            },
          };
          axios
            .post(
              process.env.REACT_APP_BASE_URL +
                process.env.REACT_APP_USER_REGISTER_API_PATH,
              body
            )
            .then(function (response) {
              dispatch(
                setStatusBackdrop({
                  isOpen: false,
                })
              );
              if (response.data.code != 200) {
                Swal.fire({
                  icon: "warning",
                  title: `${checkStatusCode(response.data.code)}`,
                  text: "Hãy kiểm tra lại nhé !",
                  confirmButtonText: "Đồng ý",
                });
              } else {
                Swal.fire({
                  icon: "success",
                  title: `${checkStatusCode(response.data.code)}`,
                  text: "Hãy đăng nhập và tận hưởng nhé <3",
                  confirmButtonText: "Đồng ý",
                });
                navigate("/home");
              }
            })
            .catch(function (error) {
              dispatch(
                setStatusBackdrop({
                  isOpen: false,
                })
              );
              Swal.fire({
                icon: "error",
                title: "Lỗi đăng ký",
                text: "Hãy kiểm tra lại thông tin hoặc thử lại sau",
                confirmButtonText: "Đồng ý",
              });
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Ngày sinh không hợp lệ",
            text: "Hãy kiểm tra lại thông tin",
            confirmButtonText: "Đồng ý",
          });
        }
      }
    },
  });

  return (
    <>
      <SubHeader title={"Đăng ký"}></SubHeader>
      <Box sx={{ pb: 5, marginTop: "-50px" }}>
        <Box className="login-form" style={backgroundStyle.form}>
          <Box className="login-header" style={backgroundStyle.header}>
            <Typography
              variant="h5"
              sx={{
                textAlign: "center",
                fontWeight: "bold",
                backgroundColor: "#57112f",
                py: 2,
                color: "white",
              }}
            >
              Tạo Tài Khoản
            </Typography>
          </Box>
          <Box sx={{ p: 2 }}>
            <form onSubmit={formik.handleSubmit}>
              <Box sx={{ display: "flex", flexDirection: "column", gap: 2 }}>
                <Box sx={{ display: "flex", gap: 2 }}>
                  <CustomTextField
                    fullWidth
                    id="custom-css-outlined-input"
                    name="firstName"
                    label="Họ"
                    type="text"
                    value={formik.values.firstName}
                    error={
                      formik.touched.firstName &&
                      Boolean(formik.errors.firstName)
                    }
                    onChange={formik.handleChange}
                    helperText={
                      formik.touched.firstName && formik.errors.firstName
                    }
                  />
                  <CustomTextField
                    fullWidth
                    id="custom-css-outlined-input"
                    name="lastName"
                    label="Tên"
                    type="text"
                    value={formik.values.lastName}
                    error={
                      formik.touched.lastName && Boolean(formik.errors.lastName)
                    }
                    onChange={formik.handleChange}
                    helperText={
                      formik.touched.lastName && formik.errors.lastName
                    }
                  />
                </Box>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DesktopDatePicker
                    disableFuture
                    label="Ngày Sinh"
                    inputFormat="DD/MM/YYYY"
                    value={birthDate}
                    onChange={handleChangeBirthDate}
                    renderInput={(params) => (
                      <CustomTextField fullWidth {...params} />
                    )}
                  />
                </LocalizationProvider>
                <CustomTextField
                  fullWidth
                  id="custom-css-outlined-input"
                  name="username"
                  label="Tên tài khoản"
                  placeholder="Tối thiểu 6 ký tự"
                  type="text"
                  value={formik.values.username}
                  error={
                    formik.touched.username && Boolean(formik.errors.username)
                  }
                  onChange={formik.handleChange}
                  helperText={formik.touched.username && formik.errors.username}
                />
                <CustomTextField
                  fullWidth
                  id="custom-css-outlined-input"
                  name="email"
                  label="Email"
                  placeholder="abcxyz@gmail.com"
                  type="text"
                  value={formik.values.email}
                  error={formik.touched.email && Boolean(formik.errors.email)}
                  onChange={formik.handleChange}
                  helperText={formik.touched.email && formik.errors.email}
                />
                <CustomTextField
                  fullWidth
                  id="custom-css-outlined-input"
                  name="identifyNumber"
                  label="CMND"
                  placeholder="066XXXXXX hoặc 066XXXXXXXXX"
                  type="text"
                  value={formik.values.identifyNumber}
                  error={
                    formik.touched.identifyNumber &&
                    Boolean(formik.errors.identifyNumber)
                  }
                  onChange={formik.handleChange}
                  helperText={
                    formik.touched.identifyNumber &&
                    formik.errors.identifyNumber
                  }
                />
                <CustomTextField
                  fullWidth
                  id="custom-css-outlined-input"
                  name="passportNumber"
                  label="Hộ Chiếu (Tùy chọn)"
                  placeholder="A1231231"
                  type="text"
                  value={formik.values.passportNumber}
                  error={
                    formik.touched.passportNumber &&
                    Boolean(formik.errors.passportNumber)
                  }
                  onChange={formik.handleChange}
                  helperText={
                    formik.touched.passportNumber &&
                    formik.errors.passportNumber
                  }
                />
                <CustomTextField
                  fullWidth
                  id="phoneNumber"
                  name="phoneNumber"
                  label="Số điện thoại"
                  type="text"
                  placeholder="0971767XXX"
                  value={formik.values.phoneNumber}
                  error={
                    formik.touched.phoneNumber &&
                    Boolean(formik.errors.phoneNumber)
                  }
                  onChange={formik.handleChange}
                  helperText={
                    formik.touched.phoneNumber && formik.errors.phoneNumber
                  }
                />
                <CustomTextField
                  fullWidth
                  id="address"
                  name="address"
                  label="Địa chỉ"
                  type="text"
                  placeholder="Số nhà X đường X thành phố X"
                  value={formik.values.address}
                  error={
                    formik.touched.address && Boolean(formik.errors.address)
                  }
                  onChange={formik.handleChange}
                  helperText={formik.touched.address && formik.errors.address}
                />
                <FormControl>
                  <FormLabel id="demo-row-radio-buttons-group-label">
                    Giới Tính
                  </FormLabel>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                    value={gender}
                    onChange={(e) => {
                      handleChangeGender(e.target.value);
                    }}
                  >
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="Nữ"
                    />
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Nam"
                    />
                  </RadioGroup>
                </FormControl>
                <CustomTextField
                  fullWidth
                  id="custom-css-outlined-input"
                  name="password"
                  label="Mật khẩu"
                  placeholder="Tối thiểu 6 ký tự"
                  type="password"
                  value={formik.values.password}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  onChange={formik.handleChange}
                  helperText={formik.touched.password && formik.errors.password}
                />
                <CustomTextField
                  fullWidth
                  id="rePassword"
                  name="rePassword"
                  label="Nhập lại mật khẩu"
                  type="password"
                  value={formik.values.rePassword}
                  error={
                    formik.touched.rePassword &&
                    Boolean(formik.errors.rePassword)
                  }
                  onChange={formik.handleChange}
                  helperText={
                    formik.touched.rePassword && formik.errors.rePassword
                  }
                />
                <Button
                  color="primary"
                  variant="contained"
                  fullWidth
                  type="submit"
                  sx={{
                    marginTop: "20px",
                    fontWeight: "600",
                    backgroundColor: "#57112f",
                    ":hover": {
                      backgroundColor: "#FFA903",
                      color: "#57112f",
                    },
                  }}
                >
                  Đăng ký
                </Button>
              </Box>
            </form>
          </Box>
        </Box>
      </Box>
      <SnackBar></SnackBar>
    </>
  );
};

export default Register;
