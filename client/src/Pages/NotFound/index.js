import React from "react";
import ReactDOM from "react-dom";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import image from "Assets";
import { Navigate, useNavigate } from "react-router-dom";

const NotFound = () => {
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        width: "100vw",
        height: "100vh",
        position: "relative",
        backgroundColor: "#1a2238",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%,-50%)",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            background: `url(${image.notFound.logo})`,
            backgroundSize: "cover",
            width: "300px",
            height: "300px",
          }}
        ></Box>
        <Typography
          variant="h3"
          sx={{
            color: "white",
            textTransform: "uppercase",
            opacity: 0.7,
            textAlign: "center",
          }}
        >
          Không khả dụng
        </Typography>
        <Button
          sx={{
            backgroundColor: "#FFA903",
            fontWeight: "600",
            color: "#1a2238",
            my: 3,
            ":hover": {
              color: "white",
              border: "1px dashed white",
            },
          }}
          onClick={() => {
            navigate("/home");
          }}
        >
          Quay lại trang chủ
        </Button>
      </Box>
    </Box>
  );
};
export default NotFound;
